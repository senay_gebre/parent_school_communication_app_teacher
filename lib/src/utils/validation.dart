import 'package:form_field_validator/form_field_validator.dart';

class CustomValidator {
  final String emptyFieldErrorText = "field is required";
  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'field is required'),
    MinLengthValidator(8, errorText: 'password must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-])',
        errorText: 'passwords must have at least one special character')
  ]);

  final usernameValidator = MultiValidator([
    RequiredValidator(errorText: 'field is required'),
    LengthRangeValidator(min: 5, max: 12, errorText: "enter a valid username"),
    PatternValidator(r'^[a-zA-Z]+$', errorText: "enter a valid username"),
  ]);

  final nameValidator = MultiValidator([
    RequiredValidator(errorText: 'field is required'),
    PatternValidator(r"^[a-zA-Z ,.'-]+$", errorText: "enter a valid name"),
    LengthRangeValidator(min: 2, max: 20, errorText: "enter a valid name"),
  ]);

  final emailddressValidator = MultiValidator([
    RequiredValidator(errorText: 'field is required'),
    EmailValidator(errorText: 'Enter a valid email address'),
  ]);

  final phoneNumberValidator = MultiValidator([
    RequiredValidator(errorText: 'field is required'),
    PatternValidator(r'(^(?:[+0]9)?[0-9]{10,12}$)',
        errorText: 'enter a valid phone number'),
  ]);

  final emptyFieldValidator = RequiredValidator(errorText: 'field is required');
}
