import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:login/src/model/com_book.dart';
import 'package:login/src/model/com_book_get_data.dart';
import 'package:login/src/model/response_model.dart';

import '../resources/auth_methods.dart';

class ComBookProvider extends ChangeNotifier {
  AuthMethods _authMethods = AuthMethods();
  var _selectedClass;

  ComBookGetData comBookGetData = ComBookGetData();

  List<ComBook> _comBookList;

  ResponseModel responseModel = ResponseModel();

  UnmodifiableListView<ComBook> get comBookList {
    if (_comBookList != null) {
      return UnmodifiableListView(_comBookList);
    }
    return null;
  }

  int get getCount {
    return _comBookList != null ? _comBookList.length : 0;
  }

  void setSelectedClass({var selectedClass}) {
    this._selectedClass = selectedClass;
    notifyListeners();
  }

  get getSelectedClass => _selectedClass;

  void setComBookData({int month, int year}) {
    comBookGetData.crid = _selectedClass.crid;
    comBookGetData.month = month;
    comBookGetData.year = year;
  }

  void clearComBookist() {
    _comBookList.clear();
  }

  Future<void> refreshComBook() async {
    _comBookList = null;
    responseModel = await _authMethods.viewClassComBook(comBookGetData);
    _comBookList = responseModel.response != null
        ? responseModel.response.isNotEmpty
            ? responseModel.response
            : []
        : null;

    print("${_comBookList} feven");

    notifyListeners();
  }
}
