// import 'dart:collection';

// import 'package:flutter/cupertino.dart';
// import 'package:login/src/model/Activity.dart';

// class ActivityProvider extends ChangeNotifier {
//   List<Activity> _activities = [
//     Activity(subject: 'English', activityType: []),
//     Activity(subject: 'Science', activityType: []),
//     Activity(subject: 'Math', activityType: []),
//     Activity(subject: 'Spoken', activityType: []),
//     Activity(subject: 'Reading', activityType: []),
//   ];

//   List<Activity> _reseterList = [
//     Activity(subject: 'English', activityType: []),
//     Activity(subject: 'Science', activityType: []),
//     Activity(subject: 'Math', activityType: []),
//     Activity(subject: 'Spoken', activityType: []),
//     Activity(subject: 'Reading', activityType: []),
//   ];

//   UnmodifiableListView<Activity> get activities {
//     return UnmodifiableListView(_activities);
//   }

//   int get activitiesLength {
//     return _activities.length;
//   }

//   void addActivityType(int index, String type) {
//     _activities[index].activityType.add(type);
//     notifyListeners();
//   }

//   void deleteActivityType(int index, String type) {
//     _activities[index].activityType.remove(type);
//     notifyListeners();
//   }

//   // void addTask(String newTaskTitle) {
//   //   final task = Task(name: newTaskTitle);
//   //   _tasks.add(task);
//   //   notifyListeners();
//   // }
//   //
//   void updateHWActivity(Activity activity) {
//     activity.toggleHWCheckbox();
//     notifyListeners();
//   }

//   void updateGWActivity(Activity activity) {
//     activity.toggleGWCheckbox();
//     notifyListeners();
//   }

//   void updateAssiActivity(Activity activity) {
//     activity.toggleAssiCheckbox();
//     notifyListeners();
//   }

//   void crearActivities() {
//     _activities = _reseterList;
//     notifyListeners();
//   }

//   //
//   // void deleteTask(Task task) {
//   //   _tasks.remove(task);
//   //   notifyListeners();
//   // }
// }
