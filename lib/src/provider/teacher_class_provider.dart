import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:login/src/model/teacher_class.dart';
import 'package:login/src/resources/auth_methods.dart';

class TeacherClassProvider extends ChangeNotifier {
  AuthMethods _authMethods = AuthMethods();
  List<TeacherClass> _teacherClassesList = [];

  UnmodifiableListView<TeacherClass> get teacherClassList {
    if (_teacherClassesList != null) {
      return UnmodifiableListView(_teacherClassesList);
    }
    return null;
  }

  int get getCount {
    return _teacherClassesList.length;
  }

  // void setCrid(String crid) {
  //   this.crid = crid;
  // }

  void clearComBookist() {
    print("aynamelem");

    _teacherClassesList.clear();
  }

  Future<void> refreshteacherClassList() async {
    print("aynamelem");
    _teacherClassesList = await _authMethods.viewMyClass();
    notifyListeners();
  }
}
