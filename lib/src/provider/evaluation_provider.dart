import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:login/src/model/rating.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/resources/auth_methods.dart';

class EvaluationProvider extends ChangeNotifier {
  bool isLoading = false;
  var _selectedClass;
  String _quarter;
  String _selectedSubject;
  ResponseModel responseModel = ResponseModel();
  AuthMethods _authMethods = AuthMethods();

  List<dynamic> _listOfRateToBeUpdated = [];
  List<dynamic> _dataToSent = [];
  // var _studentGradeReport;

  String subject;

  void toggleIsLoadingT() {
    isLoading = true;
  }

  void toggleIsLoadingF() {
    isLoading = false;
  }

  void setSelectedClass({var selectedClass}) {
    this._selectedClass = selectedClass;
    notifyListeners();
  }

  get getSelectedClass => _selectedClass;

  void setSelectedSubject({String subject}) {
    this._selectedSubject = subject != null ? subject.toLowerCase() : null;
    notifyListeners();
  }

  get getSelectedSubject => _selectedSubject;

  void setQuarter({String quarter}) {
    this._quarter = quarter;
    notifyListeners();
  }

  get getQuarter => _quarter;

  UnmodifiableListView<dynamic> get listOfRateToBeUpdated {
    return UnmodifiableListView(_listOfRateToBeUpdated);
  }

  UnmodifiableListView<dynamic> get dataToSent {
    return UnmodifiableListView(_dataToSent);
  }

  // UnmodifiableListView<dynamic> get studentGradeReportList {
  //   return UnmodifiableListView(_studentGradeReport);
  // }

  int get getCount {
    return _listOfRateToBeUpdated.length;
  }

  // int get getGRCount {
  //   return _studentGradeReport.length;
  // }

  // void setCrid(String crid) {
  //   this.crid = crid;
  // }

  void clearDataToBeUpdated() {
    _listOfRateToBeUpdated.clear();
  }

  void setSubject(String subject) {
    subject = subject;
  }

  void updateRate(
      {Rating rateToBeUpdated, String skillType, int updatedValue}) {
    bool _isExist = false;

    rateToBeUpdated.rateDate = "${DateTime.now()}".split(' ')[0];

    switch (skillType) {
      case "Communication Skill":
        rateToBeUpdated.comSkill = updatedValue;
        break;
      case "Problem Solving Skill":
        rateToBeUpdated.psSkill = updatedValue;
        break;
      case "Participation Skill":
        rateToBeUpdated.parSkill = updatedValue;
        break;
      default:
        rateToBeUpdated.parSkill = 0;
    }
    var newUpdatedRate = rateToBeUpdated;

    print("${_listOfRateToBeUpdated.isEmpty} plplpl");
    var singleUpdatedRate;
    for (singleUpdatedRate in _listOfRateToBeUpdated) {
      if (singleUpdatedRate.stuIdNo == rateToBeUpdated.stuIdNo) {
        print("gggggg");
        _isExist = true;
      }
    }
    print("${_isExist} ggggggg");
    if (_isExist) {
      singleUpdatedRate.rateDate = "${DateTime.now()}".split(' ')[0];
      switch (skillType) {
        case "Communication Skill":
          singleUpdatedRate.comSkill = updatedValue;
          break;
        case "Problem Solving Skill":
          singleUpdatedRate.psSkill = updatedValue;

          break;
        case "Participation Skill":
          singleUpdatedRate.parSkill = updatedValue;

          break;
        default:
          singleUpdatedRate.parSkill = 0;
      }
    } else {
      _listOfRateToBeUpdated.add(newUpdatedRate);
    }
    // notifyListeners();
  }

  void refreshGradeReportList() async {
    responseModel = await _authMethods.viewResultPerClassBySub(
        selectedClass: _selectedClass,
        quarter: _quarter,
        subject: _selectedSubject);
    notifyListeners();
  }
}
