import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:login/src/model/attendance.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:intl/intl.dart';

class AttendanceProvider extends ChangeNotifier {
  // String crid;
  AuthMethods _authMethods = AuthMethods();
  ResponseModel _responseModel = ResponseModel();
  DateTime selectedDate;
  String crid;
  String stuid;
  List<Attendance> _listOfAttendance = [];
  List<Attendance> _listOfAddAttendance = [];
  int prevMonth = 0;

  List calendar = [];
  int numberOfDaysInAmonth = 0;

  UnmodifiableListView<dynamic> get listOfAttendance {
    return UnmodifiableListView(_listOfAttendance);
  }

  UnmodifiableListView<dynamic> get listOfAddAttendance {
    return UnmodifiableListView(_listOfAddAttendance);
  }

  int get getCount {
    return _listOfAttendance.length;
  }

  int get getAddedCount {
    return _listOfAddAttendance.length;
  }

  void setAttendanceGetData({
     String crid,
     String stuid,
     int prevMonth,
  }) {
    this.crid = crid;
    this.stuid = stuid;
    this.prevMonth = prevMonth;
  }

  void updateCalendar(Attendance singleAttendance) {
    int index = calendar.indexOf(singleAttendance.date.split('-')[2]);
    print("${index} baby");
    if (index != -1) {
      calendar.removeAt(index);
      calendar.insert(index, singleAttendance);
    }
  }

  void setSelectedDate(DateTime selectedDate) {
    this.selectedDate = selectedDate;
  }

  int daysIn({int month, int forYear}) {
    DateTime firstOfNextMonth;
    if (month == 12) {
      firstOfNextMonth =
          DateTime(forYear + 1, 1, 1, 12); //year, month, day, hour
    } else {
      firstOfNextMonth = DateTime(forYear, month + 1, 1, 12);
    }
    int numberOfDaysInMonth = firstOfNextMonth.subtract(Duration(days: 1)).day;
    //.subtract(Duration) returns a DateTime, .day gets the integer for the day of that DateTime
    return numberOfDaysInMonth;
  }

  List addZeroGaps(int numberOfGap) {
    var zeroList = [];
    for (int i = 0; i < numberOfGap; i++) {
      zeroList.add("0");
    }
    return zeroList;
  }

  void constructCalendar() {
    print("${selectedDate} kana");
    calendar.clear();

    var nameOfFirstDay = DateFormat('EEEE').format(selectedDate);

    switch (nameOfFirstDay) {
      case "Sunday":
        calendar += addZeroGaps(0);
        break;
      case "Monday":
        calendar += addZeroGaps(1);

        break;
      case "Tuesday":
        calendar += addZeroGaps(2);

        break;
      case "Wednesday":
        calendar += addZeroGaps(3);

        break;
      case "Thursday":
        calendar += addZeroGaps(4);

        break;
      case "Friday":
        calendar += addZeroGaps(5);

        break;
      case "Saturday ":
        calendar += addZeroGaps(6);

        break;
      default:
    }
    numberOfDaysInAmonth =
        daysIn(month: selectedDate.month, forYear: selectedDate.year);

    for (int j = 1; j <= numberOfDaysInAmonth; j++) {
      if (j.toString().length <= 1) {
        calendar.add("0" + j.toString());
      } else {
        calendar.add(j.toString());
      }
    }
  }

  void refreshAttendnaceList() async {
    _listOfAttendance.clear();
    _responseModel = await _authMethods.getStudentAttendance(
      crid: crid,
      month: selectedDate.month,
      stuId: stuid,
      year: selectedDate.year,
      prevMonth: prevMonth,
    );
    print(
        "${_responseModel.response} kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk senay son son son son son son son son son son");

    if (_responseModel.response != null) {
      if (_responseModel.response.isNotEmpty) {
        _listOfAttendance = _responseModel.response;
      } else {
        _listOfAttendance = [];
      }
    } else {
      _listOfAttendance = [];
    }

    notifyListeners();
  }

  void clearAttendanceList() {
    _listOfAddAttendance.clear();
  }

  void updateValue({String stuid, int index}) {
    bool _isExist = false;
    print("${index} shito");
    var singleStudentAttendance;
    for (singleStudentAttendance in _listOfAddAttendance) {
      if (singleStudentAttendance.stuId == stuid) {
        _isExist = true;
        switch (index) {
          case 0:
            singleStudentAttendance.present = true;
            singleStudentAttendance.permitted = false;
            singleStudentAttendance.sLate = false;
            break;
          case 1:
            singleStudentAttendance.present = false;
            singleStudentAttendance.permitted = false;
            singleStudentAttendance.sLate = true;
            break;
          case 2:
            singleStudentAttendance.present = false;
            singleStudentAttendance.permitted = false;
            singleStudentAttendance.sLate = false;
            break;
          case 3:
            singleStudentAttendance.present = false;
            singleStudentAttendance.permitted = true;
            singleStudentAttendance.sLate = false;
            break;
          default:
        }
      }
    }

    if (!_isExist) {
      Attendance attendance = Attendance();
      attendance.stuId = stuid;
      switch (index) {
        case 0:
          attendance.present = true;
          attendance.permitted = false;
          attendance.sLate = false;
          break;
        case 1:
          attendance.present = false;
          attendance.permitted = false;
          attendance.sLate = true;
          break;
        case 2:
          attendance.present = false;
          attendance.permitted = false;
          attendance.sLate = false;
          break;
        case 3:
          attendance.present = false;
          attendance.permitted = true;
          attendance.sLate = false;
          break;
        default:
      }

      _listOfAddAttendance.add(attendance);
    }
    notifyListeners();
  }
}
