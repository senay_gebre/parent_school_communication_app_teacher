import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:login/src/model/event.dart';

import '../resources/auth_methods.dart';

class EventsProvider extends ChangeNotifier {
  int pageNumberAE;
  int pageNumberUP;

  AuthMethods _authMethods = AuthMethods();
  List<Event> _listOfAllEvents;
  List<Event> _listOfUpcomingEvents;

  bool fetchDone = false;
  bool _eventNotNull = false;
  var fetchedData;

  UnmodifiableListView<Event> get listOfAllEvents {
    return UnmodifiableListView(_listOfAllEvents);
  }

  UnmodifiableListView<Event> get listOfUpcomingEvents {
    return UnmodifiableListView(_listOfUpcomingEvents);
  }

  bool eventNotNull() {
    print("angi");
    if (_listOfAllEvents != null && _listOfUpcomingEvents != null) {
      _eventNotNull = true;
    } else {
      _eventNotNull = false;
    }
    return _eventNotNull;
  }

  void setPageNumberAE({int pageNumberAE}) {
    this.pageNumberAE = pageNumberAE;
  }

  void setPageNumberUP({int pageNumberUP}) {
    this.pageNumberUP = pageNumberUP;
  }

  Future<void> refreshAllEvents() async {
    fetchedData = null;
    fetchDone = false;
    List<Event> eventLoadList = [];
    await _authMethods.getAllEvents(pageNumberAE: pageNumberAE).then((value) {
      fetchedData = value;
      if (value != null) {
        for (int i = 0; i < value.response.length; i++) {
          eventLoadList.add(value.response[i]);
        }

        if (_listOfAllEvents == null) {
          _listOfAllEvents = eventLoadList;
        } else {
          for (int i = 0; i < eventLoadList.length; i++) {
            _listOfAllEvents.add(eventLoadList[i]);
          }
        }
      }

      if (fetchedData != null) {
        fetchDone = true;
        print("${fetchDone} value");
      }
    });

    notifyListeners();
  }

  Future<void> refreshUpcomingEvents() async {
    fetchedData = null;
    fetchDone = false;
    List<Event> eventLoadList = [];
    await _authMethods
        .getUpcomingEvents(pageNumberUP: pageNumberUP)
        .then((value) {
      fetchedData = value;
      for (int i = 0; i < value.response.length; i++) {
        eventLoadList.add(value.response[i]);
      }

      if (_listOfUpcomingEvents == null) {
        _listOfUpcomingEvents = eventLoadList;
      } else {
        for (int i = 0; i < eventLoadList.length; i++) {
          _listOfUpcomingEvents.add(eventLoadList[i]);
        }
      }

      if (fetchedData != null) {
        fetchDone = true;
        print("${fetchDone} value");
      }
    });

    notifyListeners();
  }
}
