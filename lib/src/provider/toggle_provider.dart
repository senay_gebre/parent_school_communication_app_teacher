import 'package:flutter/material.dart';

class ToggleProvider extends ChangeNotifier {
  bool isEditingEnabled = false;

  void toggleSwitch() {
    print("toggled");
    isEditingEnabled = !isEditingEnabled;
    notifyListeners();
  }
}
