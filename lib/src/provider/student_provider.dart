import 'dart:collection';
import 'package:flutter/foundation.dart';
import '../model/student.dart';
import '../resources/auth_methods.dart';

class StudentProvider extends ChangeNotifier {
  AuthMethods _authMethods = AuthMethods();
  Map studentMap = <String, dynamic>{};
  String crid;
  List<Student> _studentList = [];
  List<Student> _allStudentList = [];
  List<Map> _studentMapList = [];
  // String crid = "60b0c9fea681ac0004c84c36";

  UnmodifiableListView<Student> get studentList {
    return UnmodifiableListView(_studentList);
  }

  UnmodifiableListView<Student> get allStudentList {
    return UnmodifiableListView(_allStudentList);
  }

  UnmodifiableListView<Map> get studentMapList {
    return UnmodifiableListView(_studentMapList);
  }

  int get getCount {
    return _allStudentList.length;
  }

  int get getStuCount {
    return _studentList.length;
  }

  int get getStuMapCount {
    return _studentMapList.length;
  }

// TODO: the below should be deleted it is for test.
  int get getStuMapCountFirst {
    return _studentMapList[0]["StudentList"].length;
  }

  void setCrid(String crid) {
    this.crid = crid;
  }

  Future<void> refreshStudentList() async {
    print("${_studentList} senay son son son son son son son son son son");

    _studentList = await _authMethods.viewMyStudents(crid);

    print(_studentList);

    notifyListeners();
  }

  Future<void> refreshStudentMapList(var classList) async {
    var singleClass;
    var oneClassStudent;
    var singleStudent;

    for (singleClass in classList) {
      await _authMethods.viewMyStudents(singleClass.crid).then((value) {
        print("${value} gebr");
        _studentList = value;
      });
      _studentMapList
          .add({"crid": singleClass.crid, "StudentList": _studentList});
    }

    if (_studentMapList != null) {
      for (oneClassStudent in _studentMapList) {
        for (singleStudent in oneClassStudent["StudentList"]) {
          _allStudentList.add(singleStudent);
        }
      }
    }

    notifyListeners();
  }
}
