import 'package:flutter/widgets.dart';
import 'package:login/src/model/user_file.dart';
import 'package:login/src/resources/auth_methods.dart';

class UserProvider with ChangeNotifier {
  UserFile _user;
  AuthMethods _authMethods = AuthMethods();

  UserFile get getUser => _user;

  Future<void> refreshUser() async {
    UserFile user = await _authMethods.getCurrentUser();
    _user = user;
    notifyListeners();
  }
}
