import 'package:flutter/material.dart';

class ActivityTile extends StatelessWidget {
  final bool isChecked;
  final String subject;
  final Function checkboxCallback;


  ActivityTile(
      {this.isChecked,
        this.subject,this.checkboxCallback,
 });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      // onLongPress: longPressCallback,
      title: Text(
        subject,
      ),
      trailing: Checkbox(
        activeColor: Colors.lightBlueAccent,
        value: isChecked,
        onChanged: checkboxCallback,
      ),
    );
  }
}