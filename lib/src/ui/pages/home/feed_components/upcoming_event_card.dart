import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/event.dart';
import 'package:intl/intl.dart';

class UpcomingEventCard extends StatelessWidget {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  final Event event;
  final onTap;

  UpcomingEventCard({
    this.event,
    this.onTap,
  });

  //TODO: on events output from postman there is a key "page" it is always 3

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: [
          Container(
            width: 180,
            decoration: BoxDecoration(
              border: Border.all(width: 0.2, color: Colors.grey),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 3,
                  offset: Offset(0, 2), // changes position of shadow
                ),
              ],
              // color: kPrimaryColor,
              borderRadius: BorderRadius.circular(12),
              image: event.imageURL != ""
                  ? DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(event.imageURL))
                  : DecorationImage(
                      fit: BoxFit.scaleDown,
                      scale: 15,
                      alignment: Alignment.center,
                      image: ExactAssetImage(
                          "assets/theimages/event_icon_orange.png"),
                    ),
            ),
            child: Stack(
              children: [
                Container(
                  // height: double.maxFinite,
                  width: 180,
                  decoration: BoxDecoration(
                    color: kPrimaryColor.withOpacity(0.18),
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: double.infinity,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.85),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(12),
                        bottomRight: Radius.circular(12),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        capitalize(event.eventTitle),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 17,
                            color: kPrimaryColor,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned.fill(
              child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    splashColor: Colors.black12.withOpacity(0.05),
                    borderRadius: BorderRadius.circular(12),
                    onTap: onTap,
                  ))),
        ],
      ),
    );
  }
}
