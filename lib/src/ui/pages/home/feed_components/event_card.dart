import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/event.dart';
import 'package:intl/intl.dart';
import 'dart:ui' as ui;

class EventCard extends StatelessWidget {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  final onTap;
  bool _eventPassed = false;
  bool _isEventOnGoing = false;
  List months = [
    'Dec',
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
  ];
  var current_mon = DateTime.now().month;

  final Event event;

  EventCard({
    this.event,
    this.onTap,
  });

  //TODO: on events output from postman there is a key "page" it is always
  //TODO: luel photo upload sidereg crop yadrgew
  //TODO: nati event paginationun yasanisew
  //TODO: event lay description yelewm

  @override
  Widget build(BuildContext context) {
    DateTime eventStartDate = DateTime.parse(event.startDate.split("T")[0]);
    DateTime eventEndDate = DateTime.parse(event.endDate.split("T")[0]);
    // print(DateTime.now());
    print(eventStartDate);
    print(event.eventTitle);

    if (eventEndDate.compareTo(DateTime.now()) < 0) {
      _eventPassed = true;
    } else if (eventStartDate.compareTo(DateTime.now()) < 0 &&
        eventEndDate.compareTo(DateTime.now()) > 0) {
      _isEventOnGoing = true;
    }

    final size = MediaQuery.of(context).size;

    return Container(
      height: 100,
      margin: EdgeInsets.symmetric(horizontal: 6.0, vertical: 4.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(3),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.8),
            spreadRadius: 0.2,
            blurRadius: 0.5,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(3),
                  bottomLeft: Radius.circular(3),
                ),
                child: Stack(
                  children: [
                    Container(
                      height: 100,
                      width: 100,
                      color: Colors.white,
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: event.imageURL,
                        placeholder: (context, url) => SpinKitThreeBounce(
                          size: 34,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin: EdgeInsets.all(4),
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  color: Colors.orange,
                                  borderRadius: BorderRadius.circular(50),
                                ),
                              ),
                            );
                          },
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          'assets/theimages/event_icon_orange.png',
                          fit: BoxFit.scaleDown,
                          scale: 20,
                          color: Colors.orangeAccent,
                        ),
                      ),
                    ),
                    Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        color: kPrimaryColor.withOpacity(0.2),
                        // borderRadius: BorderRadius.circular(12),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 4.0, vertical: 4.0),
                        child: Text(
                          "${capitalize(event.eventTitle)}",
                          style: TextStyle(
                            color: kPrimaryColor,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.calendar_today,
                                  size: 15,
                                  color: Colors.orange,
                                ),
                                !eventStartDate.isAtSameMomentAs(eventEndDate)
                                    ? Text(
                                        " ${months[eventStartDate.month]} ${eventStartDate.day}, ${eventStartDate.year} -  ${months[eventEndDate.month]} ${eventEndDate.day}, ${eventEndDate.year}",
                                        style: TextStyle(
                                            color: Colors.orangeAccent,
                                            fontSize: size.width * 0.036),
                                      )
                                    : Text(
                                        " ${months[eventStartDate.month]} ${eventStartDate.day}, ${eventStartDate.year}",
                                        style: TextStyle(
                                            color: Colors.orangeAccent,
                                            fontSize: size.width * 0.036),
                                      ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: size.width * 0.06,
                              ),
                              Text(
                                "Starts at ${DateFormat('hh:mm a').format(eventStartDate)}",
                                style: TextStyle(
                                    color: Colors.orangeAccent, fontSize: 12),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 18.0),
                            child: Row(
                              children: [
                                Expanded(child: SizedBox()),
                                _eventPassed
                                    ? Text(
                                        "event passed",
                                        style: TextStyle(
                                            color: Colors.redAccent,
                                            fontWeight: FontWeight.w300),
                                      )
                                    : _isEventOnGoing
                                        ? Text(
                                            "event ongoing",
                                            style: TextStyle(
                                                color: Colors.greenAccent,
                                                fontWeight: FontWeight.w300),
                                          )
                                        : Container(),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Positioned.fill(
              child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    splashColor: Colors.black12.withOpacity(0.05),
                    borderRadius: BorderRadius.circular(5),
                    onTap: onTap,
                  ))),
        ],
      ),
    );
  }
}
