import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/event.dart';
import 'package:login/src/provider/events_provider.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/home/feed_components/event_card.dart';
import 'package:login/src/ui/pages/home/feed_components/upcoming_event_card.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class EventFeed extends StatefulWidget {
  @override
  _EventFeedState createState() => _EventFeedState();
}

class _EventFeedState extends State<EventFeed> {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  bool _eventPassed = false;
  bool _isEventOnGoing = false;
  bool _isLoading = false;

  List months = [
    'Dec',
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
  ];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitDown]);
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 12),
              Text(
                "Hey! Wellcome.",
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(height: 12),
              Text(
                "Tap any event to view details.",
                style: TextStyle(fontSize: 11, color: Colors.grey),
              ),
            ],
          ),
          Expanded(
            child: Container(
              child: Consumer<EventsProvider>(
                builder: (context, eventsProvider, child) {
                  print("index");
                  var singleEventAE;
                  var singleEventUp;
                  List<Widget> listOfAllEventWidget = [];
                  List<Widget> listOfUpcomigEventWidget = [];

                  // for (singleEventAE in eventsProvider.listOfAllEvents) {
                  //   listOfAllEventWidget.add(
                  //     EventCard(
                  //       onTap: () {
                  //         showModalBottomSheet(
                  //           elevation: 9.0,
                  //           useRootNavigator: true,
                  //           shape: RoundedRectangleBorder(
                  //             borderRadius: BorderRadius.only(
                  //               topLeft: Radius.circular(10),
                  //               topRight: Radius.circular(10),
                  //             ),
                  //           ),
                  //           context: context,
                  //           builder: (builder) =>
                  //               bottomSheet(event: singleEventAE),
                  //         );
                  //       },
                  //       event: singleEventAE,
                  //     ),
                  //   );
                  // }
                  // for (singleEventUp in eventsProvider.listOfUpcomingEvents) {
                  //   listOfUpcomigEventWidget.add(UpcomingEventCard(
                  //     event: singleEventUp,
                  //   ));
                  // }
                  // listOfAllEventWidget.add(
                  //   TextButton(
                  //     onPressed: () async {
                  //       setState(() {
                  //         _isLoading = true;
                  //       });

                  //       await performLoadAE();
                  //     },
                  //     child: _isLoading == false
                  //         ? Text("Load more")
                  //         : SpinKitThreeBounce(
                  //             size: 34,
                  //             itemBuilder: (BuildContext context, int index) {
                  //               return Container(
                  //                 margin: EdgeInsets.all(4),
                  //                 child: DecoratedBox(
                  //                   decoration: BoxDecoration(
                  //                       color: Colors.orange,
                  //                       borderRadius:
                  //                           BorderRadius.circular(50)),
                  //                 ),
                  //               );
                  //             },
                  //           ),
                  //   ),
                  // );

                  // listOfUpcomigEventWidget.add(
                  //   TextButton(
                  //     onPressed: () async {
                  //       setState(() {
                  //         _isLoading = true;
                  //       });

                  //       await performLoadUP();
                  //     },
                  //     child: _isLoading == false
                  //         ? Text("Load more")
                  //         : SpinKitThreeBounce(
                  //             size: 34,
                  //             itemBuilder: (BuildContext context, int index) {
                  //               return Container(
                  //                 margin: EdgeInsets.all(4),
                  //                 child: DecoratedBox(
                  //                   decoration: BoxDecoration(
                  //                       color: Colors.orange,
                  //                       borderRadius:
                  //                           BorderRadius.circular(50)),
                  //                 ),
                  //               );
                  //             },
                  //           ),
                  //   ),
                  // );

                  return eventsProvider.eventNotNull() == true
                      ? eventsProvider.listOfAllEvents.isNotEmpty
                          ? Column(
                              children: [
                                // Container(
                                //   height: size.height * 0.27,
                                //   padding: const EdgeInsets.all(8.0),
                                //   child: Column(
                                //     crossAxisAlignment: CrossAxisAlignment.start,
                                //     children: [
                                //       Text(
                                //         "Upcoming Events:",
                                //         style: TextStyle(
                                //             color: Colors.grey, fontSize: 16),
                                //       ),
                                //       Expanded(
                                //         child: ListView(
                                //           shrinkWrap: true,
                                //           scrollDirection: Axis.horizontal,
                                //           children: listOfUpcomigEventWidget,
                                //         ),
                                //       ),
                                //     ],
                                //   ),
                                // ),
                                // Expanded(
                                //   child: Padding(
                                //     padding: const EdgeInsets.all(8.0),
                                //     child: Column(
                                //       crossAxisAlignment: CrossAxisAlignment.start,
                                //       children: [
                                //         Text(
                                //           "All Events:",
                                //           style: TextStyle(
                                //               color: Colors.grey, fontSize: 16),
                                //         ),
                                //         Expanded(
                                //           child: ListView(
                                //             shrinkWrap: true,
                                //             children: listOfAllEventWidget,
                                //           ),
                                //         ),
                                //       ],
                                //     ),
                                //   ),
                                // ),
                                Container(
                                  height: 260,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0, vertical: 6),
                                        child: Text(
                                          "All Events:",
                                          style: TextStyle(
                                              fontSize: 16, color: Colors.grey),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          shrinkWrap: true,
                                          itemBuilder: (context, index) {
                                            Event singleEventFromUP =
                                                eventsProvider
                                                        .listOfUpcomingEvents[
                                                    index];
                                            return UpcomingEventCard(
                                              event: singleEventFromUP,
                                              onTap: () {
                                                showModalBottomSheet(
                                                  elevation: 9.0,
                                                  useRootNavigator: true,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topLeft:
                                                          Radius.circular(10),
                                                      topRight:
                                                          Radius.circular(10),
                                                    ),
                                                  ),
                                                  context: context,
                                                  builder: (builder) =>
                                                      bottomSheet(
                                                          event:
                                                              singleEventFromUP),
                                                );
                                              },
                                            );
                                          },
                                          itemCount: eventsProvider
                                              .listOfUpcomingEvents.length,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 50,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0, vertical: 6),
                                        child: Text(
                                          "All Events:",
                                          style: TextStyle(
                                              fontSize: 16, color: Colors.grey),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          itemBuilder: (context, index) {
                                            Event singleEventFromAE =
                                                eventsProvider
                                                    .listOfAllEvents[index];
                                            return EventCard(
                                              event: singleEventFromAE,
                                              onTap: () {
                                                showModalBottomSheet(
                                                  elevation: 9.0,
                                                  useRootNavigator: true,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topLeft:
                                                          Radius.circular(10),
                                                      topRight:
                                                          Radius.circular(10),
                                                    ),
                                                  ),
                                                  context: context,
                                                  builder: (builder) =>
                                                      bottomSheet(
                                                          event:
                                                              singleEventFromAE),
                                                );
                                              },
                                            );
                                          },
                                          itemCount: eventsProvider
                                              .listOfAllEvents.length,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )
                          : Center(
                              heightFactor: size.height * 0.05,
                              child: Text("No events added."),
                            )
                      : LoadingPage();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  performLoadAE() async {
    Provider.of<EventsProvider>(context, listen: false).pageNumberAE++;
    await Provider.of<EventsProvider>(context, listen: false)
        .refreshAllEvents();
    if (Provider.of<EventsProvider>(context, listen: false).fetchDone) {
      print("fetch");
      setState(() {
        _isLoading = false;
      });
    }
  }

  performLoadUP() async {
    Provider.of<EventsProvider>(context, listen: false).pageNumberUP++;
    await Provider.of<EventsProvider>(context, listen: false)
        .refreshUpcomingEvents();
    if (Provider.of<EventsProvider>(context, listen: false).fetchDone) {
      print("fetch");
      setState(() {
        _isLoading = false;
      });
    }
  }

  Widget bottomSheet({Event event}) {
    print("${event.eventTitle} evento");
    final size = MediaQuery.of(context).size;

    DateTime eventStartDate = DateTime.parse(event.startDate.split("T")[0]);
    DateTime eventEndDate = DateTime.parse(event.endDate.split("T")[0]);
    if (eventEndDate.compareTo(DateTime.now()) < 0) {
      _eventPassed = true;
    } else if (eventStartDate.compareTo(DateTime.now()) < 0 &&
        eventEndDate.compareTo(DateTime.now()) > 0) {
      _isEventOnGoing = true;
    }

    print("${eventEndDate.compareTo(DateTime.now())} evento");

    return SingleChildScrollView(
      child: Container(
        // height: double.infinity,
        height: MediaQuery.of(context).size.height * 0.6,
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        decoration: BoxDecoration(
          // color: Colors.red,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              capitalize(event.eventTitle),
              style: TextStyle(
                color: kPrimaryColor,
                fontSize: 22,
                fontWeight: FontWeight.w600,
              ),
            ),
            // SizedBox(
            //   height: 12,
            // ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              height: size.height * 0.3,
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl: event.imageURL,
                placeholder: (context, url) => SpinKitThreeBounce(
                  size: 34,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin: EdgeInsets.all(4),
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                    );
                  },
                ),
                errorWidget: (context, url, error) => Image.asset(
                  'assets/theimages/event_icon_orange.png',
                  fit: BoxFit.scaleDown,
                  scale: 20,
                  color: Colors.orangeAccent,
                ),
              ),
            ),

            !eventStartDate.isAtSameMomentAs(eventEndDate)
                ? Text(
                    " ${months[eventStartDate.month]} ${eventStartDate.day}, ${eventStartDate.year} -  ${months[eventEndDate.month]} ${eventEndDate.day}, ${eventEndDate.year}",
                    style: TextStyle(
                        color: Colors.orangeAccent,
                        fontSize: size.width * 0.036),
                  )
                : Text(
                    " ${months[eventStartDate.month]} ${eventStartDate.day}, ${eventStartDate.year}",
                    style: TextStyle(
                        color: Colors.orangeAccent,
                        fontSize: size.width * 0.036),
                  ),
            SizedBox(
              height: 10,
            ),

            Text(
              "Starts at ${DateFormat('hh:mm a').format(eventStartDate)}",
              style: TextStyle(color: Colors.orangeAccent, fontSize: 12),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 6.0),
              child: _eventPassed
                  ? Text(
                      "event passed",
                      style: TextStyle(
                          color: Colors.redAccent, fontWeight: FontWeight.w300),
                    )
                  : _isEventOnGoing
                      ? Text(
                          "event ongoing",
                          style: TextStyle(
                              color: Colors.greenAccent,
                              fontWeight: FontWeight.w300),
                        )
                      : Container(),
            ),
            Expanded(
              child: Text(event.eventDescription),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
