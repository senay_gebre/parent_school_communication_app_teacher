import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/student.dart';
import 'package:login/src/provider/events_provider.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/provider/user_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/common/sidebar_menu.dart';
import 'package:login/src/ui/pages/home/event_feed.dart';
import 'package:login/src/ui/pages/message/messaging_page.dart';
import 'package:login/src/ui/pages/profile/prof_components/settings_page.dart';
import 'package:login/src/ui/pages/student/student_screen.dart';
import 'package:provider/provider.dart';

import '../../provider/user_provider.dart';

class HomeScreen extends StatefulWidget {
  static const String id = 'home_screen';
  //final UserFile currentUser;
  // HomeScreen(this.currentUser);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  int _page = 0;

  UserProvider userProvider = UserProvider();
  final storage = new FlutterSecureStorage();
  PageController pageController;
  StudentProvider studentProvider = StudentProvider();

  List<Student> studentList;
  AuthMethods _authMethods = AuthMethods();

  var classList;
  var singleClass;

  @override
  void initState() {
    super.initState();

    refereshCurrentUser();
    refreshAllEvents();
    refreshUpcomingEvents();
    pageController = PageController();

    // refreshStudentList();

    pageController = PageController();
  }

  void refreshAllEvents() async {
    print("object");
    Provider.of<EventsProvider>(context, listen: false)
        .setPageNumberAE(pageNumberAE: 0);
    await Provider.of<EventsProvider>(context, listen: false)
        .refreshAllEvents();
  }

  void refreshUpcomingEvents() async {
    print("object");
    Provider.of<EventsProvider>(context, listen: false)
        .setPageNumberUP(pageNumberUP: 0);
    await Provider.of<EventsProvider>(context, listen: false)
        .refreshUpcomingEvents();
  }

  void refereshCurrentUser() async {
    userProvider = Provider.of<UserProvider>(context, listen: false);
    await userProvider.refreshUser();
  }

  void onPageChanged(int page) {
    setState(() {
      _page = page;
    });
  }

  void navigationTapped(int page) {
    pageController.jumpToPage(page);
  }

  // void refreshStudentList() async {
  //   studentProvider = Provider.of<StudentProvider>(context, listen: false);
  //   await studentProvider.refreshStudentList(null);
  // }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // print(
    //     "${userProvider.getUser.firstName} ma nigaa ma niga ma niga ma niga ma niga");
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);

    return userProvider != null
        ? Scaffold(
            // appBar: AppBar(
            //   actions: [
            //     IconButton(
            //       onPressed: () {},
            //       icon: Icon(Icons.more_vert),
            //       iconSize: 22,
            //     ),
            //   ],
            //   elevation: 0.0,
            // ),
            body: PageView(
              children: <Widget>[
                EventFeed(),
                StudentScreen(),
                MessagingPage(),
                SettingsPage(),
              ],
              //new code
              controller: pageController,
              onPageChanged: (index) {
                setState(() => _currentIndex = index);
              },
              // physics: NeverScrollableScrollPhysics(),
            ),
            drawer: SideBarMenu(),
            bottomNavigationBar: BottomNavigationBar(
              elevation: 0.0,
              selectedItemColor: kPrimaryColor,
              // showSelectedLabels: false,
              showUnselectedLabels: false,
              type: BottomNavigationBarType.fixed,
              onTap: navigationTapped,
              currentIndex: _page,

              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: "Home",
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.school),
                  label: "Student",
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.message),
                  label: "message",
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.settings),
                  label: "Settings",
                ),
              ],
            ),
          )
        : LoadingPage();
  }
}
