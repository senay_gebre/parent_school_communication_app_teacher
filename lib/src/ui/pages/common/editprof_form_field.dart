import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';

class EditProfileField extends StatelessWidget {
  final Function onChanged;

  final String labelText;
  final Function validator;
  final keyboardType;
  final hintText;

  EditProfileField({
    this.labelText,
    this.onChanged,
    this.validator,
    this.keyboardType,
    this.hintText,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextFormField(
          validator: validator,
          keyboardType: keyboardType,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: hintText,
          ),
          onChanged: onChanged),
    );
  }
}
