import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/user_file.dart';
import 'package:login/src/provider/user_provider.dart';
import 'package:login/src/ui/pages/Auth/login/login_page.dart';
import 'package:login/src/ui/pages/profile/edit_profile_page.dart';
import 'package:provider/provider.dart';

class SideBarMenu extends StatelessWidget {
  final storage = FlutterSecureStorage();
  final UserProvider userProvider = UserProvider();

  String capitalizeFirstLetter(String s) =>
      (s?.isNotEmpty ?? false) ? '${s[0].toUpperCase()}${s.substring(1)}' : s;
  String capitalize(String string) {
    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  // final storage = new FlutterSecureStorage();

  // var _user;

  // getUser() async {
  //   _user = await storage.read(key: "user");
  //   user = UserFile.fromMap(jsonDecode(_user.read(key: "user")));
  // }

  ListTile _drawerItems(
      {Function onTap, IconData icon, String drawerItemName}) {
    return ListTile(
      onTap: onTap,
      leading: Icon(
        icon,
        color: kDrawerListTileColor,
        size: 24,
      ),
      title: Align(
        alignment: Alignment(-1.2, 0),
        child: Text(
          drawerItemName,
          style: TextStyle(color: kDrawerListTileColor, fontSize: 14),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // var vuser = userProvider.getUser.currentAddress;
    // print("${vuser} bbbbbbbbb");
    return SafeArea(
      child: Container(
        // color: Color(0xff046487),
        decoration: BoxDecoration(color: Colors.red),
        width: MediaQuery.of(context).size.width * 0.8,
        child: Consumer<UserProvider>(
          builder: (context, userProvider, child) {
            return Drawer(
              elevation: 50,
              child: Container(
                color: Color(0xffdeeaf2),
                child: ListView(
                  children: <Widget>[
                    DrawerHeader(
                      margin: EdgeInsets.all(0),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: kPrimaryColor,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.all(5),
                            child: CircleAvatar(
                              maxRadius:
                                  MediaQuery.of(context).size.aspectRatio * 60,
                              backgroundColor: Colors.white,
                              backgroundImage: userProvider
                                          .getUser.profilePic !=
                                      null
                                  ? NetworkImage(
                                      userProvider.getUser.profilePic)
                                  : NetworkImage(
                                      'https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png'),
                            ),
                            decoration: BoxDecoration(
                              color: kPrimaryColor,
                            ),
                          ),
                          SizedBox(
                            height: 1,
                          ),
                          Text(
                            capitalizeFirstLetter(
                                    userProvider.getUser.firstName.toString()) +
                                " " +
                                capitalizeFirstLetter(
                                    userProvider.getUser.lastName.toString()),
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            'Parent',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w300),
                          ),
                          SizedBox(
                            height: 11,
                          ),
                        ],
                      ),
                    ),
                    _drawerItems(
                        onTap: () {},
                        icon: Icons.notifications,
                        drawerItemName: "Notification"),
                    _drawerItems(
                        onTap: () {},
                        icon: Icons.people,
                        drawerItemName: "Invite Parent"),
                    _drawerItems(
                        onTap: () {},
                        icon: Icons.fact_check,
                        drawerItemName: "Attendance"),
                    _drawerItems(
                        onTap: () {},
                        icon: Icons.menu_book,
                        drawerItemName: "Communication Book"),
                    _drawerItems(
                        onTap: () {},
                        icon: Icons.grade,
                        drawerItemName: "Grade Report"),
                    Divider(
                      color: kPrimaryColor.withOpacity(0.4),
                      height: 50,
                    ),
                    _drawerItems(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EditProfilePage()),
                          );
                        },
                        icon: Icons.settings,
                        drawerItemName: "Settings"),
                    _drawerItems(
                        onTap: () {},
                        icon: Icons.info,
                        drawerItemName: "About"),
                    _drawerItems(
                        onTap: () async {
                          await storage.deleteAll();

                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      LoginPage()),
                              (Route<dynamic> route) => false);
                        },
                        icon: Icons.logout,
                        drawerItemName: "Log out"),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
