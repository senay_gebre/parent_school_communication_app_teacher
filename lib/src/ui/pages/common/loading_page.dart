import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:login/src/constants/constants.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 0),
      width: double.infinity,
      height: double.infinity,
      color: Colors.white.withOpacity(0.2),
      child: SpinKitThreeBounce(
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.all(4),
            child: DecoratedBox(
              decoration: BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.circular(50)),
            ),
          );
        },
      ),
    );
  }
}
