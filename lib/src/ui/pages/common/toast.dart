import 'package:flutter/material.dart';

class CustomToast extends StatelessWidget {
  final IconData icon;
  final String message;
  final Color color;
  CustomToast({this.message, this.icon, this.color});

  @override
  Widget build(BuildContext context) {
    //FocusManager.instance.primaryFocus.unfocus();
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: color.withOpacity(0.8),
      ),
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(icon),
            SizedBox(
              width: 12.0,
            ),
            Text(message),
          ],
        ),
      ),
    );
  }
}
