import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';

class CustomFormField extends StatelessWidget {
  final Function onChanged;

  final String labelText;
  final Function validator;
  final keyboardType;
  final suffixIcon;
  bool obscureText = false;
  final controller;

  CustomFormField(
      {this.labelText,
      this.onChanged,
      this.validator,
      this.keyboardType,
      this.suffixIcon,
      this.controller,
      this.obscureText = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: TextFormField(
        controller: controller,
        obscureText: obscureText,
        keyboardType: keyboardType,
        textCapitalization: TextCapitalization.words,
        decoration: kAuthInputDecoration.copyWith(labelText: labelText),
        validator: validator,
        onChanged: onChanged,
      ),
    );
  }
}
