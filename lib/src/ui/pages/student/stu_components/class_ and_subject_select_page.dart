import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/ui/pages/student/evaluations/grading_page.dart';
import 'package:login/src/ui/pages/student/rating/progress_page.dart';
import 'package:provider/provider.dart';

class ClassAndSubjectSelectPage extends StatefulWidget {
  final route;
  ClassAndSubjectSelectPage({this.route});
  @override
  _ClassAndSubjectSelectPageState createState() =>
      _ClassAndSubjectSelectPageState();
}

class _ClassAndSubjectSelectPageState extends State<ClassAndSubjectSelectPage> {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  var _selectedClass;
  String _selectedSubject;

  @override
  Widget build(BuildContext context) {
    print(widget.route);
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: ListTile(
                    dense: true,
                    title: Text(
                      "Select Class",
                      style: TextStyle(color: kPrimaryColor, fontSize: 28.0),
                    ),
                    subtitle: Text(
                      "select a class you want to evaluate or view evaluations.",
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 23,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/classroom_icon.png"),
                    color: Colors.orange.withOpacity(0.6),
                    size: 80.0,
                  ),
                )
              ],
            ),
            Consumer<TeacherClassProvider>(
              builder: (context, teacherClassProvider, child) {
                return ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) => Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _selectedClass =
                                teacherClassProvider.teacherClassList[index];
                            _selectedSubject = null;
                          });
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              _selectedClass ==
                                      teacherClassProvider
                                          .teacherClassList[index]
                                  ? kPrimaryColor
                                  : Colors.white),
                          overlayColor: MaterialStateProperty.all(
                            kPrimaryColor.withOpacity(0.2),
                          ),
                          padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(
                              horizontal: 60.0,
                              vertical: 15.0,
                            ),
                          ),
                        ),
                        child: Text(
                          "Class ${teacherClassProvider.teacherClassList[index].grade.toString()}${capitalize(teacherClassProvider.teacherClassList[index].section.toString())}",
                          style: TextStyle(
                              color: _selectedClass ==
                                      teacherClassProvider
                                          .teacherClassList[index]
                                  ? Colors.white
                                  : kPrimaryColor),
                        ),
                      ),
                    ),
                  ),
                  itemCount: teacherClassProvider.teacherClassList.length,
                );
              },
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.5,
              padding: EdgeInsets.only(top: 10),
              color: Colors.white,
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    margin: EdgeInsets.only(top: 30),
                    decoration: BoxDecoration(
                      color: kPrimaryColor.withOpacity(0.2),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        dense: true,
                        title: Text(
                          "Select Subject",
                          style:
                              TextStyle(color: kPrimaryColor, fontSize: 28.0),
                        ),
                        subtitle: Text(
                          "select a subject you teach in a class.",
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    right: 20,
                    child: ImageIcon(
                      AssetImage("assets/icons/bookshelf_icon.png"),
                      size: 90.0,
                      color: Colors.orange.withOpacity(0.6),
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(
                        height: 100.0,
                      ),
                      _selectedClass != null
                          ? Expanded(
                              child: Container(
                                margin: EdgeInsets.symmetric(vertical: 12.0),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20.0),
                                  child: Center(
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      shrinkWrap: true,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        var subject;

                                        subjectList.forEach((element) {
                                          print(
                                              "${_selectedClass.subjects[index]} bitiko");
                                          if (element['subject_name']
                                              .toLowerCase()
                                              .contains(_selectedClass
                                                  .subjects[index]
                                                  .toLowerCase())) {
                                            subject = element;
                                          }
                                        });

                                        return Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  _selectedSubject =
                                                      subject['subject_name'];
                                                });
                                              },
                                              child: Container(
                                                height: 120.0,
                                                width: 120.0,
                                                decoration: BoxDecoration(
                                                  color: _selectedSubject ==
                                                          subject[
                                                              'subject_name']
                                                      ? kPrimaryColor
                                                      : Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                child: Stack(
                                                  children: [
                                                    Center(
                                                      child: ImageIcon(
                                                        AssetImage(subject[
                                                            'subject_icon']),
                                                        color: _selectedSubject ==
                                                                subject[
                                                                    'subject_name']
                                                            ? Colors.white
                                                            : kPrimaryColor,
                                                        size: 40,
                                                      ),
                                                    ),
                                                    Align(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: Text(
                                                        subject['subject_name'],
                                                        style: TextStyle(
                                                          color: _selectedSubject ==
                                                                  subject[
                                                                      'subject_name']
                                                              ? Colors.white
                                                              : kPrimaryColor,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                      itemCount: _selectedClass.subjects.length,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0),
                                child: Text(
                                  "Please select your class first and you subjects will be diplayed here.",
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ),
                      _selectedClass != null && _selectedSubject != null
                          ? Padding(
                              padding: EdgeInsets.symmetric(vertical: 8.0),
                              child: ElevatedButton(
                                onPressed: () {
                                  Provider.of<EvaluationProvider>(context,
                                          listen: false)
                                      .setSelectedClass(
                                          selectedClass: _selectedClass);
                                  Provider.of<EvaluationProvider>(context,
                                          listen: false)
                                      .setSelectedSubject(
                                          subject: _selectedSubject);
                                  Navigator.pushNamed(
                                      context,
                                      widget.route == 'grading_page'
                                          ? GradingPage.id
                                          : widget.route == 'progress_page'
                                              ? ProgressPage.id
                                              : null);
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.white),
                                  overlayColor: MaterialStateProperty.all(
                                    kPrimaryColor.withOpacity(0.2),
                                  ),
                                  padding: MaterialStateProperty.all(
                                    EdgeInsets.symmetric(
                                      horizontal: 60.0,
                                      vertical: 15.0,
                                    ),
                                  ),
                                ),
                                child: Text(
                                  "Continue",
                                  style: TextStyle(
                                    color: kPrimaryColor,
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
