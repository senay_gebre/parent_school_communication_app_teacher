import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/attendance.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/model/student.dart';
import 'package:login/src/provider/attendance_provider.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/student/take_attendance/attendance_calendar.dart';
import 'package:login/src/ui/pages/student/take_attendance/take_attendance_page.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TAHomeScreen extends StatefulWidget {
  final selectedClass;
  TAHomeScreen({@required this.selectedClass});
  // final String crid;
  // TakeAttendancePage(this.crid);
  @override
  _TAHomeScreenState createState() => _TAHomeScreenState();
}

class _TAHomeScreenState extends State<TAHomeScreen> {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  String crid = "";
  bool isChecked = true;
  ResponseModel _responseModel = ResponseModel();

  bool isLoading = false;

  Map attendanceMap = <String, dynamic>{};
  List<Attendance> attendanceList = [];

  List<Map> listOfAttendanceTaken = [];
  Map singleStudentAttendance = <String, dynamic>{};

  List<Student> _studentList;

  AuthMethods _authMethods = AuthMethods();
  List<bool> isSelected = [false, false, false, false];

  @override
  void initState() {
    super.initState();
    // fetchStudents();
    crid = widget.selectedClass.crid;
  }

  // void fetchStudents() async {
  //   await Provider.of<StudentProvider>(context, listen: false)
  //       .refreshStudentList();

  //   _studentList =
  //       Provider.of<StudentProvider>(context, listen: false).studentList;
  //   if (_studentList != null) {
  //     for (var singleStudent in _studentList) {
  //       Attendance _attendance = Attendance();
  //       _attendance.stuId = singleStudent.studid;
  //       attendanceList.add(_attendance);
  //     }
  //     Provider.of<AttendanceProvider>(context, listen: false)
  //         .refreshAttendanceList(attendanceList: attendanceList);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            color: Colors.grey.shade200,
            height: 34,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.calendar_today,
                        color: kPrimaryColor,
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        "${DateFormat.yMMMMd().format(DateTime.now())}",
                        style: TextStyle(color: kPrimaryColor),
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TakeAttendancePage(
                                  selectedClass: widget.selectedClass)));
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4.0),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 4),
                        decoration: BoxDecoration(
                          color: kPrimaryColor,
                        ),
                        child: Text(
                          "Take attendance",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Consumer<StudentProvider>(
                builder: (context, studentProvider, child) {
              final _studentList = studentProvider.studentList;

              return ListView.builder(
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        Provider.of<AttendanceProvider>(context, listen: false)
                            .setAttendanceGetData(
                                crid: _studentList[index].crid,
                                prevMonth: 0,
                                stuid: _studentList[index].studid);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AttendanceCalendar(
                              firstName: _studentList[index].firstName,
                              lastName: _studentList[index].lastName,
                            ),
                          ),
                        );
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Container(
                          color: Colors.grey.shade300,
                          child: ListTile(
                            dense: true,
                            leading: CircleAvatar(
                              radius: 18,
                              backgroundColor: Colors.grey,
                            ),
                            title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${capitalize(_studentList[index].firstName)} ${capitalize(_studentList[index].lastName)}",
                                  style: TextStyle(
                                      color: kPrimaryColor,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  capitalize(_studentList[index].gender),
                                  style: TextStyle(
                                    color: kPrimaryColor.withOpacity(0.7),
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                },
                itemCount: _studentList.length,
              );
            }),
          ),
        ],
      ),
    );
  }
}
