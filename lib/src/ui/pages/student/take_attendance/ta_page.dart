import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/attendance.dart';
import 'package:login/src/model/teacher_class.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/student/take_attendance/attendance_calendar.dart';
import 'package:login/src/ui/pages/student/take_attendance/ta_homescreen.dart';
import 'package:login/src/ui/pages/student/take_attendance/take_attendance_page.dart';
import 'package:provider/provider.dart';

class TAPage extends StatefulWidget {
  @override
  _TAPageState createState() => _TAPageState();
}

class _TAPageState extends State<TAPage> {
  final AuthMethods _authMethods = AuthMethods();
  List<TeacherClass> _listOfClasses;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _authMethods.viewHomeRoomClass(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            _listOfClasses = snapshot.data;
            if (_listOfClasses.length == 1) {
              // Provider.of<StudentProvider>(context, listen: false)
              //     .setCrid(_listOfClasses[0].crid);
              TeacherClass _selectedClass = _listOfClasses[0];
              // _selectedClass = widget.listOfClass[index];
              Provider.of<StudentProvider>(context, listen: false)
                  .setCrid(_selectedClass.crid);
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => TAHomeScreen(
              //               selectedClass: _selectedClass,
              //             )));
              return TAHomeScreen(
                            selectedClass: _selectedClass,
                          );
            }
            return ClassListPage(
              listOfClass: _listOfClasses,
            );
          } else {
            return LoadingPage();
          }
        },
      ),
    );
  }
}

class ClassListPage extends StatefulWidget {
  final listOfClass;

  ClassListPage({@required this.listOfClass});

  @override
  _ClassListPageState createState() => _ClassListPageState();
}

class _ClassListPageState extends State<ClassListPage> {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  var _selectedClass;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: ListTile(
                  dense: true,
                  title: Text(
                    "Select Homeroom Class",
                    style: TextStyle(color: kPrimaryColor, fontSize: 24.0),
                  ),
                  subtitle: Text(
                    "select a class you want to take or view attendance.",
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 23,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: ImageIcon(
                  AssetImage("assets/icons/classroom_icon.png"),
                  color: Colors.orange.withOpacity(0.6),
                  size: 80.0,
                ),
              )
            ],
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _selectedClass = widget.listOfClass[index];
                            Provider.of<StudentProvider>(context, listen: false)
                                .setCrid(_selectedClass.crid);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TAHomeScreen(
                                          selectedClass: _selectedClass,
                                        )));
                          });
                        },
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.white),
                          overlayColor: MaterialStateProperty.all(
                            kPrimaryColor.withOpacity(0.2),
                          ),
                          padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(
                              horizontal: 60.0,
                              vertical: 15.0,
                            ),
                          ),
                        ),
                        child: Text(
                          "Class ${widget.listOfClass[index].grade.toString()}${capitalize(widget.listOfClass[index].section.toString())}",
                          style: TextStyle(color: kPrimaryColor),
                        ),
                      ),
                    ),
                  );
                },
                itemCount: widget.listOfClass.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
