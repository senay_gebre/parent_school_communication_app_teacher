import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/attendance.dart';
import 'package:login/src/model/response_model.dart';
import 'package:intl/intl.dart';
import 'package:login/src/provider/attendance_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:provider/provider.dart';
import 'package:popup_menu/popup_menu.dart';
import 'package:jiffy/jiffy.dart';

class AttendanceCalendar extends StatefulWidget {
  final ResponseModel responseModel;
  final firstName;
  final lastName;

  AttendanceCalendar({
    this.responseModel,
    this.firstName,
    this.lastName,
  });

  @override
  _AttendanceCalendarState createState() => _AttendanceCalendarState();
}

class _AttendanceCalendarState extends State<AttendanceCalendar> {
  int prevMonth = 0;
  List updatedAttendances = [];
  AuthMethods _authMethods = AuthMethods();
  List<MenuItem> menutItems = [
    MenuItem(
      title: 'present',
      image: Container(
        decoration: BoxDecoration(
          color: Colors.green,
          shape: BoxShape.circle,
        ),
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Icon(
            Icons.check,
            size: 23,
          ),
        ),
      ),
    ),
    MenuItem(
      title: 'late',
      image: Container(
        decoration: BoxDecoration(
          color: Colors.yellow,
          shape: BoxShape.circle,
        ),
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Icon(
            Icons.access_time_outlined,
            size: 23,
          ),
        ),
      ),
    ),
    MenuItem(
      title: 'absent',
      image: Container(
        decoration: BoxDecoration(
          color: Colors.red,
          shape: BoxShape.circle,
        ),
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Icon(
            Icons.close,
            size: 23,
          ),
        ),
      ),
    ),
    MenuItem(
      title: 'permit',
      image: Container(
        decoration: BoxDecoration(
          color: Colors.blueAccent,
          shape: BoxShape.circle,
        ),
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Icon(
            Icons.local_parking,
            size: 23,
          ),
        ),
      ),
    ),
  ];
  List<Attendance> listOfAttendance = [];
  DateTime defaultDate = DateTime.now();

  int numberOfDaysInAmonth = 0;

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1, s.length);
  @override
  void initState() {
    super.initState();
    // calendar += listOfDays;
    print("${defaultDate} kana2");

    refreshAttendanceList();
  }

  void refreshAttendanceList() async {
    Provider.of<AttendanceProvider>(context, listen: false)
        .setSelectedDate(defaultDate);
    Provider.of<AttendanceProvider>(context, listen: false).constructCalendar();
    await Provider.of<AttendanceProvider>(context, listen: false)
        .refreshAttendnaceList();
  }

  final List<String> listOfDays = [
    "Sun",
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat"
  ];

  void showPopup(
      {Offset offset,
      List<MenuItem> newMenuItems,
      Attendance attendance,
      String crid}) {
    Attendance originalAttendance = attendance;
    PopupMenu menu = PopupMenu(
      backgroundColor: Colors.grey.shade200,
      lineColor: Colors.transparent,
      maxColumn: 3,
      items: newMenuItems,
      onClickMenu: (value) {
        switch (value.menuTitle) {
          case "present":
            attendance.present = true;
            attendance.permitted = false;
            attendance.sLate = false;
            break;
          case "late":
            attendance.present = false;
            attendance.permitted = false;
            attendance.sLate = true;
            break;
          case "permit":
            attendance.present = false;
            attendance.permitted = true;
            attendance.sLate = false;
            break;
          default:
            attendance.present = false;
            attendance.permitted = false;
            attendance.sLate = false;
        }

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text("Do you want to confirm the change?"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("No")),
                  TextButton(
                      onPressed: () async {
                        await _authMethods.editAttendance(attendance, crid);
                        setState(() {});
                        Navigator.pop(context);
                      },
                      child: Text("Yes"))
                ],
              );
            });
      },
    );
    menu.show(rect: Rect.fromPoints(offset, offset));
  }

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    PopupMenu.context = context;

    print("${defaultDate} mekashaw");

    return Scaffold(
      appBar: AppBar(),
      body: Consumer<AttendanceProvider>(
        builder: (context, attendanceProvider, child) {
          int numberOfPresentDays = 0;
          int numberOfLateDays = 0;
          int numberOfPremitDays = 0;
          int numberOfAbsentDays = 0;
          var singleAttendance;
          for (singleAttendance in attendanceProvider.listOfAttendance) {
            if (singleAttendance.present) {
              numberOfPresentDays++;
            } else if (singleAttendance.permitted) {
              numberOfPremitDays++;
            } else if (singleAttendance.sLate) {
              numberOfLateDays++;
            } else {
              numberOfAbsentDays++;
            }
            print("${singleAttendance.date.split('-')[2]} babyy");

            attendanceProvider.updateCalendar(singleAttendance);
          }

          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            // mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Expanded(
                flex: 8,
                child: Container(
                  color: kPrimaryColor,
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Center(
                          child: Text(
                            "${capitalize(widget.firstName)} ${capitalize(widget.lastName)}",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      ),
                      Expanded(child: SizedBox()),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: buildIndicator(
                                indicatorText: "Present",
                                attendanceCount: numberOfPresentDays,
                                indicatorColor: Colors.green),
                          ),
                          Expanded(
                            child: buildIndicator(
                                indicatorText: "Late",
                                attendanceCount: numberOfLateDays,
                                indicatorColor: Colors.yellow),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: buildIndicator(
                                indicatorText: "Permission",
                                attendanceCount: numberOfPremitDays,
                                indicatorColor: Colors.blue),
                          ),
                          Expanded(
                            child: buildIndicator(
                                indicatorText: "Absent",
                                attendanceCount: numberOfAbsentDays,
                                indicatorColor: Colors.red),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        icon: Icon(Icons.arrow_back_ios),
                        onPressed: () {
                          defaultDate = DateTime(defaultDate.year,
                              defaultDate.month - 1, defaultDate.day);
                          int difNum = Jiffy([
                            DateTime.now().year,
                            DateTime.now().month,
                            DateTime.now().day
                          ]).diff(
                              Jiffy([
                                defaultDate.year,
                                defaultDate.month,
                                defaultDate.day
                              ]),
                              Units.MONTH);
                          if (difNum >= 0) {
                            prevMonth = difNum;
                            Provider.of<AttendanceProvider>(context,
                                    listen: false)
                                .setAttendanceGetData(prevMonth: prevMonth);
                            attendanceProvider.refreshAttendnaceList();
                          }
                        }),
                    Text("${DateFormat.yMMMM().format(defaultDate)}"),
                    IconButton(
                        icon: Icon(Icons.arrow_forward_ios),
                        onPressed: () {
                          defaultDate = DateTime(defaultDate.year,
                              defaultDate.month + 1, defaultDate.day);
                          int difNum = Jiffy([
                            DateTime.now().year,
                            DateTime.now().month,
                            DateTime.now().day
                          ]).diff(
                              Jiffy([
                                defaultDate.year,
                                defaultDate.month,
                                defaultDate.day
                              ]),
                              Units.MONTH);
                          if (difNum >= 0) {
                            prevMonth = difNum;
                            Provider.of<AttendanceProvider>(context,
                                    listen: false)
                                .setAttendanceGetData(prevMonth: prevMonth);
                            attendanceProvider.refreshAttendnaceList();
                          }
                        }),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Center(
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Text(listOfDays[index]);
                    },
                    itemCount: 7,
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(
                          width: MediaQuery.of(context).size.width * 0.076);
                    },
                  ),
                ),
              ),
              Expanded(
                flex: 15,
                child: GridView.builder(
                  itemCount: attendanceProvider.calendar.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 7,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    int attendanceTypeIndex;
                    var attendanceColor = Colors.white;

                    if (attendanceProvider.calendar[index].runtimeType
                            .toString() ==
                        "Attendance") {
                      if (attendanceProvider.calendar[index].present) {
                        attendanceTypeIndex = 0;
                        attendanceColor = Colors.green;
                      } else if (attendanceProvider.calendar[index].permitted) {
                        attendanceTypeIndex = 3;

                        attendanceColor = Colors.blue;
                      } else if (attendanceProvider.calendar[index].sLate) {
                        attendanceTypeIndex = 1;

                        attendanceColor = Colors.yellow;
                      } else {
                        attendanceTypeIndex = 2;

                        attendanceColor = Colors.red;
                      }

                      return Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: GestureDetector(
                          onTapUp: (TapUpDetails details) {
                            List<MenuItem> newMenuItems = [];
                            for (int i = 0; i <= menutItems.length - 1; i++) {
                              if (i != attendanceTypeIndex) {
                                print(i);
                                newMenuItems.add(menutItems[i]);
                              }
                            }
                            print(newMenuItems);

                            String crid = Provider.of<AttendanceProvider>(
                                    context,
                                    listen: false)
                                .crid;

                            showPopup(
                                offset: details.globalPosition,
                                newMenuItems: newMenuItems,
                                crid: crid,
                                attendance: attendanceProvider.calendar[index]);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: attendanceColor,
                              border: Border.all(color: Colors.blueAccent),
                            ),
                            child: Center(
                              child: Text(
                                attendanceProvider.calendar[index].date
                                    .split('-')[2],
                                style: TextStyle(color: kPrimaryColor),
                              ),
                            ),
                          ),
                        ),
                      );
                    } else if (attendanceProvider.calendar[index] == "0") {
                      return Text(" ");
                    }
                    return Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.blueAccent),
                        ),
                        child: Center(
                            child: Text(
                                attendanceProvider.calendar[index].toString())),
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Container buildIndicator(
      {String indicatorText, Color indicatorColor, int attendanceCount}) {
    return Container(
      margin: EdgeInsets.all(
        6.0,
      ),
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.withOpacity(0.4),
      ),
      child: Row(
        children: [
          Container(
            // height: 23,
            width: 25,
            decoration: BoxDecoration(
              color: indicatorColor,
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(width: 6),
          Text(
            indicatorText,
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
          Expanded(child: SizedBox()),
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Text(
              attendanceCount.toString(),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
