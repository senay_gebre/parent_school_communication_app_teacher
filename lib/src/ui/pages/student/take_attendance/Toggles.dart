import 'package:flutter/material.dart';
import 'package:login/src/provider/attendance_provider.dart';
import 'package:provider/provider.dart';

class ToggleButtonsList extends StatefulWidget {
  final stuidForAttendance;
  ToggleButtonsList({this.stuidForAttendance});
  @override
  _ToggleButtonsListState createState() => _ToggleButtonsListState();
}

class _ToggleButtonsListState extends State<ToggleButtonsList> {
  String stuid;
  List<bool> isSelected = [true, false, false, false];

  FocusNode focusNodeButton1 = FocusNode();
  FocusNode focusNodeButton2 = FocusNode();
  FocusNode focusNodeButton3 = FocusNode();
  FocusNode focusNodeButton4 = FocusNode();

  List<FocusNode> focusToggle;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    stuid = widget.stuidForAttendance;
    focusToggle = [
      focusNodeButton1,
      focusNodeButton2,
      focusNodeButton3,
      focusNodeButton4,
    ];
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    focusNodeButton1.dispose();
    focusNodeButton2.dispose();
    focusNodeButton3.dispose();
    focusNodeButton4.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(
        "${Provider.of<AttendanceProvider>(context, listen: false).listOfAddAttendance} tome");
    return ToggleButtons(
      color: Colors.white,
      selectedColor: Colors.white,
      fillColor: Colors.grey.shade300,
      splashColor: Colors.grey.shade300,
      highlightColor: Colors.grey.shade300,
      borderColor: Colors.grey.shade300,
      borderWidth: 0,
      selectedBorderColor: Colors.grey.shade300,
      renderBorder: true,
      constraints: BoxConstraints(minWidth: 37),
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
          bottomLeft: Radius.circular(25),
          topRight: Radius.circular(25)),
      disabledColor: Colors.blueGrey,
      disabledBorderColor: Colors.blueGrey,
      focusColor: Colors.red,
      focusNodes: focusToggle,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: isSelected[0] == true ? Colors.green : Colors.grey,
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: Icon(
              Icons.check,
              size: 23,
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: isSelected[1] == true ? Colors.yellow : Colors.grey,
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: Icon(
              Icons.access_time,
              size: 23,
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: isSelected[2] == true ? Colors.red : Colors.grey,
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: Icon(
              Icons.close,
              size: 23,
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: isSelected[3] == true ? Colors.blueAccent : Colors.grey,
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: Icon(
              Icons.local_parking,
              size: 23,
            ),
          ),
        ),
      ],
      isSelected: isSelected,
      onPressed: (int index) {
        print(stuid);
        setState(() {
          for (int buttonIndex = 0;
              buttonIndex < isSelected.length;
              buttonIndex++) {
            if (buttonIndex == index) {
              isSelected[buttonIndex] = true;
              Provider.of<AttendanceProvider>(context, listen: false)
                  .updateValue(stuid: stuid, index: buttonIndex);
            } else {
              isSelected[buttonIndex] = false;
              // Provider.of<AttendanceProvider>(context)
              //     .updateValue(stuid: stuid, index: buttonIndex, value: true);
            }
          }
        });
      },
    );
  }
}
