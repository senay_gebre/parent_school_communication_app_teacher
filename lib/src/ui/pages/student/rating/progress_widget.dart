import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/rating.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:provider/provider.dart';

class ProgressWidget extends StatelessWidget {
  ProgressWidget({
    this.rating,
  });

  final Rating rating;

  // ProgressWidget({this.rating});

  @override
  Widget build(BuildContext context) {
    print(
        "${Provider.of<EvaluationProvider>(context).listOfRateToBeUpdated} kaka");
    print(
        "${rating.comSkill}, ${rating.msg}, ${rating.rateDate}, ${rating.parSkill}, ${rating.subject}, ${rating.psSkill}, ${rating.stuIdNo} mimimimimimimimimimi");
    return Container(
      color: kPrimaryColor.withOpacity(0.8),
      width: double.infinity,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                rating.msg,
                style: TextStyle(
                  color: Colors.white54,
                ),
              ),
            ),
          ),
          ProgressLine(
            rateValue: rating.comSkill,
            rateToBeUpdated: rating,
            skillType: "Communication Skill",
          ),
          ProgressLine(
            rateToBeUpdated: rating,
            rateValue: rating.psSkill,
            skillType: "Problem Solving Skill",
          ),
          ProgressLine(
            rateToBeUpdated: rating,
            rateValue: rating.parSkill,
            skillType: "Participation Skill",
          ),
        ],
      ),
    );
  }
}

class ProgressLine extends StatefulWidget {
  final String skillType;
  final int valueToBeUpdated;
  final Rating rateToBeUpdated;
  final int rateValue;
  ProgressLine({
    this.rateToBeUpdated,
    this.rateValue,
    this.valueToBeUpdated,
    this.skillType,
  });

  @override
  _ProgressLineState createState() => _ProgressLineState();
}

class _ProgressLineState extends State<ProgressLine> {
  int rateValue;
  Rating rateToBeUpdated;
  String skillType;

  @override
  void initState() {
    super.initState();
    rateValue = widget.rateValue;
    rateToBeUpdated = widget.rateToBeUpdated;
    skillType = widget.skillType;
  }

  @override
  Widget build(BuildContext context) {
    print(
        "${Provider.of<EvaluationProvider>(context).listOfRateToBeUpdated} flex");
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            skillType,
            style: TextStyle(color: Colors.white),
          ),
          SliderTheme(
            data: SliderTheme.of(context).copyWith(
              inactiveTrackColor: Colors.grey,
              activeTickMarkColor: Colors.white,
              inactiveTickMarkColor: Colors.white,
              activeTrackColor: Colors.orange,
              thumbColor: kPrimaryColor,
              overlayColor: Color(0x29EB1555),
              thumbShape: RoundSliderThumbShape(enabledThumbRadius: 8.0),
              overlayShape: RoundSliderOverlayShape(overlayRadius: 15.0),
            ),
            child: Slider(
              value: rateValue.toDouble(),
              min: 0.0,
              max: 10.0,
              divisions: 10,
              label: "${rateValue}",
              onChanged: (double newValue) {
                print("${newValue} newval");
                setState(
                  () {
                    rateValue = newValue.round();
                  },
                );
              },
              onChangeEnd: (double value) {
                print("${rateToBeUpdated} ${rateValue} ${skillType} rato");
                Provider.of<EvaluationProvider>(context, listen: false)
                    .updateRate(
                  rateToBeUpdated: rateToBeUpdated,
                  updatedValue: rateValue,
                  skillType: skillType,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
