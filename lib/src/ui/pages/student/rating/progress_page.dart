import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/rating.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/model/student.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/common/toast.dart';
import 'package:login/src/ui/pages/student/rating/progress_widget.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class ProgressPage extends StatefulWidget {
  static const String id = 'progress_page';
  @override
  _ProgressPageState createState() => _ProgressPageState();
}

class _ProgressPageState extends State<ProgressPage> {
  AuthMethods _authMethods = AuthMethods();
  ResponseModel _responseModel = ResponseModel();
  bool _isInitiallyExpanded = false;
  bool isFiltered = false;
  String query = "";

  FToast fToast;
  var listOfUpdatedRate;
  // var selectedClass;
  // String _chosenClass;
  // String _chosenSubject;

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  // List<Map<String, dynamic>> classList = [];
  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);

    // for (var singleClass
    //     in Provider.of<TeacherClassProvider>(context, listen: false)
    //         .teacherClassList) {
    //   classList.add({
    //     "class": singleClass,
    //     "nameOfClass":
    //         "${singleClass.grade.toString()}${capitalize(singleClass.section.toString())}"
    //   });
    // }
  }

  int defalutClass;
  int psSkill = 6;
  int partiSkill = 4;
  int comSkill = 9;

  @override
  Widget build(BuildContext context) {
    // print("$classList ayu ayu ayu");
    return Scaffold(
      appBar: AppBar(),
      resizeToAvoidBottomInset: false,
      floatingActionButton: Container(
        padding: EdgeInsets.all(6.0),
        height: 45,
        width: 130,
        child: FloatingActionButton.extended(
          backgroundColor: kPrimaryColor,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          onPressed: () async {
            List<Map> dataToBeSent = [];

            var updatedRateObjectList =
                Provider.of<EvaluationProvider>(context, listen: false)
                    .listOfRateToBeUpdated;

            updatedRateObjectList.forEach((element) {
              dataToBeSent.add(element.toMap());
            });

            _responseModel = await _authMethods.updateRate(dataToBeSent);

            print("${_responseModel.msg} usa");

            if (_responseModel.success) {
              fToast.showToast(
                child: CustomToast(
                  message: _responseModel.msg,
                  icon: Icons.check,
                  color: Color(0xff13e8ac),
                ),
                gravity: ToastGravity.BOTTOM,
                toastDuration: Duration(seconds: 4),
              );
            } else {
              fToast.showToast(
                child: CustomToast(
                  message: _responseModel.msg,
                  icon: Icons.error,
                  color: Color(0xffF14E4C),
                ),
                gravity: ToastGravity.BOTTOM,
                toastDuration: Duration(seconds: 4),
              );
            }
          },
          label: Text(
            "Update",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w300,
              color: Colors.white,
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Consumer<EvaluationProvider>(
                builder: (context, evaluationProvider, child) {
                  var _teacherClassList =
                      Provider.of<TeacherClassProvider>(context, listen: false)
                          .teacherClassList;
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 8.0, vertical: 6.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int index) =>
                                Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              child: GestureDetector(
                                onTap: () {
                                  evaluationProvider.setSelectedClass(
                                      selectedClass: _teacherClassList[index]);

                                  evaluationProvider.setSelectedSubject(
                                      subject: null);
                                },
                                child: Text(
                                  "Class ${_teacherClassList[index].grade.toString()}${capitalize(_teacherClassList[index].section.toString())}",
                                  style: evaluationProvider.getSelectedClass ==
                                          _teacherClassList[index]
                                      ? TextStyle(
                                          color: kPrimaryColor,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        )
                                      : TextStyle(
                                          color: Colors.grey, fontSize: 14),
                                ),
                              ),
                            ),
                            itemCount: _teacherClassList.length,
                          ),
                        ),
                        evaluationProvider.getSelectedClass != null
                            ? Expanded(
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0),
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            evaluationProvider
                                                .setSelectedSubject(
                                                    subject: evaluationProvider
                                                        .getSelectedClass
                                                        .subjects[index]);
                                          });
                                        },
                                        child: Text(
                                          capitalize(evaluationProvider
                                              .getSelectedClass
                                              .subjects[index]),
                                          style: evaluationProvider
                                                      .getSelectedSubject ==
                                                  evaluationProvider
                                                      .getSelectedClass
                                                      .subjects[index]
                                                      .toLowerCase()
                                              ? TextStyle(
                                                  color: kPrimaryColor,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600,
                                                )
                                              : TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 14),
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount: evaluationProvider
                                      .getSelectedClass.subjects.length,
                                ),
                              )
                            : Center(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20.0),
                                  child: Text(
                                    "Please select your class first and you subjects will be diplayed here.",
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ),
                      ],
                    ),
                  );
                },
              ),
            ),
            Provider.of<EvaluationProvider>(context).getSelectedClass != null &&
                    Provider.of<EvaluationProvider>(context)
                            .getSelectedSubject !=
                        null
                ? Expanded(
                    flex: 6,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: TextFormField(
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  enabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 12.0),
                                  hintText: "Search student...",
                                ),
                                onChanged: (val) {
                                  setState(() {
                                    query = val;
                                  });
                                },
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                setState(() {
                                  _isInitiallyExpanded = !_isInitiallyExpanded;
                                });
                              },
                              child: Text("Expand All"),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Consumer<StudentProvider>(
                            builder: (context, studentProvider, child) {
                              var _singleClass;
                              var myClassStudent;

                              var _selectedClassCrid =
                                  Provider.of<EvaluationProvider>(context,
                                          listen: false)
                                      .getSelectedClass
                                      .crid;
                              var _selectedSubject =
                                  Provider.of<EvaluationProvider>(context,
                                          listen: false)
                                      .getSelectedSubject;
                              print('${_selectedClassCrid} aba');

                              for (_singleClass
                                  in studentProvider.studentMapList) {
                                if (_selectedClassCrid ==
                                    _singleClass["crid"]) {
                                  if (query.isNotEmpty) {
                                    final List<Student> suggestionList = query
                                            .isEmpty
                                        ? []
                                        : _singleClass["StudentList"] != null
                                            ? _singleClass["StudentList"]
                                                .where((Student student) {
                                                String _getSrudentName = student
                                                        .firstName
                                                        .toLowerCase() +
                                                    " " +
                                                    student.lastName
                                                        .toLowerCase();
                                                String _query =
                                                    query.toLowerCase();
                                                String _getUsername =
                                                    student.idNo.toLowerCase();
                                                bool matchesName =
                                                    _getSrudentName
                                                        .contains(_query);
                                                bool matchesUsername =
                                                    _getUsername
                                                        .contains(_query);

                                                return (matchesName ||
                                                    matchesUsername);
                                              }).toList()
                                            : [];

                                    myClassStudent = suggestionList;
                                    print("${myClassStudent} miskino");
                                  } else {
                                    myClassStudent =
                                        _singleClass["StudentList"];
                                    print("${myClassStudent} miskin");
                                  }
                                }
                              }

                              return studentProvider.getStuMapCount != 0
                                  ? ListView.builder(
                                      itemBuilder: (context, index) {
                                        final Rating _rating = Rating();
                                        var singleSubjectRate;
                                        var rateOnSubject;
                                        final student = myClassStudent[index];

                                        if (myClassStudent[index]
                                            .status
                                            .isNotEmpty) {
                                          for (singleSubjectRate
                                              in myClassStudent[index].status) {
                                            if (singleSubjectRate["subject"] ==
                                                _selectedSubject) {
                                              rateOnSubject = singleSubjectRate;
                                            }
                                          }
                                        }

                                        if (rateOnSubject != null) {
                                          var ratedDate =
                                              "${rateOnSubject['date']}"
                                                  .split('T')[0];

                                          _rating.subject = _selectedSubject;
                                          _rating.msg =
                                              "Student rated on ${DateFormat.yMMMMd().format(DateTime.parse(ratedDate))}";
                                          _rating.rateDate =
                                              rateOnSubject['date'];
                                          _rating.stuIdNo = student.idNo;
                                          _rating.comSkill =
                                              rateOnSubject["skills"]
                                                  ["comSkill"];
                                          _rating.psSkill =
                                              rateOnSubject["skills"]
                                                  ["probSolving"];
                                          _rating.parSkill =
                                              rateOnSubject["skills"]
                                                  ["participation"];
                                        } else {
                                          _rating.subject = _selectedSubject;
                                          _rating.msg =
                                              "Student not rated yet.";
                                          _rating.rateDate =
                                              "${DateTime.now()}".split(' ')[0];

                                          _rating.stuIdNo = student.idNo;
                                          _rating.comSkill = 0;
                                          _rating.psSkill = 0;
                                          _rating.parSkill = 0;
                                        }

                                        return Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            child: ListTileTheme(
                                              dense: true,
                                              child: ExpansionTile(
                                                key: GlobalKey(),
                                                initiallyExpanded:
                                                    _isInitiallyExpanded,
                                                onExpansionChanged: (value) {
                                                  if (value) {
                                                    FocusManager
                                                        .instance.primaryFocus
                                                        .unfocus();
                                                  }
                                                },
                                                leading: CircleAvatar(
                                                  radius: 17,
                                                ),
                                                title: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                        "${capitalize(student.firstName)} ${capitalize(student.lastName)}"),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          "${capitalize(student.gender)}",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                        SizedBox(
                                                          width: 23,
                                                        ),
                                                        Text(
                                                          "COM: ${_rating.comSkill}",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                        Text(
                                                          "PS: ${_rating.psSkill}",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                        Text(
                                                          "PAR: ${_rating.parSkill}",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                //subtitle: Text("data"),
                                                backgroundColor: Colors.white,
                                                collapsedBackgroundColor:
                                                    Colors.white,

                                                children: [
                                                  ProgressWidget(
                                                    rating: _rating,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                      itemCount: myClassStudent.length,
                                    )
                                  : LoadingPage();
                            },
                          ),
                        )
                      ],
                    ),
                  )
                : Expanded(
                    flex: 7,
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.only(bottom: 70.0),
                        child: Text(
                          "Please select your class and subject.",
                          style:
                              TextStyle(color: Colors.orange, fontSize: 18.0),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
