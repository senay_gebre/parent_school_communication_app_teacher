import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/student/com_book/class_and_date_select_page.dart';
import 'package:login/src/ui/pages/student/com_book/com_book_page.dart';
import 'package:login/src/ui/pages/student/evaluations/grading_page.dart';
import 'package:login/src/ui/pages/student/rating/progress_page.dart';
import 'package:login/src/ui/pages/student/stu_components/class_%20and_subject_select_page.dart';
import 'package:login/src/ui/pages/student/stu_components/reusable_card.dart';
import 'package:login/src/ui/pages/student/take_attendance/ta_page.dart';
import 'package:provider/provider.dart';

class StudentScreen extends StatefulWidget {
  @override
  _StudentScreenState createState() => _StudentScreenState();
}

class _StudentScreenState extends State<StudentScreen> {
  AuthMethods _authMethods = AuthMethods();
  var classList;

  @override
  void initState() {
    super.initState();
    fetchMyClassData();
  }

  void fetchMyClassData() async {
    await Provider.of<TeacherClassProvider>(context, listen: false)
        .refreshteacherClassList()
        .then(
      (value) async {
        classList = Provider.of<TeacherClassProvider>(context, listen: false)
            .teacherClassList;

        if (classList != null) {
          await Provider.of<StudentProvider>(context, listen: false)
              .refreshStudentMapList(classList);
        }
      },
    );
  }

  // void fetchAllTeacherClass() async {
  //   await Provider.of<TeacherClassProvider>(this.context, listen: false)
  //       .refreshteacherClassList();
  // }

  @override
  Widget build(BuildContext context) {
    print(
        "${Provider.of<StudentProvider>(context).studentMapList} cococococococococococococococococ");
    return Scaffold(
      body: Container(
        //padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.all(28.0),
              child: Container(
                height: 190,
                child: Center(
                  child: Image(
                    image: AssetImage("assets/icons/logo_student_screen.png"),
                  ),
                ),
              ),
            ),
            Column(
              children: [
                ReusableCard(
                  label: "Com Book",
                  icon: Icons.library_books,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ClassAndDateSelectPage()));
                  },
                ),
                ReusableCard(
                  label: "Evaluation",
                  icon: Icons.grade,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ClassAndSubjectSelectPage(
                                  route: GradingPage.id,
                                )));
                  },
                ),
                ReusableCard(
                  label: "Student Rating",
                  icon: Icons.rate_review,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ClassAndSubjectSelectPage(
                                  route: ProgressPage.id,
                                )));
                  },
                ),
                ReusableCard(
                  label: "Attendance",
                  icon: Icons.playlist_add,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => TAPage()));
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
