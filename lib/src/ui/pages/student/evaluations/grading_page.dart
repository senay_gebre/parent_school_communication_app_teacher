import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/model/result.dart';
import 'package:login/src/model/student.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/provider/toggle_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/student/evaluations/eva_components/set_exam_property_page.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';

class GradingPage extends StatefulWidget {
  static const String id = 'grading_page';
  @override
  _GradingPageState createState() => _GradingPageState();
}

class _GradingPageState extends State<GradingPage> {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  String query = "";
  AuthMethods _authMethods = AuthMethods();
  ResponseModel _responseModel = ResponseModel();
  final _formKey = GlobalKey<FormState>();
  List<String> quarterList = ['1', '2', '3', '4'];
  String selectedQuarter;

  var listOfEditedResults = [];
  bool _editingEnabled = false;

  List studentResults;

  @override
  void initState() {
    super.initState();
    selectedQuarter = quarterList[0];
    Provider.of<EvaluationProvider>(context, listen: false)
        .setQuarter(quarter: selectedQuarter);
    refreshStudentsResult();
  }

  void refreshStudentsResult() {
    Provider.of<EvaluationProvider>(context, listen: false)
        .refreshGradeReportList();
  }

  void handleClick(String value) {
    switch (value) {
      case 'Add Grade':
        showDialog(
          context: context,
          builder: (BuildContext context) => SetExamPropertyPage(),
        );

        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("${studentResults} yesus");
    return Scaffold(
        floatingActionButton: _editingEnabled
            ? Container(
                padding: EdgeInsets.all(6.0),
                height: 45,
                width: 120,
                child: FloatingActionButton.extended(
                  backgroundColor: kPrimaryColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  onPressed: () async {
                    // bool _isEmpty = false;
                    //first clean up empty results

                    // print("${listOfAssessmentResult} habte");

                    if (_formKey.currentState.validate()) {
                      await performUpdate();
                    }
                  },
                  label: Text(
                    "Update",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w300,
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            : null,
        appBar: AppBar(
          actions: <Widget>[
            PopupMenuButton<String>(
              onSelected: handleClick,
              itemBuilder: (BuildContext context) {
                return {'Add Grade', 'Enable Editing'}.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Row(
                      children: [
                        Text(choice),
                        choice == 'Add Grade'
                            ? Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 4.0),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.black,
                                ),
                              )
                            : Consumer<ToggleProvider>(
                                builder: (context, toggleProvider, child) {
                                  return Switch(
                                    value: toggleProvider.isEditingEnabled,
                                    onChanged: (value) {
                                      toggleProvider.toggleSwitch();
                                      setState(() {
                                        _editingEnabled =
                                            toggleProvider.isEditingEnabled;
                                      });
                                    },
                                  );
                                },
                              ),
                      ],
                    ),
                  );
                }).toList();
              },
            ),
          ],
        ),
        resizeToAvoidBottomInset: false,
        body: SafeArea(
            child: SingleChildScrollView(child: Consumer<EvaluationProvider>(
          builder: (context, evaluationProvider, child) {
            print("mita");
            studentResults = evaluationProvider.responseModel.response;

            var _teacherClassList =
                Provider.of<TeacherClassProvider>(context, listen: false)
                    .teacherClassList;
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 60.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                // studentResults = Provider.of<EvaluationProvider>(
                                //         context,
                                //         listen: false)
                                //     .studentGradeReportList;
                                return Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        evaluationProvider.setSelectedClass(
                                            selectedClass:
                                                _teacherClassList[index]);

                                        evaluationProvider.setSelectedSubject(
                                            subject: null);
                                      },
                                      child: Text(
                                        "Class ${_teacherClassList[index].grade.toString()}${capitalize(_teacherClassList[index].section.toString())}",
                                        style: evaluationProvider
                                                    .getSelectedClass ==
                                                _teacherClassList[index]
                                            ? TextStyle(
                                                color: kPrimaryColor,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                              )
                                            : TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14),
                                      ),
                                    ),
                                  ),
                                );
                              },
                              itemCount: _teacherClassList.length,
                            ),
                          ],
                        ),
                      ),
                      evaluationProvider.getSelectedClass != null
                          ? Expanded(
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          studentResults = null;
                                          evaluationProvider.setSelectedSubject(
                                              subject: evaluationProvider
                                                  .getSelectedClass
                                                  .subjects[index]);
                                          evaluationProvider
                                              .refreshGradeReportList();
                                        });
                                      },
                                      child: Text(
                                        capitalize(evaluationProvider
                                            .getSelectedClass.subjects[index]),
                                        style: evaluationProvider
                                                    .getSelectedSubject ==
                                                evaluationProvider
                                                    .getSelectedClass
                                                    .subjects[index]
                                                    .toLowerCase()
                                            ? TextStyle(
                                                color: kPrimaryColor,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              )
                                            : TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14),
                                      ),
                                    ),
                                  );
                                },
                                itemCount: evaluationProvider
                                    .getSelectedClass.subjects.length,
                              ),
                            )
                          : Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0),
                                child: Text(
                                  "Please select your class first and you subjects will be diplayed here.",
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            filled: true,
                            // enabledBorder: InputBorder.none,
                            // focusedBorder: InputBorder.none,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            contentPadding:
                                const EdgeInsets.symmetric(horizontal: 6.0),
                            hintText: "Search...",
                            prefixIcon: Icon(
                              Icons.search,
                              size: 16,
                              color: kPrimaryColor,
                            ),
                          ),
                          onChanged: (val) {
                            setState(() {
                              query = val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12.0),
                        child: Row(
                          children: [
                            Icon(Icons.filter_list),
                            Text(
                              "Quarter  ",
                              style: TextStyle(
                                color: kPrimaryColor,
                              ),
                            ),
                            evaluationProvider.getSelectedClass != null &&
                                    evaluationProvider.getSelectedSubject !=
                                        null
                                ? DropdownButton<String>(
                                    focusColor: Colors.white,
                                    value: selectedQuarter,
                                    //elevation: 5,
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                    iconEnabledColor: Colors.black,
                                    items: quarterList
                                        .map<DropdownMenuItem<String>>((value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (String value) {
                                      setState(
                                        () {
                                          selectedQuarter = value;

                                          // _isLoading = true;
                                        },
                                      );
                                      evaluationProvider.setQuarter(
                                          quarter: selectedQuarter);
                                      studentResults = null;
                                      evaluationProvider
                                          .refreshGradeReportList();
                                    },
                                  )
                                : Text("#"),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                evaluationProvider.getSelectedClass != null &&
                        evaluationProvider.getSelectedSubject != null
                    ? evaluationProvider.responseModel.response != null
                        ? studentResults.isNotEmpty
                            ? SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 136.0,
                                        ),
                                        Container(
                                          width: studentResults[0]
                                                      .listOfResult
                                                      .length <=
                                                  2
                                              ? 400
                                              : studentResults.length <= 4
                                                  ? 800
                                                  : studentResults.length <= 6
                                                      ? 1000
                                                      : studentResults.length <=
                                                              8
                                                          ? 1400
                                                          : 2000,
                                          height: 40,
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                width: 126.0,
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      studentResults[0]
                                                          .listOfResult[index]
                                                          .title,
                                                      style: TextStyle(
                                                          color: Colors.orange),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 3.0),
                                                      child: Text(
                                                        "${studentResults[0].listOfResult[index].weight.toString()}",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.orange),
                                                        textAlign:
                                                            TextAlign.justify,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                            itemCount: studentResults[0]
                                                .listOfResult
                                                .length,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Form(
                                      key: _formKey,
                                      child: Consumer<StudentProvider>(
                                        builder:
                                            (context, studentProvider, child) {
                                          var _singleClass;
                                          var myClassStudent;

                                          for (_singleClass in studentProvider
                                              .studentMapList) {
                                            if (Provider.of<EvaluationProvider>(
                                                        context,
                                                        listen: false)
                                                    .getSelectedClass
                                                    .crid ==
                                                _singleClass["crid"]) {
                                              if (query.isNotEmpty) {
                                                final List<
                                                    Student> suggestionList = query
                                                        .isEmpty
                                                    ? []
                                                    : _singleClass[
                                                                "StudentList"] !=
                                                            null
                                                        ? _singleClass[
                                                                "StudentList"]
                                                            .where((Student
                                                                student) {
                                                            String
                                                                _getSrudentName =
                                                                student.firstName
                                                                        .toLowerCase() +
                                                                    " " +
                                                                    student
                                                                        .lastName
                                                                        .toLowerCase();
                                                            String _query = query
                                                                .toLowerCase();
                                                            String
                                                                _getUsername =
                                                                student.idNo
                                                                    .toLowerCase();
                                                            bool matchesName =
                                                                _getSrudentName
                                                                    .contains(
                                                                        _query);
                                                            bool
                                                                matchesUsername =
                                                                _getUsername
                                                                    .contains(
                                                                        _query);

                                                            return (matchesName ||
                                                                matchesUsername);
                                                          }).toList()
                                                        : [];

                                                myClassStudent = suggestionList;
                                                print(
                                                    "${myClassStudent} miskino");
                                              } else {
                                                myClassStudent =
                                                    _singleClass["StudentList"];
                                                print(
                                                    "${myClassStudent} miskin");
                                              }
                                            }
                                          }

                                          return myClassStudent != null
                                              ? Container(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.65,
                                                  width: studentResults[0]
                                                              .listOfResult
                                                              .length <=
                                                          2
                                                      ? 400
                                                      : studentResults.length <=
                                                              4
                                                          ? 900
                                                          : studentResults
                                                                      .length <=
                                                                  6
                                                              ? 1100
                                                              : studentResults
                                                                          .length <=
                                                                      8
                                                                  ? 1500
                                                                  : 2000, ///////////TODO

                                                  child: ListView.builder(
                                                    itemBuilder:
                                                        (context, upperIndex) {
                                                      var studentGradeReport;
                                                      var singleGrageReport;
                                                      final student =
                                                          myClassStudent[
                                                              upperIndex];

                                                      for (singleGrageReport
                                                          in studentResults) {
                                                        if (student.studid ==
                                                            singleGrageReport
                                                                .studId) {
                                                          print(
                                                              singleGrageReport
                                                                  .studId);

                                                          studentGradeReport =
                                                              singleGrageReport;
                                                        }
                                                      }

                                                      return Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              width: 130.0,
                                                              // decoration: BoxDecoration(
                                                              //   border: Border(
                                                              //     right: BorderSide(
                                                              //         width: 4.2,
                                                              //         color: Colors.black),
                                                              //   ),
                                                              // ),
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Text(
                                                                    "${capitalize(student.firstName)} ${capitalize(student.lastName)}",
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            16.0,
                                                                        color:
                                                                            kPrimaryColor),
                                                                  ),
                                                                  Text(
                                                                    "ID No: ${student.idNo}",
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            12,
                                                                        color: Colors
                                                                            .grey),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Expanded(
                                                              child: Container(
                                                                height: 50.0,
                                                                child: ListView
                                                                    .builder(
                                                                  shrinkWrap:
                                                                      true,
                                                                  scrollDirection:
                                                                      Axis.horizontal,
                                                                  physics:
                                                                      NeverScrollableScrollPhysics(),
                                                                  itemBuilder:
                                                                      (context,
                                                                          index) {
                                                                    bool
                                                                        _isExist =
                                                                        false;
                                                                    bool
                                                                        _newAdd =
                                                                        false;
                                                                    Result
                                                                        result =
                                                                        Result();
                                                                    result.title = studentGradeReport
                                                                        .listOfResult[
                                                                            index]
                                                                        .title;
                                                                    result.weight = studentGradeReport
                                                                        .listOfResult[
                                                                            index]
                                                                        .weight;
                                                                    return Container(
                                                                      width:
                                                                          127.0,
                                                                      padding:
                                                                          EdgeInsets
                                                                              .symmetric(
                                                                        horizontal:
                                                                            35.0,
                                                                      ),
                                                                      child:
                                                                          TextFormField(
                                                                        keyboardType:
                                                                            TextInputType.number,
                                                                        inputFormatters: <
                                                                            TextInputFormatter>[
                                                                          FilteringTextInputFormatter.allow(
                                                                              RegExp(r'[0-9.]')),
                                                                        ],
                                                                        validator:
                                                                            (value) {
                                                                          if (value !=
                                                                              "") {
                                                                            if (value.endsWith(".")) {
                                                                              return "invalid!";
                                                                            } else if (double.parse(value) >
                                                                                studentGradeReport.listOfResult[index].weight) {
                                                                              return 'weight';
                                                                            }
                                                                            return null;
                                                                          }
                                                                          return null;
                                                                        },
                                                                        enabled:
                                                                            _editingEnabled,
                                                                        onChanged:
                                                                            (value) {
                                                                          if (listOfEditedResults
                                                                              .isNotEmpty) {
                                                                            listOfEditedResults.forEach(
                                                                              (examElement) {
                                                                                if (examElement['examid'] == studentGradeReport.listOfResult[index].resultId) {
                                                                                  print("yes");
                                                                                  _isExist = true;
                                                                                  if (value == "") {
                                                                                    examElement['results']['taken'] = false;
                                                                                    examElement['results']['score'] = 0.0;
                                                                                  } else {
                                                                                    examElement['results']['taken'] = true;
                                                                                    examElement['results']['score'] = double.parse(value);
                                                                                  }
                                                                                }
                                                                              },
                                                                            );
                                                                          } else {
                                                                            if (value !=
                                                                                "") {
                                                                              _newAdd = true;
                                                                              result.score = double.parse(value);
                                                                              result.taken = true;
                                                                              listOfEditedResults.add({
                                                                                "grid": singleGrageReport.grid,
                                                                                "examid": studentGradeReport.listOfResult[index].resultId,
                                                                                "results": result.toMap()
                                                                              });
                                                                            } else {
                                                                              result.taken = false;
                                                                              listOfEditedResults.clear();
                                                                            }
                                                                          }

                                                                          if (!_isExist &&
                                                                              !_newAdd) {
                                                                            result.score =
                                                                                double.parse(value);
                                                                            result.taken =
                                                                                true;
                                                                            listOfEditedResults.add({
                                                                              "grid": singleGrageReport.grid,
                                                                              "examid": studentGradeReport.listOfResult[index].resultId,
                                                                              "results": result.toMap()
                                                                            });
                                                                          }
                                                                        },
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                        initialValue: studentGradeReport.listOfResult[index].taken ==
                                                                                true
                                                                            ? studentGradeReport.listOfResult[index].score.toString()
                                                                            : "",
                                                                        style: studentGradeReport.listOfResult[index].taken ==
                                                                                true
                                                                            ? TextStyle(color: Colors.black)
                                                                            : TextStyle(color: Colors.black),
                                                                        decoration:
                                                                            InputDecoration(
                                                                          floatingLabelBehavior:
                                                                              FloatingLabelBehavior.never,
                                                                          hintText: studentGradeReport.listOfResult[index].taken == false
                                                                              ? "NAN"
                                                                              : "",
                                                                          hintStyle: studentGradeReport.listOfResult[index].taken == true
                                                                              ? TextStyle(color: Colors.black)
                                                                              : TextStyle(color: Colors.red.shade400.withOpacity(0.8), fontSize: 14),
                                                                          disabledBorder:
                                                                              UnderlineInputBorder(
                                                                            borderSide:
                                                                                BorderSide(
                                                                              color: Colors.white,
                                                                            ),
                                                                          ),
                                                                          focusedBorder:
                                                                              UnderlineInputBorder(
                                                                            borderSide:
                                                                                BorderSide(color: kPrimaryColor),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    );
                                                                  },
                                                                  itemCount:
                                                                      studentGradeReport
                                                                          .listOfResult
                                                                          .length,
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      );
                                                    },
                                                    itemCount:
                                                        myClassStudent.length,
                                                  ),
                                                )
                                              : Container(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.65,
                                                  child: LoadingPage());
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Padding(
                                padding: const EdgeInsets.only(top: 100.0),
                                child: Center(
                                  child: Text(
                                    "No result has been submitted yet.",
                                    style: TextStyle(
                                      color: Colors.orange,
                                    ),
                                  ),
                                ),
                              )
                        : Container(
                            height: MediaQuery.of(context).size.height * 0.65,
                            child: LoadingPage())
                    : Padding(
                        padding: const EdgeInsets.all(68.0),
                        child: Center(
                            child:
                                Text("Please select your class and subject.")),
                      ),
              ],
            );
          },
        ))));
  }

  performUpdate() async {
    _responseModel = await _authMethods.updateResult(
        listOfEditedResults: listOfEditedResults);
    print("${_responseModel.msg} kobo");
  }
}
