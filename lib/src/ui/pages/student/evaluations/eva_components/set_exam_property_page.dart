import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/student/evaluations/AddExamResultPage.dart';
import 'package:provider/provider.dart';

class SetExamPropertyPage extends StatefulWidget {
  @override
  _SetExamPropertyPageState createState() => _SetExamPropertyPageState();
}

class _SetExamPropertyPageState extends State<SetExamPropertyPage> {
  List<String> listOfSampleWeight = ["5", "10", "15", "20", "25", "50"];
  double _weightValue;
  String title;
  var _selectedClass;
  String _selectedSubject;
  String _selectedQuarter;
  @override
  void initState() {
    super.initState();
    _selectedClass = Provider.of<EvaluationProvider>(context, listen: false)
        .getSelectedClass;
    _selectedSubject = Provider.of<EvaluationProvider>(context, listen: false)
        .getSelectedSubject;
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  var _myWeightController = TextEditingController();
  var _myTitleController = TextEditingController();


  List<Map<String, dynamic>> _listOfAddedAssessment = [];

  @override
  Widget build(BuildContext context) {
    print("${_selectedClass} select");
    return AlertDialog(
      // scrollable: true,
      actions: [
        TextButton(
          onPressed: () {
            if (_listOfAddedAssessment.isNotEmpty && _selectedQuarter != null) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddExamResultPage(
                    listOfAssessmentType: _listOfAddedAssessment,
                    academicYear: _selectedClass.academicYear,
                    quarter: _selectedQuarter,
                    selectedSubject: _selectedSubject,
                  ),
                ),
              );
            }
          },
          child: Text(
            "Finish",
            style: TextStyle(
              color:
                  _listOfAddedAssessment.isNotEmpty && _selectedQuarter != null
                      ? kPrimaryColor
                      : Colors.grey,
            ),
          ),
        )
      ],
      content: Container(
        width: double.maxFinite,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListView(
              shrinkWrap: true,
              children: <Widget>[
                Center(
                  child: Text(
                    "Evaluation Information",
                    style: TextStyle(
                      color: Colors.orange,
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: RichText(
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: "Academic Year: ",
                          style: TextStyle(color: kPrimaryColor),
                        ),
                        TextSpan(
                          text: _selectedClass.academicYear.toString(),
                          style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  children: [
                    RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: "Class: ",
                            style: TextStyle(color: kPrimaryColor),
                          ),
                          TextSpan(
                            text:
                                "${_selectedClass.grade.toString()}${capitalize(_selectedClass.section.toString())}",
                            style: TextStyle(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: "Subject: ",
                            style: TextStyle(color: kPrimaryColor),
                          ),
                          TextSpan(
                            text: capitalize(_selectedSubject),
                            style: TextStyle(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.maxFinite,
                  child: Divider(
                    color: kPrimaryColor,
                    height: 23,
                    thickness: 1.2,
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Row(
                  children: [
                    Text(
                      "Quarter  ",
                      style: TextStyle(
                        color: kPrimaryColor,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 34,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            String quarterIndex = (index + 1).toString();
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selectedQuarter = quarterIndex;
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 10.0),
                                margin: EdgeInsets.symmetric(horizontal: 1),
                                decoration: BoxDecoration(
                                  color: _selectedQuarter == quarterIndex
                                      ? kPrimaryColor
                                      : Colors.white,
                                  border: Border.all(
                                      width: 1, color: kPrimaryColor),
                                ),
                                child: Center(
                                  child: Text(
                                    quarterIndex,
                                    style: TextStyle(
                                      color: _selectedQuarter == quarterIndex
                                          ? Colors.white
                                          : kPrimaryColor,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: 4,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            TextField(
              controller: _myTitleController,
              onChanged: (value) {
                title = value;
              },
              decoration: InputDecoration(
                labelText: 'Title',
                labelStyle: TextStyle(color: kPrimaryColor),
              ),
            ),
            TextField(
              onChanged: (value) {
                _weightValue = double.parse(value);
              },
              controller: _myWeightController,
              decoration: InputDecoration(
                labelText: 'Weight',
                labelStyle: TextStyle(color: kPrimaryColor),
              ),
            ),
            Container(
              height: 34,
              width: double.maxFinite,
              padding: EdgeInsets.symmetric(vertical: 3),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        _weightValue = double.parse(listOfSampleWeight[index]);
                        _myWeightController
                          ..text = _weightValue != null
                              ? _weightValue.toString()
                              : "";
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 6.0),
                      margin: EdgeInsets.symmetric(horizontal: 1),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: kPrimaryColor),
                      ),
                      child: Center(
                        child: Text(
                          listOfSampleWeight[index],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listOfSampleWeight.length,
              ),
            ),
            Center(
              child: TextButton(
                child: Text("+ Add"),
                onPressed: () {
                  _myTitleController.clear();
                  _myWeightController.clear();
                  setState(() {
                    title != null && _weightValue != null
                        ? _listOfAddedAssessment
                            .add({"title": title, "weight": _weightValue})
                        : _listOfAddedAssessment = [];
                  });
                },
              ),
            ),
            _listOfAddedAssessment.isNotEmpty
                ? Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return ListTile(
                          dense: true,
                          title: Text(
                            "Title: ${_listOfAddedAssessment[index]['title']}",
                            style: TextStyle(color: kPrimaryColor),
                            textAlign: TextAlign.justify,
                          ),
                          subtitle: Text(
                            "Weight: ${_listOfAddedAssessment[index]['weight']}",
                            style: TextStyle(color: kPrimaryColor),
                          ),
                          trailing: IconButton(
                            onPressed: () {
                              setState(() {
                                _listOfAddedAssessment.removeWhere((element) =>
                                    _listOfAddedAssessment.indexOf(element) ==
                                    index);
                              });
                            },
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          ),
                        );
                      },
                      itemCount: _listOfAddedAssessment.length,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
