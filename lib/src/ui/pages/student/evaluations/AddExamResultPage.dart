import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/exam.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/model/result.dart';
import 'package:login/src/model/student.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:provider/provider.dart';

class AddExamResultPage extends StatefulWidget {
  final listOfAssessmentType;
  final academicYear;
  final selectedSubject;
  final quarter;

  AddExamResultPage(
      {@required this.academicYear,
      @required this.listOfAssessmentType,
      @required this.quarter,
      @required this.selectedSubject});

  @override
  _AddExamResultPageState createState() => _AddExamResultPageState();
}

class _AddExamResultPageState extends State<AddExamResultPage> {
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  String query = "";

  List<Widget> listOfAssesssmentWidget = [];

  List listOfAssessmentResult = [];
  List<Map> listResultMap = [];

  ResponseModel _responseModel = ResponseModel();
  AuthMethods _authMethods = AuthMethods();
  final _formKey = GlobalKey<FormState>();
  // TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Add result",
          style: TextStyle(fontWeight: FontWeight.w300),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                  child: Row(
                    children: [
                      RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "Academic Year: ",
                              style: TextStyle(color: kPrimaryColor),
                            ),
                            TextSpan(
                              text: widget.academicYear.toString(),
                              style: TextStyle(
                                  color: kPrimaryColor,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "Quarter: ",
                              style: TextStyle(color: kPrimaryColor),
                            ),
                            TextSpan(
                              text: widget.quarter,
                              style: TextStyle(
                                  color: kPrimaryColor,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                  child: Row(
                    children: [
                      RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "Class: ",
                              style: TextStyle(color: kPrimaryColor),
                            ),
                            TextSpan(
                              text:
                                  "${Provider.of<EvaluationProvider>(context).getSelectedClass.grade.toString()}${capitalize(Provider.of<EvaluationProvider>(context).getSelectedClass.section.toString())}",
                              style: TextStyle(
                                  color: kPrimaryColor,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "Subject: ",
                              style: TextStyle(color: kPrimaryColor),
                            ),
                            TextSpan(
                              text: capitalize(widget.selectedSubject),
                              style: TextStyle(
                                  color: kPrimaryColor,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                // Container(
                //   height: MediaQuery.of(context).size.height * 0.08,
                //   padding: EdgeInsets.symmetric(vertical: 4.0),
                //   child: ListView.builder(
                //     scrollDirection: Axis.horizontal,
                //     shrinkWrap: true,
                //     itemCount: listOfAssesssmentWidget.length,
                //     itemBuilder: (context, index) {
                //       return Padding(
                //         padding: const EdgeInsets.only(right: 34.0),
                //         child: Column(
                //           crossAxisAlignment: CrossAxisAlignment.start,
                //           children: [
                //             Padding(
                //               padding:
                //                   const EdgeInsets.symmetric(vertical: 3.0),
                //               child: Text(
                //                 widget.listOfAssessmentType[index]['title'],
                //                 style: TextStyle(
                //                     color: kPrimaryColor,
                //                     fontWeight: FontWeight.w600),
                //               ),
                //             ),
                //             RichText(
                //               text: TextSpan(
                //                 children: <TextSpan>[
                //                   TextSpan(
                //                     text: "Weight: ",
                //                     style: TextStyle(color: kPrimaryColor),
                //                   ),
                //                   TextSpan(
                //                     text: widget.listOfAssessmentType[index]
                //                             ['weight']
                //                         .toString(),
                //                     style: TextStyle(
                //                         color: kPrimaryColor,
                //                         fontWeight: FontWeight.w500),
                //                   ),
                //                 ],
                //               ),
                //             ),
                //           ],
                //         ),
                //       );
                //     },
                //   ),
                // ),
              ],
            ),
          ),
          Form(
            key: _formKey,
            child: Expanded(
              child: Consumer<StudentProvider>(
                builder: (context, studentProvider, child) {
                  var _singleClass;
                  var myClassStudent;

                  for (_singleClass in studentProvider.studentMapList) {
                    if (Provider.of<EvaluationProvider>(context, listen: false)
                            .getSelectedClass
                            .crid ==
                        _singleClass["crid"]) {
                      if (query.isNotEmpty) {
                        final List<Student> suggestionList = query.isEmpty
                            ? []
                            : _singleClass["StudentList"] != null
                                ? _singleClass["StudentList"]
                                    .where((Student student) {
                                    String _getSrudentName =
                                        student.firstName.toLowerCase() +
                                            " " +
                                            student.lastName.toLowerCase();
                                    String _query = query.toLowerCase();
                                    String _getUsername =
                                        student.idNo.toLowerCase();
                                    bool matchesName =
                                        _getSrudentName.contains(_query);
                                    bool matchesUsername =
                                        _getUsername.contains(_query);

                                    return (matchesName || matchesUsername);
                                  }).toList()
                                : [];

                        myClassStudent = suggestionList;
                        print("${myClassStudent} miskino");
                      } else {
                        myClassStudent = _singleClass["StudentList"];
                        print("${myClassStudent} miskin");
                      }
                    }
                  }

                  listOfAssessmentResult.clear();

                  return studentProvider.getStuMapCount != 0
                      ? ListView.builder(
                          itemBuilder: (context, upperIndex) {
                            final student = myClassStudent[upperIndex];
                            List listOfResult = [];

                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ListTile(
                                  leading: CircleAvatar(
                                    radius: 17,
                                  ),
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          "${capitalize(student.firstName)} ${capitalize(student.lastName)}"),
                                      Text(
                                        "ID No: ${student.idNo}",
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey),
                                      ),
                                    ],
                                  ),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        // scrollDirection: Axis.horizontal,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemBuilder: (context, lowerIndex) {
                                          Exam exam = Exam();
                                          bool _isExist = false;
                                          bool _newAdded = false;

                                          Result result = Result();

                                          result.title =
                                              widget.listOfAssessmentType[
                                                  lowerIndex]['title'];

                                          result.weight =
                                              widget.listOfAssessmentType[
                                                  lowerIndex]['weight'];

                                          listOfResult.add(result);

                                          // result.weight =
                                          //     widget.listOfAssessmentType[
                                          //         lowerIndex]['weight'];

                                          listOfAssessmentResult
                                              .forEach((element) {
                                            if (element.stuid == student.idNo) {
                                              print("${_isExist} klkl");
                                              _isExist = true;
                                            }
                                          });

                                          if (!_isExist) {
                                            exam.stuid = student.idNo;
                                            exam.listOfResult = listOfResult;

                                            listOfAssessmentResult.add(exam);
                                          }

                                          return Row(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 70.0, right: 15),
                                                child: Text(
                                                  capitalize(widget
                                                          .listOfAssessmentType[
                                                      lowerIndex]['title']),
                                                ),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.25,
                                                padding: EdgeInsets.symmetric(
                                                    vertical: 2),
                                                child: TextFormField(
                                                  validator: (value) {
                                                    if (value != "") {
                                                      if (double.parse(value) >
                                                          widget.listOfAssessmentType[
                                                                  lowerIndex]
                                                              ['weight']) {
                                                        return 'above weight';
                                                      }
                                                      return null;
                                                    }
                                                    return null;
                                                  },
                                                  onTap: () {},
                                                  onChanged: (value) {
                                                    // listOfAssessmentResult[0]
                                                    //     .listOfResult[1]
                                                    //     .score = 12.0;
                                                    print(
                                                        "${listOfAssessmentResult} pipi");

                                                    listOfAssessmentResult
                                                        .forEach(
                                                      (examElement) {
                                                        if (examElement.stuid ==
                                                            student.idNo) {
                                                          examElement
                                                              .listOfResult
                                                              .forEach(
                                                            (singleResult) {
                                                              if (singleResult
                                                                      .title ==
                                                                  widget.listOfAssessmentType[
                                                                          lowerIndex]
                                                                      [
                                                                      'title']) {
                                                                if (value ==
                                                                    "") {
                                                                  singleResult
                                                                          .taken =
                                                                      false;
                                                                  singleResult
                                                                          .score =
                                                                      0.0;
                                                                } else {
                                                                  singleResult
                                                                          .taken =
                                                                      true;
                                                                  singleResult
                                                                          .score =
                                                                      double.parse(
                                                                          value);
                                                                }
                                                              }
                                                            },
                                                          );
                                                        }
                                                      },
                                                    );
                                                  },
                                                  keyboardType:
                                                      TextInputType.number,
                                                  decoration: InputDecoration(
                                                      hintText: "score",
                                                      hintStyle: TextStyle(
                                                          color: Colors
                                                              .grey.shade400),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius.zero,
                                                        borderSide: BorderSide(
                                                          width: 1.5,
                                                          color: kPrimaryColor
                                                              .withOpacity(0.7),
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius.zero,
                                                        borderSide: BorderSide(
                                                          width: 0.8,
                                                          color: Colors
                                                              .grey.shade300,
                                                        ),
                                                      ),
                                                      contentPadding:
                                                          EdgeInsets.symmetric(
                                                              vertical: 6,
                                                              horizontal: 2),
                                                      isCollapsed: true,
                                                      fillColor: kPrimaryColor
                                                          .withOpacity(0.1),
                                                      filled: true),
                                                ),
                                              ),
                                              Text(
                                                "  /${widget.listOfAssessmentType[lowerIndex]['weight']}",
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    color:
                                                        Colors.grey.shade500),
                                              ),
                                            ],
                                          );
                                        },
                                        itemCount:
                                            widget.listOfAssessmentType.length,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            );
                          },
                          itemCount: myClassStudent.length,
                        )
                      : LoadingPage();
                },
              ),
            ),
          ),
          Center(
            child: TextButton(
              child: Text("Submit"),
              onPressed: () async {
                // bool _isEmpty = false;
                //first clean up empty results

                print("${listOfAssessmentResult} habte");

                if (_formKey.currentState.validate()) {
                  await persormResultSubmit();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  persormResultSubmit() async {
    listResultMap.clear();
    //TODO: when submiting again all the taken property becomes false and all the scores becomes 0.
    listOfAssessmentResult.forEach((element) {
      var singleStudentResultList = [];
      print("${element.listOfResult[0].score} guzo");
      element.listOfResult.forEach((resultElemnt) {
        singleStudentResultList.add(resultElemnt.toMap());
      });

      listResultMap.add({
        "stuid": element.stuid.toString(),
        "gradeReport": {
          "academicYear": widget.academicYear.toString(),
          "quarter": widget.quarter.toString(),
          "subject": widget.selectedSubject.toString(),
          "results": singleStudentResultList
        }
      });
    });

    _responseModel = await _authMethods.addResult(listResultMap);
    print("${_responseModel.msg} kobo");
  }
}
