import 'package:flutter/material.dart';
import 'package:login/src/ui/pages/student/com_book/com_book_card.dart';

class ComBookList extends StatelessWidget {
  final oneDayComBookList;
  final date;
  ComBookList({this.oneDayComBookList, this.date});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        child: ListView.builder(
          itemBuilder: (context, index) {
            final singleComBook = oneDayComBookList[index];
            return ComBookCard(
              oneComBookList: singleComBook,
              date: date,
            );
          },
          itemCount: oneDayComBookList.length,
        ),
      ),
    );
  }
}
