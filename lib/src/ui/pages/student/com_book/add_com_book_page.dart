import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/com_book.dart';
import 'package:login/src/model/com_book_activity.dart';
import 'package:login/src/model/com_book_assignments.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/provider/com_book_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class AddComBookPage extends StatefulWidget {
  // final classList;
  // final selectedClass;

  // AddComBookPage({this.classList, this.selectedClass});
  @override
  _AddComBookPageState createState() => _AddComBookPageState();
}

class _AddComBookPageState extends State<AddComBookPage> {
  var _selectedClass;
  ComBook comBook = ComBook();
  ComBookActivity comBookActivity = ComBookActivity();
  ComBookAssignments comBookAssignments = ComBookAssignments();

  DateTime currentDate = DateTime.now();
  String teacherComment;
  bool isLoading = false;
  AuthMethods _authMethods = AuthMethods();
  ResponseModel _responseModel = ResponseModel();
  bool isHWchecked = false;
  bool isAssChecked = false;
  bool isGWchecked = false;
  String hwDueGate;
  String assDueDate;
  String gwDueDate;
  String _selectedSubject;

  var tClassList;

  var listOfActivity = [];

  @override
  void initState() {
    super.initState();
    _selectedClass =
        Provider.of<ComBookProvider>(context, listen: false).getSelectedClass;

    comBook.date = DateTime.now().toString().split(' ')[0];
  }

  @override
  Widget build(BuildContext context) {
    // print("${selectedClass} vora");

    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              // margin: EdgeInsets.symmetric(vertical: 12.0),
              color: Colors.grey.shade300,
              height: 90.0,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Center(
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      var subject;

                      subjectList.forEach((element) {
                        if (element['subject_name'].toLowerCase() ==
                            _selectedClass.subjects[index].toLowerCase()) {
                          subject = element;
                        }
                      });

                      return Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedSubject = subject['subject_name'];
                              });
                            },
                            child: Container(
                              height: 120.0,
                              width: 120.0,
                              decoration: BoxDecoration(
                                color:
                                    _selectedSubject == subject['subject_name']
                                        ? kPrimaryColor
                                        : Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: ImageIcon(
                                      AssetImage(subject['subject_icon']),
                                      color: Colors.orange,
                                      size: 40,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Text(
                                      subject['subject_name'],
                                      style: TextStyle(
                                        color: _selectedSubject ==
                                                subject['subject_name']
                                            ? Colors.white
                                            : kPrimaryColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    itemCount: _selectedClass.subjects.length,
                  ),
                ),
              ),
            ),
            // Container(
            //   color: kPrimaryColor,
            //   width: double.infinity,
            //   child: Padding(
            //     padding: const EdgeInsets.symmetric(vertical: 16.0),
            //     child: Column(
            //       children: [
            //         Padding(
            //           padding: const EdgeInsets.symmetric(vertical: 2.0),
            //           child: DropdownButton<String>(
            //             dropdownColor: kPrimaryColor.withOpacity(0.7),
            //             focusColor: Colors.white,
            //             value: _chosenClass,
            //             //elevation: 5,
            //             style: TextStyle(
            //               color: Colors.white,
            //             ),
            //             iconEnabledColor: Colors.white,
            //             items: widget.classList
            //                 .map<DropdownMenuItem<String>>((value) {
            //               return DropdownMenuItem<String>(
            //                 value: value['nameOfClass'],
            //                 child: Text(
            //                   value['nameOfClass'],
            //                   style: TextStyle(color: Colors.white),
            //                 ),
            //               );
            //             }).toList(),
            //             hint: Text(
            //               "select class",
            //               style: TextStyle(
            //                   color: Colors.white,
            //                   fontSize: 14,
            //                   fontWeight: FontWeight.w400),
            //             ),
            //             onChanged: (String value) {
            //               setState(
            //                 () {
            //                   _chosenClass = value;
            //                   for (var singleClass in widget.classList) {
            //                     if (singleClass["nameOfClass"] ==
            //                         _chosenClass) {
            //                       selectedClass = singleClass;
            //                       _chosenSubject = null;
            //                     }
            //                   }
            //                   // _isLoading = true;
            //                 },
            //               );
            //             },
            //           ),
            //         ),
            //         _chosenClass != null
            //             ? Padding(
            //                 padding: const EdgeInsets.symmetric(vertical: 2.0),
            //                 child: DropdownButton<String>(
            //                   dropdownColor: kPrimaryColor.withOpacity(0.7),
            //                   focusColor: Colors.white,
            //                   value: _chosenSubject,
            //                   //elevation: 5,
            //                   style: TextStyle(
            //                     color: Colors.white,
            //                   ),
            //                   iconEnabledColor: Colors.white,
            //                   items: selectedClass['subjects']
            //                       .map<DropdownMenuItem<String>>((value) {
            //                     return DropdownMenuItem<String>(
            //                       value: value,
            //                       child: Text(
            //                         value,
            //                         style: TextStyle(color: Colors.white),
            //                       ),
            //                     );
            //                   }).toList(),
            //                   hint: Text(
            //                     "Please select subject",
            //                     style: TextStyle(
            //                         color: Colors.white,
            //                         fontSize: 14,
            //                         fontWeight: FontWeight.w400),
            //                   ),
            //                   onChanged: (String value) {
            //                     if (value != _chosenSubject) {
            //                       setState(
            //                         () {
            //                           _chosenSubject = value;
            //                         },
            //                       );
            //                     }
            //                   },
            //                 ),
            //               )
            //             : Container(),
            //       ],
            //     ),
            //   ),
            // ),
            _selectedSubject != null && _selectedSubject != null
                ? Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 12),
                            child: Column(
                              children: [
                                Container(
                                  color: Colors.grey.shade300,
                                  // margin: EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: ListTile(
                                              leading: Icon(
                                                Icons.home_work_outlined,
                                                color: kPrimaryColor,
                                              ),
                                              title: Text("Home Work"),
                                              trailing: Transform.scale(
                                                scale: 1.3,
                                                child: Checkbox(
                                                  value: isHWchecked,
                                                  onChanged: (value) {
                                                    var singleActivity;
                                                    bool _isExist = false;
                                                    ComBookActivity
                                                        _comBookActivity =
                                                        ComBookActivity();
                                                    _comBookActivity.type =
                                                        "Home Work";

                                                    if (listOfActivity
                                                        .isNotEmpty) {
                                                      for (singleActivity
                                                          in listOfActivity) {
                                                        if (singleActivity
                                                                .type ==
                                                            "Home Work") {
                                                          _isExist = true;
                                                        }
                                                      }
                                                    }

                                                    if (!_isExist) {
                                                      listOfActivity.add(
                                                          _comBookActivity);
                                                    }

                                                    setState(
                                                      () {
                                                        isHWchecked =
                                                            !isHWchecked;
                                                      },
                                                    );
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      isHWchecked
                                          ? Container(
                                              color: kPrimaryColor.withOpacity(
                                                0.8,
                                              ),
                                              padding: EdgeInsets.all(8.0),
                                              width: double.infinity,
                                              child: Center(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      "Due Date:   ",
                                                      style: TextStyle(
                                                        color: Colors.white70,
                                                        fontSize: 16.0,
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () async {
                                                        final DateTime picked =
                                                            await showDatePicker(
                                                          context: context,
                                                          initialDate: DateTime
                                                              .now(), // Refer step 1
                                                          firstDate:
                                                              DateTime.now(),
                                                          lastDate:
                                                              DateTime.utc(
                                                                  2025),
                                                          builder:
                                                              (context, child) {
                                                            return Column(
                                                              children: <
                                                                  Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      top:
                                                                          50.0),
                                                                  child:
                                                                      Container(
                                                                    height: 450,
                                                                    width: 600,
                                                                    child:
                                                                        child,
                                                                  ),
                                                                ),
                                                              ],
                                                            );
                                                          },
                                                        );
                                                        if (picked != null) {
                                                          var singleActivity;

                                                          setState(
                                                            () {
                                                              hwDueGate = picked
                                                                  .toString();
                                                              if (listOfActivity
                                                                  .isNotEmpty) {
                                                                for (singleActivity
                                                                    in listOfActivity) {
                                                                  if (singleActivity
                                                                          .type ==
                                                                      "Home Work") {
                                                                    singleActivity
                                                                            .dueDate =
                                                                        hwDueGate
                                                                            .split(" ")[0];
                                                                  }
                                                                }
                                                              }
                                                            },
                                                          );
                                                        }
                                                      },
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                            color:
                                                                kPrimaryColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                        padding:
                                                            EdgeInsets.all(5.0),
                                                        child: Text(
                                                          hwDueGate == null
                                                              ? "  select  "
                                                              : "${DateFormat.yMMMMd().format(DateTime.parse(hwDueGate))}"
                                                                  .split(
                                                                      'T')[0],
                                                          style: TextStyle(
                                                            color:
                                                                Colors.white70,
                                                            fontSize: 16.0,
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                                Container(
                                  color: Colors.grey.shade300,
                                  margin: EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: ListTile(
                                              leading: Icon(
                                                Icons.assignment_outlined,
                                                color: kPrimaryColor,
                                              ),
                                              title: Text("Assignment"),
                                              trailing: Transform.scale(
                                                scale: 1.3,
                                                child: Checkbox(
                                                  value: isAssChecked,
                                                  onChanged: (value) {
                                                    var singleActivity;
                                                    bool _isExist = false;
                                                    ComBookActivity
                                                        _comBookActivity =
                                                        ComBookActivity();
                                                    _comBookActivity.type =
                                                        "Assignment";

                                                    if (listOfActivity
                                                        .isNotEmpty) {
                                                      for (singleActivity
                                                          in listOfActivity) {
                                                        if (singleActivity
                                                                .type ==
                                                            "Assignment") {
                                                          _isExist = true;
                                                        }
                                                      }
                                                    }

                                                    if (!_isExist) {
                                                      listOfActivity.add(
                                                          _comBookActivity);
                                                    }

                                                    setState(
                                                      () {
                                                        isAssChecked =
                                                            !isAssChecked;
                                                      },
                                                    );
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      isAssChecked
                                          ? Container(
                                              color: kPrimaryColor.withOpacity(
                                                0.8,
                                              ),
                                              padding: EdgeInsets.all(8.0),
                                              width: double.infinity,
                                              child: Center(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      "Due Date:   ",
                                                      style: TextStyle(
                                                        color: Colors.white70,
                                                        fontSize: 16.0,
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () async {
                                                        final DateTime picked =
                                                            await showDatePicker(
                                                          context: context,
                                                          initialDate: DateTime
                                                              .now(), // Refer step 1
                                                          firstDate:
                                                              DateTime.now(),
                                                          lastDate:
                                                              DateTime.utc(
                                                                  2025),
                                                          builder:
                                                              (context, child) {
                                                            return Column(
                                                              children: <
                                                                  Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      top:
                                                                          50.0),
                                                                  child:
                                                                      Container(
                                                                    height: 450,
                                                                    width: 600,
                                                                    child:
                                                                        child,
                                                                  ),
                                                                ),
                                                              ],
                                                            );
                                                          },
                                                        );
                                                        if (picked != null) {
                                                          var singleActivity;

                                                          setState(() {
                                                            assDueDate = picked
                                                                .toString();

                                                            if (listOfActivity
                                                                .isNotEmpty) {
                                                              for (singleActivity
                                                                  in listOfActivity) {
                                                                if (singleActivity
                                                                        .type ==
                                                                    "Assignment") {
                                                                  singleActivity
                                                                          .dueDate =
                                                                      assDueDate
                                                                          .split(
                                                                              " ")[0];
                                                                }
                                                              }
                                                            }
                                                          });
                                                        }
                                                      },
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                            color:
                                                                kPrimaryColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                        padding:
                                                            EdgeInsets.all(5.0),
                                                        child: Text(
                                                          assDueDate == null
                                                              ? "  select  "
                                                              : "${DateFormat.yMMMMd().format(DateTime.parse(assDueDate))}"
                                                                  .split(
                                                                      'T')[0],
                                                          style: TextStyle(
                                                            color:
                                                                Colors.white70,
                                                            fontSize: 16.0,
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                                Container(
                                  color: Colors.grey.shade300,
                                  margin: EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: ListTile(
                                              leading: Icon(
                                                Icons.group_work_outlined,
                                                color: kPrimaryColor,
                                              ),
                                              title: Text("Group Work"),
                                              trailing: Transform.scale(
                                                scale: 1.3,
                                                child: Checkbox(
                                                  value: isGWchecked,
                                                  onChanged: (value) {
                                                    var singleActivity;
                                                    bool _isExist = false;
                                                    ComBookActivity
                                                        _comBookActivity =
                                                        ComBookActivity();
                                                    _comBookActivity.type =
                                                        "Group Work";

                                                    if (listOfActivity
                                                        .isNotEmpty) {
                                                      for (singleActivity
                                                          in listOfActivity) {
                                                        if (singleActivity
                                                                .type ==
                                                            "Group Work") {
                                                          _isExist = true;
                                                        }
                                                      }
                                                    }

                                                    if (!_isExist) {
                                                      listOfActivity.add(
                                                          _comBookActivity);
                                                    }

                                                    setState(
                                                      () {
                                                        isGWchecked =
                                                            !isGWchecked;
                                                      },
                                                    );
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      isGWchecked
                                          ? Container(
                                              color: kPrimaryColor.withOpacity(
                                                0.8,
                                              ),
                                              padding: EdgeInsets.all(8.0),
                                              width: double.infinity,
                                              child: Center(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      "Due Date:   ",
                                                      style: TextStyle(
                                                        color: Colors.white70,
                                                        fontSize: 16.0,
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () async {
                                                        final DateTime picked =
                                                            await showDatePicker(
                                                          context: context,
                                                          initialDate: DateTime
                                                              .now(), // Refer step 1
                                                          firstDate:
                                                              DateTime.now(),
                                                          lastDate:
                                                              DateTime.utc(
                                                                  2025),
                                                          builder:
                                                              (context, child) {
                                                            return Column(
                                                              children: <
                                                                  Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      top:
                                                                          50.0),
                                                                  child:
                                                                      Container(
                                                                    height: 450,
                                                                    width: 600,
                                                                    child:
                                                                        child,
                                                                  ),
                                                                ),
                                                              ],
                                                            );
                                                          },
                                                        );
                                                        if (picked != null) {
                                                          var singleActivity;

                                                          setState(
                                                            () {
                                                              gwDueDate = picked
                                                                  .toString();

                                                              if (listOfActivity
                                                                  .isNotEmpty) {
                                                                for (singleActivity
                                                                    in listOfActivity) {
                                                                  if (singleActivity
                                                                          .type ==
                                                                      "Group Work") {
                                                                    singleActivity
                                                                            .dueDate =
                                                                        gwDueDate
                                                                            .split(" ")[0];
                                                                  }
                                                                }
                                                              }
                                                            },
                                                          );
                                                        }
                                                      },
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                            color:
                                                                kPrimaryColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                        padding:
                                                            EdgeInsets.all(5.0),
                                                        child: Text(
                                                          gwDueDate == null
                                                              ? "  select  "
                                                              : "${DateFormat.yMMMMd().format(DateTime.parse(gwDueDate))}"
                                                                  .split(
                                                                      'T')[0],
                                                          style: TextStyle(
                                                            color:
                                                                Colors.white70,
                                                            fontSize: 16.0,
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // Expanded(child: SizedBox()),
                          SingleChildScrollView(
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0, horizontal: 8.0),
                                    child: Align(
                                      alignment: Alignment.bottomRight,
                                      child: Text(
                                        "${currentDate.toLocal()}"
                                            .split(' ')[0],
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                  TextField(
                                    maxLines: 4,
                                    decoration: InputDecoration(
                                      hintText: "Write a comment....",
                                      hintStyle: TextStyle(fontSize: 12),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black12, width: 0.6),
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black12, width: 0.6),
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                    ),
                                    onChanged: (commentValue) {
                                      teacherComment = commentValue;
                                      comBookAssignments.comment =
                                          teacherComment;
                                    },
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        width: 110,
                                        height: 35,
                                        decoration: BoxDecoration(
                                          color: kPrimaryColor,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        child: TextButton(
                                          onPressed: () async {
                                            comBook.crid = _selectedClass.crid;
                                            comBookAssignments.subject =
                                                _selectedSubject;
                                            comBookAssignments.activityList =
                                                listOfActivity;

                                            comBook.assignmentsList = [
                                              comBookAssignments
                                            ];
                                            print(
                                                "${comBook.assignmentsList} alem");
                                            await performAddCoMBook(comBook);
                                          },
                                          child: isLoading
                                              ? Center(
                                                  child: Container(
                                                  height: 23,
                                                  width: 23,
                                                  child:
                                                      CircularProgressIndicator(
                                                    strokeWidth: 3,
                                                    backgroundColor:
                                                        Colors.white70,
                                                  ),
                                                ))
                                              : Text(
                                                  "Submit",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : Container(
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.4),
                    child: Center(
                      child: Text(
                        "Please select a subject!",
                        style: TextStyle(
                          color: Colors.orange,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  )
          ],
        ),
      ),
    );
  }

  performAddCoMBook(ComBook comBook) async {
    _responseModel = await _authMethods.addComBook(comBook);

    if (_responseModel.success == true) {
      await Provider.of<ComBookProvider>(context, listen: false)
          .refreshComBook();

      Navigator.pop(context);
    }

    print(_responseModel.success);
  }
}
