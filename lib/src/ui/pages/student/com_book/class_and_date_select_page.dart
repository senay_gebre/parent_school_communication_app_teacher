import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/provider/com_book_provider.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/ui/pages/student/com_book/com_book_page.dart';
import 'package:login/src/ui/pages/student/evaluations/grading_page.dart';
import 'package:login/src/ui/pages/student/rating/progress_page.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class ClassAndDateSelectPage extends StatefulWidget {
  final route;
  ClassAndDateSelectPage({this.route});
  @override
  _ClassAndDateSelectPageState createState() => _ClassAndDateSelectPageState();
}

class _ClassAndDateSelectPageState extends State<ClassAndDateSelectPage> {
  DateTime selectedDate = DateTime.now();

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  var _selectedClass;
  String _selectedSubject;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    print(widget.route);
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: ListTile(
                    dense: true,
                    title: Text(
                      "Select Class",
                      style: TextStyle(color: kPrimaryColor, fontSize: 28.0),
                    ),
                    subtitle: Text(
                      "select a class you want to evaluate or view evaluations.",
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 23,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/classroom_icon.png"),
                    color: Colors.orange.withOpacity(0.6),
                    size: 80.0,
                  ),
                )
              ],
            ),
            Container(
              height: size.height * 0.2,
              child: Consumer<TeacherClassProvider>(
                builder: (context, teacherClassProvider, child) {
                  return ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) => Center(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _selectedClass =
                                  teacherClassProvider.teacherClassList[index];
                              _selectedSubject = null;
                            });
                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                                _selectedClass ==
                                        teacherClassProvider
                                            .teacherClassList[index]
                                    ? kPrimaryColor
                                    : Colors.white),
                            overlayColor: MaterialStateProperty.all(
                              kPrimaryColor.withOpacity(0.2),
                            ),
                            padding: MaterialStateProperty.all(
                              EdgeInsets.symmetric(
                                horizontal: 60.0,
                                vertical: 15.0,
                              ),
                            ),
                          ),
                          child: Text(
                            "Class ${teacherClassProvider.teacherClassList[index].grade.toString()}${capitalize(teacherClassProvider.teacherClassList[index].section.toString())}",
                            style: TextStyle(
                                color: _selectedClass ==
                                        teacherClassProvider
                                            .teacherClassList[index]
                                    ? Colors.white
                                    : kPrimaryColor),
                          ),
                        ),
                      ),
                    ),
                    itemCount: teacherClassProvider.teacherClassList.length,
                  );
                },
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.5,
              padding: EdgeInsets.only(top: 10),
              color: Colors.white,
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    margin: EdgeInsets.only(top: 30),
                    decoration: BoxDecoration(
                      color: kPrimaryColor.withOpacity(0.2),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        dense: true,
                        title: Text(
                          "Date",
                          style:
                              TextStyle(color: kPrimaryColor, fontSize: 28.0),
                        ),
                        subtitle: Text(
                          "select year and month to view and add communication book.",
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: -8.0,
                    right: 15,
                    child: Icon(
                      Icons.calendar_today,
                      size: 80.0,
                      color: Colors.orange.withOpacity(0.6),
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(
                        height: 100.0,
                      ),
                      _selectedClass != null
                          ? Expanded(
                              child: Container(
                                margin: EdgeInsets.symmetric(vertical: 12.0),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20.0),
                                  child: Center(
                                      child: GestureDetector(
                                    onTap: () async {
                                      await showMonthPicker(
                                        context: context,

                                        initialDate: DateTime.now(),
                                        firstDate: DateTime(
                                            DateTime.now().year - 1, 5),
                                        lastDate: DateTime(
                                            DateTime.now().year + 1, 9),
                                        // builder: (context, child) {
                                        //   return Column(
                                        //     children: <Widget>[
                                        //       Padding(
                                        //         padding: const EdgeInsets.only(
                                        //             top: 50.0),
                                        //         child: Container(
                                        //           height: 450,
                                        //           width: 600,
                                        //           child: child,
                                        //         ),
                                        //       ),
                                        //     ],
                                        //   );
                                        // },
                                      ).then((date) {
                                        if (date != null) {
                                          setState(() {
                                            selectedDate = date;
                                          });
                                        }
                                      });
                                    },
                                    child: Column(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(6.0),
                                          decoration: BoxDecoration(
                                            color:
                                                kPrimaryColor.withOpacity(0.9),
                                            borderRadius:
                                                BorderRadius.circular(2.0),
                                          ),
                                          child: Text(
                                            DateFormat.yMMMM()
                                                .format(selectedDate),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20.0),
                                          ),
                                        ),
                                        Text(
                                          "Tap to change.",
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )),
                                ),
                              ),
                            )
                          : Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 34),
                                child: Text(
                                  "Date will be displayed here.",
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ),
                      _selectedClass != null
                          ? Padding(
                              padding: EdgeInsets.symmetric(vertical: 8.0),
                              child: ElevatedButton(
                                onPressed: () {
                                  Provider.of<ComBookProvider>(context,
                                          listen: false)
                                      .setSelectedClass(
                                          selectedClass: _selectedClass);
                                  Provider.of<ComBookProvider>(context,
                                          listen: false)
                                      .setComBookData(
                                          month: selectedDate.month,
                                          year: selectedDate.year);

                                  Navigator.pushNamed(context, ComBookPage.id);
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.white),
                                  overlayColor: MaterialStateProperty.all(
                                    kPrimaryColor.withOpacity(0.2),
                                  ),
                                  padding: MaterialStateProperty.all(
                                    EdgeInsets.symmetric(
                                      horizontal: 60.0,
                                      vertical: 15.0,
                                    ),
                                  ),
                                ),
                                child: Text(
                                  "Continue",
                                  style: TextStyle(
                                    color: kPrimaryColor,
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
