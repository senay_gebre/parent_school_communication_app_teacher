// import 'package:flutter/material.dart';
// import 'package:login/src/constants/constants.dart';

// class SingleComBookTile extends StatelessWidget {
//   String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

//   final dueDate;
//   final type;

//   SingleComBookTile({this.dueDate, this.type});

//   @override
//   Widget build(BuildContext context) {
//     return ListTile(
//       title: Row(
//         children: [
//           Text(
//             capitalize(type),
//             style: TextStyle(color: kPrimaryColor, fontWeight: FontWeight.bold),
//           ),
//         ],
//       ),
//       subtitle: Text(
//         "Due date:   ${dueDate}".split('T')[0],
//         style: TextStyle(color: kPrimaryColor),
//       ),
//     );
//   }
// }
