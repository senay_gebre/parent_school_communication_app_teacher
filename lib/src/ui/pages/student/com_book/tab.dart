// import 'package:flutter/material.dart';
// import 'package:login/src/constants/constants.dart';
// import 'package:login/src/model/Activity.dart';
// import 'package:login/src/model/response_model.dart';
// import 'package:login/src/model/student.dart';
// import 'package:login/src/provider/activity_provider.dart';
// import 'package:login/src/provider/com_book_provider.dart';
// import 'package:login/src/provider/student_provider.dart';
// import 'package:login/src/provider/user_provider.dart';
// import 'package:login/src/resources/auth_methods.dart';
// import 'package:login/src/ui/pages/student/com_book/com_book_list.dart';
// import 'package:login/src/ui/pages/student/com_book/com_book_page.dart';
// import 'package:login/src/ui/pages/student/com_book/add_com_book_tile.dart';
// import 'package:provider/provider.dart';

// class AddComBookPage extends StatefulWidget {
//   @override
//   _AddComBookPageState createState() => _AddComBookPageState();
// }

// class _AddComBookPageState extends State<AddComBookPage> {
//   bool isLoading = false;
//   ResponseModel _responseModel = ResponseModel();
//   AuthMethods _authMethods = AuthMethods();
//   List<Map<String, dynamic>> listOfActivityGiven = [];

//   List<Student> studentList;

//   TextEditingController searchController = TextEditingController();
//   int _radioValue = 0;
//   DateTime selectedDate = DateTime.now();
//   DateTime currentDate = DateTime.now();

//   String teacherComment;

//   bool isPrivate = false;

//   Color _radioButtonOne = Colors.grey;

//   Color _radioButtonTwo = Colors.grey;

//   void _handleRadioValueChange(int value) {
//     setState(() {
//       _radioValue = value;

//       switch (_radioValue) {
//         case 0:
//           _radioButtonOne = Colors.black;
//           _radioButtonTwo = Colors.grey;
//           isPrivate = false;
//           //   user.gender = "Male";
//           break;
//         case 1:
//           _radioButtonTwo = Colors.black;
//           _radioButtonOne = Colors.grey;
//           isPrivate = true;
//           // user.gender = "Female";
//           break;
//       }
//     });
//   }

//   // DropdownButton<String> androidDropdown() {
//   //   List<DropdownMenuItem<String>> dropdownItems = [];
//   //   for (String activity in activities) {
//   //     var newItem = DropdownMenuItem(
//   //       child: Text(activity),
//   //       value: activity,
//   //     );
//   //     dropdownItems.add(newItem);
//   //   }

//   //   return DropdownButton<String>(
//   //     underline: null,
//   //     isDense: true,
//   //     value: valueChoose,
//   //     items: dropdownItems,
//   //     onChanged: (value) {
//   //       setState(() {
//   //         valueChoose = value;
//   //       });
//   //     },
//   //   );
//   // }

//   // _selectDate(BuildContext context) async {
//   //   final DateTime picked = await showDatePicker(
//   //     context: context,
//   //     initialDate: DateTime.now(), // Refer step 1
//   //     firstDate: DateTime.now(),
//   //     lastDate: DateTime.utc(2025),
//   //   );
//   //   if (picked != null && picked != selectedDate) {
//   //     setState(() {
//   //       selectedDate = picked;
//   //     });
//   //   }
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: IconButton(
//             icon: Icon(Icons.arrow_back),
//             onPressed: () {
//               Provider.of<ActivityProvider>(context, listen: false)
//                   .crearActivities();

//               Navigator.pop(context);
//             }),
//       ),
//       body: Column(
//         children: [
//           Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               SizedBox(width: 80,), Text("Home Work", style: TextStyle(color: Colors.blueAccent,),), Text("Assignment", style: TextStyle(color: Colors.blueAccent,),), Text("Group Work", style: TextStyle(color: Colors.blueAccent,),), SizedBox(width: 40,)
//             ],
//           ),
//           Expanded(
//             child: Consumer<ActivityProvider>(
//               builder: (context, activityProvider, child) {
//                 return SafeArea(
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisSize: MainAxisSize.max,
//                     mainAxisAlignment: MainAxisAlignment.end,
//                     children: [
//                       // Expanded(
//                       //     child: Container(
//                       //   child: Row(
//                       //     children: [
//                       //       Text("Home Work"),
//                       //       Text("Assignment"),
//                       //       Text("Group Work"),
//                       //     ],
//                       //   ),
//                       // )),
//                       Expanded(
//                         child: Padding(
//                           padding: const EdgeInsets.only(top: 8.0),
//                           child: ListView.builder(
//                             itemCount: activityProvider.activitiesLength,
//                             itemBuilder: (context, index) {
//                               return ComBookTile(
//                                 subject: activityProvider.activities[index].subject,
//                                 isHWChecked:
//                                     activityProvider.activities[index].isHWGiven,
//                                 isGWchecked:
//                                     activityProvider.activities[index].isGWGiven,
//                                 isAssiChecked:
//                                     activityProvider.activities[index].isAssiGiven,
//                                 hwDueDate:
//                                     activityProvider.activities[index].hwDueDate,
//                                 grDueDate:
//                                     activityProvider.activities[index].gwDueDate,
//                                 assiDueDate:
//                                     activityProvider.activities[index].assiDueDate,
//                                 homeworkCheckboxcallback: (value) {
//                                   setState(
//                                     () {
//                                       activityProvider.activities[index].isHWGiven =
//                                           !activityProvider
//                                               .activities[index].isHWGiven;
//                                       if (activityProvider
//                                               .activities[index].isHWGiven ==
//                                           true) {
//                                         activityProvider.addActivityType(
//                                             index, "HomeWork");
//                                       } else if (activityProvider
//                                                   .activities[index].isHWGiven ==
//                                               false &&
//                                           activityProvider
//                                               .activities[index].activityType
//                                               .contains("HomeWork")) {
//                                         activityProvider.deleteActivityType(
//                                             index, "HomeWork");
//                                       }
//                                       // else {
//                                       //   activityProvider.addActivityType(
//                                       //       index, "HomeWork");
//                                       // }
//                                     },
//                                   );
//                                 },
//                                 assignmentCheckboxCallBack: (value) {
//                                   setState(
//                                     () {
//                                       activityProvider.activities[index].isAssiGiven =
//                                           !activityProvider
//                                               .activities[index].isAssiGiven;
//                                       if (activityProvider
//                                               .activities[index].isAssiGiven ==
//                                           true) {
//                                         activityProvider.addActivityType(
//                                             index, "Assignment");
//                                       } else if (activityProvider
//                                                   .activities[index].isAssiGiven ==
//                                               false &&
//                                           activityProvider
//                                               .activities[index].activityType
//                                               .contains("Assignment")) {
//                                         activityProvider.deleteActivityType(
//                                             index, "Assignment");
//                                       }
//                                       // else {
//                                       //   activityProvider.addActivityType(
//                                       //       index, "HomeWork");
//                                       // }
//                                     },
//                                   );
//                                 },
//                                 groupworkCheckboxCallBack: (value) {
//                                   setState(
//                                     () {
//                                       activityProvider.activities[index].isGWGiven =
//                                           !activityProvider
//                                               .activities[index].isGWGiven;
//                                       if (activityProvider
//                                               .activities[index].isGWGiven ==
//                                           true) {
//                                         activityProvider.addActivityType(
//                                             index, "Group Work");
//                                       } else if (activityProvider
//                                                   .activities[index].isGWGiven ==
//                                               false &&
//                                           activityProvider
//                                               .activities[index].activityType
//                                               .contains("Group Work")) {
//                                         activityProvider.deleteActivityType(
//                                             index, "Group Work");
//                                       }
//                                     },
//                                   );
//                                 },
//                                 hwDateCallback: () async {
//                                   final DateTime picked = await showDatePicker(
//                                     context: context,
//                                     initialDate: DateTime.now(), // Refer step 1
//                                     firstDate: DateTime.now(),
//                                     lastDate: DateTime.utc(2025),
//                                     builder: (context, child) {
//                                       return Column(
//                                         children: <Widget>[
//                                           Padding(
//                                             padding: const EdgeInsets.only(top: 50.0),
//                                             child: Container(
//                                               height: 450,
//                                               width: 600,
//                                               child: child,
//                                             ),
//                                           ),
//                                         ],
//                                       );
//                                     },
//                                   );
//                                   if (picked != null && picked != selectedDate) {
//                                     setState(() {
//                                       selectedDate = picked;
//                                     });
//                                     activityProvider.activities[index].hwDueDate =
//                                         "${selectedDate.toLocal()}".split(' ')[0];
//                                   }
//                                 },
//                                 assiDateCallback: () async {
//                                   final DateTime picked = await showDatePicker(
//                                     context: context,
//                                     initialDate: DateTime.now(), // Refer step 1
//                                     firstDate: DateTime.now(),
//                                     lastDate: DateTime.utc(2025),
//                                     builder: (context, child) {
//                                       return Column(
//                                         children: <Widget>[
//                                           Padding(
//                                             padding: const EdgeInsets.only(top: 50.0),
//                                             child: Container(
//                                               height: 450,
//                                               width: 600,
//                                               child: child,
//                                             ),
//                                           ),
//                                         ],
//                                       );
//                                     },
//                                   );
//                                   if (picked != null && picked != selectedDate) {
//                                     setState(() {
//                                       selectedDate = picked;
//                                     });
//                                     activityProvider.activities[index].assiDueDate =
//                                         "${selectedDate.toLocal()}".split(' ')[0];
//                                   }
//                                 },
//                                 gwDateCallback: () async {
//                                   final DateTime picked = await showDatePicker(
//                                     context: context,
//                                     initialDate: DateTime.now(), // Refer step 1
//                                     firstDate: DateTime.now(),
//                                     lastDate: DateTime.utc(2025),
//                                     builder: (context, child) {
//                                       return Column(
//                                         children: <Widget>[
//                                           Padding(
//                                             padding: const EdgeInsets.only(top: 50.0),
//                                             child: Container(
//                                               height: 450,
//                                               width: 600,
//                                               child: child,
//                                             ),
//                                           ),
//                                         ],
//                                       );
//                                     },
//                                   );
//                                   if (picked != null && picked != selectedDate) {
//                                     setState(() {
//                                       selectedDate = picked;
//                                     });
//                                     activityProvider.activities[index].gwDueDate =
//                                         "${selectedDate.toLocal()}".split(' ')[0];
//                                   }
//                                 },
//                               );
//                             },
//                           ),
//                         ),
//                       ),

//                       // Expanded(
//                       //   child: HomeWork(),
//                       // ),
//                       SingleChildScrollView(
//                         child: Container(
//                           margin: EdgeInsets.symmetric(horizontal: 15),

//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(10),
//                           ),
//                           child: Column(
//                             children: [
//                               Padding(
//                                 padding: const EdgeInsets.symmetric(
//                                     vertical: 8.0, horizontal: 8.0),
//                                 child: Align(
//                                   alignment: Alignment.bottomRight,
//                                   child: Text(
//                                     "${currentDate.toLocal()}".split(' ')[0],
//                                     style: TextStyle(
//                                       fontSize: 14,
//                                       color: Colors.black54,
//                                       fontWeight: FontWeight.w400,
//                                     ),
//                                   ),
//                                 ),
//                               ),

//                               TextField(
//                                 maxLines: 4,
//                                 decoration: InputDecoration(
//                                   hintText: "Write a comment....",
//                                   hintStyle: TextStyle(fontSize: 12),
//                                   enabledBorder: OutlineInputBorder(
//                                     borderSide:
//                                         BorderSide(color: Colors.black12, width: 0.6),
//                                     borderRadius: BorderRadius.circular(30),
//                                   ),
//                                   focusedBorder: OutlineInputBorder(
//                                     borderSide:
//                                         BorderSide(color: Colors.black12, width: 0.6),
//                                     borderRadius: BorderRadius.circular(30),
//                                   ),
//                                 ),
//                                 onChanged: (commentValue) {
//                                   teacherComment = commentValue;

//                                   var singleActivity;

//                                   for (singleActivity
//                                       in activityProvider.activities) {
//                                     singleActivity.comment = teacherComment;
//                                   }


//                                 },
//                               ),
//                               Padding(
//                                 padding: const EdgeInsets.all(8.0),
//                                 child: Align(
//                                   alignment: Alignment.centerRight,
//                                   child: Container(
//                                     width: 110,
//                                     height: 35,
//                                     decoration: BoxDecoration(
//                                       color: kPrimaryColor,
//                                       borderRadius: BorderRadius.circular(10),
//                                     ),
//                                     child: TextButton(
//                                       onPressed: () async {
//                                         // activityProvider.addActivityType(0, "HomeWork");

//                                         var _activity;
//                                         listOfActivityGiven.clear();
//                                         for (_activity
//                                             in activityProvider.activities) {
//                                           if (_activity.isHWGiven == true) {
//                                             listOfActivityGiven.add({
//                                               "dueDate": _activity.hwDueDate,
//                                               "comment": _activity.comment,
//                                               "type": "home work",
//                                               "subject": _activity.subject
//                                             });
//                                           }
//                                           if (_activity.isGWGiven == true) {
//                                             listOfActivityGiven.add({
//                                               "dueDate": _activity.gwDueDate,
//                                               "comment": _activity.comment,
//                                               "type": "group work",
//                                               "subject": _activity.subject
//                                             });
//                                           }
//                                           if (_activity.isAssiGiven == true) {
//                                             listOfActivityGiven.add({
//                                               "dueDate": _activity.assiDueDate,
//                                               "comment": _activity.comment,
//                                               "type": "assignment",
//                                               "subject": _activity.subject
//                                             });
//                                           }
//                                         }
//                                         if (listOfActivityGiven.isNotEmpty) {
//                                           setState(() {
//                                             isLoading = true;
//                                           });
//                                           isPrivate
//                                               ? await performSendPrivateMessage()
//                                               : await performAddCoMBook();
//                                         }
//                                       },
//                                       child: isLoading
//                                           ? Center(
//                                               child: Container(
//                                               height: 23,
//                                               width: 23,
//                                               child: CircularProgressIndicator(
//                                                 strokeWidth: 3,
//                                                 backgroundColor: Colors.white70,
//                                               ),
//                                             ))
//                                           : Text(
//                                               "Submit",
//                                               style: TextStyle(color: Colors.white),
//                                             ),
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(height: 28.0,)
//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 );
//               },
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   performAddCoMBook() async {
//     final crid0 = Provider.of<ComBookProvider>(context, listen: false).crid;

//     _responseModel = await _authMethods.addComBook(crid0, listOfActivityGiven);

//     if (_responseModel.success == true) {
//       await Provider.of<ComBookProvider>(context, listen: false)
//           .refreshComBook();

//       Navigator.pop(context);
//     }

//     print(_responseModel.success);
//   }

//   performSendPrivateMessage() async {
//     final crid1 = Provider.of<ComBookProvider>(context, listen: false).crid;
//     final tid = Provider.of<UserProvider>(context, listen: false).getUser.tid;
//   }

// }
