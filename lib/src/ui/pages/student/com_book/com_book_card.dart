import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:intl/intl.dart';

class ComBookCard extends StatelessWidget {
  final oneComBookList;
  final date;
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  // final comBookDate;
  // final subject;
  // final dueDate;
  // final type;
  // final comment;

  ComBookCard({this.oneComBookList, this.date});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Navigator.push(context, MaterialPageRoute(builder: (context) => ));
      },
      child: Container(
        margin: EdgeInsets.all(23),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 23.0, left: 15.0),
              child: Text(
                "${DateFormat.yMMMMd().format(DateTime.parse(date))}"
                    .split('T')[0],
                style: TextStyle(
                  color: kPrimaryColor.withOpacity(0.8),
                  fontSize: 20.0,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 14.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Subject - ",
                    style: TextStyle(
                      color: kPrimaryColor,
                    ),
                  ),
                  Text(
                    "${capitalize(oneComBookList.subject)}",
                    style: TextStyle(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 150,
              child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      capitalize(
                          oneComBookList.activityList[index].type.toString()),
                      style: TextStyle(
                          color: kPrimaryColor, fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      "Due date:   ${oneComBookList.activityList[index].dueDate}"
                          .split('T')[0],
                      style: TextStyle(color: kPrimaryColor),
                    ),
                  );
                },
                itemCount: oneComBookList.activityList.length,
              ),
            ),
            Container(
              child: ExpansionTile(
                backgroundColor: kPrimaryColor,
                title: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "view comment",
                    style: TextStyle(color: kPrimaryColor.withOpacity(0.7)),
                  ),
                ),
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      oneComBookList.comment,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.start,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
