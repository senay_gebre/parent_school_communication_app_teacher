import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/provider/com_book_provider.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/common/toast.dart';
import 'package:login/src/ui/pages/student/com_book/add_com_book_page.dart';
import 'package:login/src/ui/pages/student/com_book/com_book_list.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

class ComBookPage extends StatefulWidget {
  static const String id = 'com_book_page';
  @override
  _ComBookPageState createState() => _ComBookPageState();
}

class _ComBookPageState extends State<ComBookPage> {
  AuthMethods _authMethods = AuthMethods();
  Random random = Random();
  ResponseModel _responseModel = ResponseModel();
  bool _isInitiallyExpanded = false;
  bool isFiltered = false;
  DateTime selectedDate = DateTime.now();
  String query = "";
  int randomClassNumber;
  List _comBookList;

  FToast fToast;
  var listOfUpdatedRate;
  var selectedClass;
  var _teacherClassList;

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  List<Map<String, dynamic>> classList = [];
  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
    _teacherClassList =
        Provider.of<TeacherClassProvider>(context, listen: false)
            .teacherClassList;

    refreshComBookData();
  }

  bool _isLoading = false;

  void refreshComBookData() async {
    Provider.of<ComBookProvider>(context, listen: false).refreshComBook();
  }

  @override
  Widget build(BuildContext context) {
    print("${classList} jojo");
    print(
        "${Provider.of<StudentProvider>(context, listen: false).studentMapList} ayu ayu ayu");
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AddComBookPage(
                           
                          )));
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              height: 60.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            // studentResults = Provider.of<EvaluationProvider>(
                            //         context,
                            //         listen: false)
                            //     .studentGradeReportList;
                            return Center(
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: GestureDetector(
                                  onTap: () async {
                                    setState(() {
                                      _comBookList = null;
                                    });
                                    Provider.of<ComBookProvider>(context,
                                            listen: false)
                                        .setSelectedClass(
                                            selectedClass:
                                                _teacherClassList[index]);
                                    Provider.of<ComBookProvider>(context,
                                            listen: false)
                                        .setComBookData(
                                            month: DateTime.now().month,
                                            year: DateTime.now().year);
                                    await Provider.of<ComBookProvider>(context,
                                            listen: false)
                                        .refreshComBook();

                                    // evaluationProvider.setSelectedSubject(
                                    //     subject: null);
                                  },
                                  child: Text(
                                    "Class ${_teacherClassList[index].grade.toString()}${capitalize(_teacherClassList[index].section.toString())}",
                                    style: Provider.of<ComBookProvider>(context,
                                                    listen: false)
                                                .getSelectedClass ==
                                            _teacherClassList[index]
                                        ? TextStyle(
                                            color: kPrimaryColor,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600,
                                          )
                                        : TextStyle(
                                            color: Colors.grey, fontSize: 14),
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: _teacherClassList.length,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.grey.shade200,
              height: 34,
              child: Padding(
                padding: const EdgeInsets.only(right: 10),
                child: GestureDetector(
                  onTap: () async {
                    await showMonthPicker(
                      context: context,

                      initialDate: selectedDate,
                      firstDate: DateTime(DateTime.now().year - 1, 5),
                      lastDate: DateTime(DateTime.now().year + 1, 9),
                      // builder: (context, child) {
                      //   return Column(
                      //     children: <Widget>[
                      //       Padding(
                      //         padding: const EdgeInsets.only(
                      //             top: 50.0),
                      //         child: Container(
                      //           height: 450,
                      //           width: 600,
                      //           child: child,
                      //         ),
                      //       ),
                      //     ],
                      //   );
                      // },
                    ).then((date) {
                      if (date != null) {
                        setState(() {
                          selectedDate = date;
                        });
                        Provider.of<ComBookProvider>(context, listen: false)
                            .setComBookData(
                                month: selectedDate.month,
                                year: selectedDate.year);
                        Provider.of<ComBookProvider>(context, listen: false)
                            .refreshComBook();
                      }
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.filter_list,
                        color: kPrimaryColor,
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Icon(
                        Icons.calendar_today,
                        color: kPrimaryColor,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        "${DateFormat.yMMMM().format(selectedDate)}",
                        style: TextStyle(color: kPrimaryColor),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Consumer<ComBookProvider>(
                builder: (context, comBookProvider, child) {
                  _comBookList = comBookProvider.comBookList;
                  var reversedComBookList = _comBookList != null
                      ? _comBookList.reversed.toList()
                      : [];

                  return Container(
                    child: _comBookList != null
                        ? _comBookList.isNotEmpty
                            ? ListView.builder(
                                // shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  final oneComBookDate =
                                      reversedComBookList[index].date;

                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ComBookList(
                                                    oneDayComBookList:
                                                        reversedComBookList[
                                                                index]
                                                            .assignmentsList,
                                                    date: oneComBookDate,
                                                  )));
                                    },
                                    child: Container(
                                      margin: EdgeInsets.all(23),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ]),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 23.0, left: 15.0),
                                            child: Text(
                                              "${DateFormat.yMMMMd().format(DateTime.parse(oneComBookDate))}"
                                                  .split('T')[0],
                                              style: TextStyle(
                                                color: kPrimaryColor
                                                    .withOpacity(0.8),
                                                fontSize: 28.0,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                                itemCount: reversedComBookList.length,
                              )
                            : Center(
                                child:
                                    Text("No communication book uploaded yet."),
                              )
                        : LoadingPage(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
