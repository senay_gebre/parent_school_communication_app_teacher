import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/student.dart';
import 'package:login/src/model/teacher_class.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/student/findstudent/search_student_screen.dart';
import 'package:provider/provider.dart';

class FindStudentPage extends StatefulWidget {
  @override
  _FindStudentPageState createState() => _FindStudentPageState();
}

class _FindStudentPageState extends State<FindStudentPage> {
  @override
  void initState() {
    super.initState();
  }

  final AuthMethods _authMethods = AuthMethods();
  String query = "";
  TextEditingController searchController = TextEditingController();

  String crid;

  String filterKey;

  bool isFiltered = false;

  List<Student> _listOfStudent = [];

  List<Student> _allStudentList = [];

  List<Student> studentList;

  var classList;

  Future<List<TeacherClass>> getAllClasses() async {
    classList = await _authMethods.viewMyClass();

    return classList;
  }

  allClassesList() {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              isFiltered = false;
            });
          },
          child: Container(
            height: 45,
            margin: EdgeInsets.symmetric(
              horizontal: 10.0,
            ),
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 14.0,
              ),
              child: Row(
                children: [
                  Icon(
                    Icons.filter_list_outlined,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "All",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: classList.length,
            itemBuilder: ((context, index) {
              TeacherClass singleClass = TeacherClass(
                  crid: classList[index].crid,
                  grade: classList[index].grade,
                  section: classList[index].section);
              var className =
                  "${singleClass.grade.toString()}${singleClass.section.toUpperCase()}";

              return GestureDetector(
                onTap: () {
                  isFiltered = true;

                  setState(() {
                    filterKey = classList[index].crid;
                  });
                },
                child: Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 10.0,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 14.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.filter_list_outlined,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          className,
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ),
      ],
    );
  }

  buildSuggestions(String query) {
    print(
        "$studentList &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&**********");

    final List<Student> suggestionList = query.isEmpty
        ? []
        : studentList != null
            ? studentList.where((Student student) {
                String _getFirstName = student.firstName.toLowerCase();
                String _query = query.toLowerCase();
                String _getLastName = student.lastName.toLowerCase();
                bool matchesFirstName = _getFirstName.contains(_query);
                bool matchesLastName = _getLastName.contains(_query);

                return (matchesFirstName || matchesLastName);

                // (User user) => (user.username.toLowerCase().contains(query.toLowerCase()) ||
                //     (user.name.toLowerCase().contains(query.toLowerCase()))),
              }).toList()
            : [];

    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: ((context, index) {
        Student searchedStudent = Student(
            firstName: suggestionList[index].firstName,
            lastName: suggestionList[index].lastName,
            gender: suggestionList[index].gender);

        return ListTile(
          onTap: () {
            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //         builder: (context) => ChatScreen(
            //               receiver: searchedUser,
            //             )));
          },
          // leading: CachedImage(
          //   searchedUser.profilePhoto,
          //   radius: 25,
          //   isRound: true,
          // ),
          // leading: CircleAvatar(
          //   backgroundImage: NetworkImage(searchedUser.profilePhoto),
          //   backgroundColor: Colors.grey,
          // ),
          title: Text(
            "${searchedStudent.firstName} ${searchedStudent.firstName}",
            style: TextStyle(
              fontWeight: FontWeight.w400,
            ),
          ),
          subtitle: Text(
            searchedStudent.gender,
            style: TextStyle(color: Colors.grey),
          ),
          leading: CircleAvatar(),
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
      ),
      body: Container(
        color: kPrimaryColor,
        //padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.25),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    // suffixIcon: IconButton(
                    //   onPressed: () {
                    //     Navigator.push(
                    //         context,
                    //         MaterialPageRoute(
                    //             builder: (context) => SearchStudentScreen()));
                    //   },
                    //   icon: Icon(Icons.search),
                    // ),
                    contentPadding:
                        const EdgeInsets.fromLTRB(20.0, 17.0, 20.0, 17.0),
                    hintText: "Search student...",
                  ),
                  onChanged: (val) {
                    setState(() {
                      query = val;
                    });
                  },
                ),
              ),
            ),
            Expanded(
              child: FutureBuilder(
                future: getAllClasses(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    print(
                        "${snapshot.data}  0000000000000000000000000000000000000000000000");

                    // Provider.of<StudentProvider>(context, listen: false)
                    //     .refreshStudentList(classList[0].crid);
                    return query.isNotEmpty
                        ? buildSuggestions(query)
                        : buildStudentListContent();
                  } else {
                    return LoadingPage();
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Column buildStudentListContent() {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: allClassesList(),
          ),
        ),
        Expanded(
          flex: 8,
          child: Container(
            height: 50,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
            ),
            child: Consumer<StudentProvider>(
              builder: (context, studentProvider, child) {
                // _allStudentList = studentProvider.allStudentList;
                // studentList = studentProvider.allStudentList;
                if (isFiltered == true) {
                  _listOfStudent.clear();
                  for (var _singleStudent in studentProvider.allStudentList) {
                    if (_singleStudent.crid == filterKey) {
                      _listOfStudent.add(_singleStudent);
                    }
                  }
                } else {
                  _listOfStudent.clear();
                  for (var _singleStudent in studentProvider.allStudentList) {
                    _listOfStudent.add(_singleStudent);
                  }
                }

                return ListView.builder(
                  itemBuilder: (context, index) {
                    var student = _listOfStudent[index];
                    var grade, section;
                    for (var aSingleClass in classList) {
                      if (aSingleClass.crid == student.crid) {
                        grade = aSingleClass.grade;
                        section = aSingleClass.section;
                      }
                    }

                    return ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.blueAccent,
                      ),
                      title: Text("${student.firstName} ${student.lastName}"),
                      subtitle:
                          Text("${grade}${section.toString().toUpperCase()}"),
                    );
                  },
                  itemCount: _listOfStudent.length,
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
