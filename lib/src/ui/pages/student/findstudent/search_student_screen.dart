// import 'package:flutter/material.dart';
// import 'package:login/src/model/student.dart';
// import 'package:login/src/resources/auth_methods.dart';

// class SearchStudentScreen extends StatefulWidget {
//   @override
//   _SearchStudentScreenState createState() => _SearchStudentScreenState();
// }

// class _SearchStudentScreenState extends State<SearchStudentScreen> {
//   final AuthMethods _authMethods = AuthMethods();
//   String query = "";
//   TextEditingController searchController = TextEditingController();

//   //   buildSuggestions(String query) {
//   //   final List<Student> suggestionList = query.isEmpty
//   //       ? []
//   //       : userList != null
//   //           ? userList.where((UserFile user) {
//   //               String _getUsername = user.username.toLowerCase();
//   //               String _query = query.toLowerCase();
//   //               String _getName = user.name.toLowerCase();
//   //               bool matchesUsername = _getUsername.contains(_query);
//   //               bool matchesName = _getName.contains(_query);

//   //               return (matchesUsername || matchesName);

//   //               // (User user) => (user.username.toLowerCase().contains(query.toLowerCase()) ||
//   //               //     (user.name.toLowerCase().contains(query.toLowerCase()))),
//   //             }).toList()
//   //           : [];

//   //   return ListView.builder(
//   //     itemCount: suggestionList.length,
//   //     itemBuilder: ((context, index) {
//   //       UserFile searchedUser = UserFile(
//   //           uid: suggestionList[index].uid,
//   //           profilePhoto: suggestionList[index].profilePhoto,
//   //           name: suggestionList[index].name,
//   //           username: suggestionList[index].username);

//   //       return CustomTile(
//   //         mini: false,
//   //         onTap: () {
//   //           Navigator.push(
//   //               context,
//   //               MaterialPageRoute(
//   //                   builder: (context) => ChatScreen(
//   //                         receiver: searchedUser,
//   //                       )));
//   //         },
//   //         leading: CachedImage(
//   //           searchedUser.profilePhoto,
//   //           radius: 25,
//   //           isRound: true,
//   //         ),
//   //         // leading: CircleAvatar(
//   //         //   backgroundImage: NetworkImage(searchedUser.profilePhoto),
//   //         //   backgroundColor: Colors.grey,
//   //         // ),
//   //         title: Text(
//   //           searchedUser.username,
//   //           style: TextStyle(
//   //             color: Colors.white,
//   //             fontWeight: FontWeight.bold,
//   //           ),
//   //         ),
//   //         subtitle: Text(
//   //           searchedUser.name,
//   //           style: TextStyle(color: UniversalVariables.greyColor),
//   //         ),
//   //       );
//   //     }),
//   //   );
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(10.0),
//       child: Container(
//         decoration: BoxDecoration(
//           color: Colors.white.withOpacity(0.25),
//           borderRadius: BorderRadius.circular(20),
//         ),
//         child: TextFormField(
//           style: TextStyle(color: Colors.white),
//           decoration: InputDecoration(
//               enabledBorder: InputBorder.none,
//               focusedBorder: InputBorder.none,
//               suffixIcon: IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.search),
//               ),
//               contentPadding: const EdgeInsets.fromLTRB(20.0, 17.0, 20.0, 17.0),
//               hintText: "Search student..."),
//         ),
//       ),
//     );
//   }
// }
