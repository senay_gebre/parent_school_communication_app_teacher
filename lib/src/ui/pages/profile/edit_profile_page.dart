import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/model/user_file.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/editprof_form_field.dart';
import 'package:login/src/ui/pages/common/toast.dart';
import 'package:login/src/ui/pages/profile/prof_components/profile_pic.dart';
import 'package:login/src/utils/validation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:image/image.dart' as Im;
import 'package:firebase_core/firebase_core.dart';
import 'package:path/path.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class EditProfilePage extends StatefulWidget {
  final UserFile currentUser;
  EditProfilePage({this.currentUser});
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  CustomValidator _validator = CustomValidator();
  AuthMethods _authMethods = AuthMethods();
  int _radioValue;
  bool _isLoading = false;
  Color _radioButtonOne = Colors.grey;
  Color _radioButtonTwo = Colors.grey;
  IconData _passHiddenIcon = Icons.visibility_off;
  IconData _passVisibleIcon = Icons.visibility;
  bool showPassword = true;
  UserFile _user = UserFile();
  bool autoValidator = false;
  UserFile _currentUser;
  FToast fToast;
  var _imageFile;
  final ImagePicker _picker = ImagePicker();

  final _myFormKey = GlobalKey<FormState>();

  void getImage(ImageSource source) async {
    final pickedFile = await _picker.getImage(source: source);
    var imageFile = await compressImage(File(pickedFile.path));
    setState(() {
      _imageFile = imageFile;
    });
  }

  Future uploadImage() async {
    String fileName = basename(_imageFile.path).toString();
    firebase_storage.Reference reference = firebase_storage
        .FirebaseStorage.instance
        .ref()
        .child("profiles/${DateTime.now().millisecondsSinceEpoch}");
    firebase_storage.UploadTask uploadTask = reference.putFile(_imageFile);
    firebase_storage.TaskSnapshot taskSnapshot = await uploadTask.whenComplete(
        () => print(
            "File Uploaded 7777777777777777777777777777777777777777777777777 "));
    var url = await reference.getDownloadURL();
    user.profilePic = url.toString();
  }

  static Future<File> compressImage(File imageToCompress) async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    int rand = Random().nextInt(10000);

    Im.Image image = Im.decodeImage(imageToCompress.readAsBytesSync());
    Im.copyResize(image, width: 500, height: 500);

    return new File('$path/img_$rand.jpg')
      ..writeAsBytesSync(Im.encodeJpg(image, quality: 85));
  }

  @override
  void initState() {
    super.initState();
    Firebase.initializeApp();
    _currentUser = widget.currentUser;
    fToast = FToast();
    fToast.init(this.context);
  }

  var user;
  var userToken;
  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _radioButtonOne = Colors.black;
          _radioButtonTwo = Colors.grey;
          _user.gender = "Male";
          break;
        case 1:
          _radioButtonTwo = Colors.black;
          _radioButtonOne = Colors.grey;
          _user.gender = "Female";
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Edit Profile",
          style: TextStyle(color: Color(0xff0f507d)),
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Color(0xff0f507d),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        shadowColor: Color(0xff0f507d),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Form(
          key: _myFormKey,
          child: ListView(
            children: [
              SizedBox(
                height: 15,
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Stack(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.height * 0.15,
                        height: MediaQuery.of(context).size.height * 0.15,
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 2,
                              color: Theme.of(context).scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 1.5,
                                blurRadius: 10,
                                color: Color(0xff0f507d).withOpacity(0.1),
                                offset: Offset(0, 10))
                          ],
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: _imageFile == null
                                ? AssetImage(
                                    'assets/theimages/profile_placeholder.png',
                                  )
                                : FileImage(File(_imageFile.path)),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 4,
                              color: Theme.of(context).scaffoldBackgroundColor,
                            ),
                            color: Color(0xff0f507d),
                          ),
                          child: GestureDetector(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                         builder: (builder) => bottomSheet(),
                              );
                            },
                            child: Icon(
                              Icons.photo_camera,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 35,
              ),
              EditProfileField(
                labelText: "First Name",
                //  validator: _validator.nameValidator,
                onChanged: (value) {
                  _user.firstName = value;
                },
                hintText: _currentUser.firstName,
              ),
              EditProfileField(
                labelText: "Last Name",
                // validator: _validator.nameValidator,
                onChanged: (value) {
                  _user.lastName = value;
                },
                hintText: _currentUser.lastName,
              ),
              EditProfileField(
                  labelText: "Username",
                  // validator: _validator.usernameValidator,
                  onChanged: (value) {
                    _user.username = value;
                  },
                  hintText: _currentUser.username),
              EditProfileField(
                labelText: "Email address",
                // validator: _validator.emailddressValidator,
                onChanged: (value) {
                  _user.email = value;
                },
                hintText: _currentUser.email,
              ),
              EditProfileField(
                labelText: "Current address",
                //   validator: _validator.nameValidator,
                onChanged: (value) {
                  _user.currentAddress = value;
                },
                hintText: _currentUser.currentAddress,
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: Column(
                  children: [
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16),
                        child: Text(
                          "Gender",
                          style: TextStyle(color: Colors.grey.shade500),
                        )),
                    Row(
                      children: [
                        Radio(
                          activeColor: kPrimaryColor,
                          value: 0,
                          groupValue: _radioValue,
                          onChanged: _handleRadioValueChange,
                        ),
                        Text(
                          'Male',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: _radioButtonOne,
                          ),
                        ),
                        Radio(
                          value: 1,
                          groupValue: _radioValue,
                          onChanged: _handleRadioValueChange,
                        ),
                        Text(
                          'Female',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: _radioButtonTwo,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              EditProfileField(
                labelText: "Phone Number",
                //   validator: _validator.phoneNumberValidator,
                keyboardType: TextInputType.phone,
                hintText: "0" + _currentUser.phone.toString(),
                onChanged: (val) {
                  _user.phone = val;
                },
              ),
              SizedBox(
                height: 35,
              ),
              Container(
                height: 50,
                decoration: BoxDecoration(
                    color: kPrimaryColor,
                    borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                  onPressed: () async {
                    performEditProfile();
                  },
                  child: Text(
                    "Save",
                    style: TextStyle(
                        fontSize: 14, letterSpacing: 2.2, color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performEditProfile() async {
    setState(() {
      autoValidator = true;
    });
    if (_myFormKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      if (_imageFile != null) {
        await uploadImage();
      }

      ResponseModel _response = await _authMethods.changeProfileData(_user);
      //print("${_response.msg} 444444444444444444444444444444444");
      if (_response.success == true) {
        fToast.showToast(
          child: CustomToast(
            message: _response.msg,
            icon: Icons.check,
            color: Color(0xff13e8ac),
          ),
          gravity: ToastGravity.BOTTOM,
          toastDuration: Duration(seconds: 2),
        );
      } else {
        fToast.showToast(
          child: CustomToast(
            message: _response.msg,
            icon: Icons.error,
            color: Color(0xffF14E4C),
          ),
          gravity: ToastGravity.BOTTOM,
          toastDuration: Duration(seconds: 2),
        );
      }
      Navigator.pop(this.context);

      print("VALIDATED");
    } else {
      print("NOT VALIDATED");
    }
  }

  Widget bottomSheet() {
    return SingleChildScrollView(
      child: Container(
        height: 100.0,
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Choose profile picture"),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    getImage(ImageSource.camera);
                  },
                  child: Icon(
                    Icons.camera,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    getImage(ImageSource.gallery);
                  },
                  child: Icon(
                    Icons.image,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
