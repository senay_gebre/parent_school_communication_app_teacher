import 'dart:io';
import 'dart:math';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as Im;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ProfilePicChange extends StatefulWidget {
  final profileImage;

  ProfilePicChange({this.profileImage});
  @override
  _ProfilePicChangeState createState() => _ProfilePicChangeState();
}

class _ProfilePicChangeState extends State<ProfilePicChange> {
  var _imageFile;
  File _compressedImage;
  final ImagePicker _picker = ImagePicker();


  void getImage(ImageSource source) async {
    final pickedFile = await _picker.getImage(source: source);
    // File image;
    // if (isCamera) {
    //   image = (await ImagePicker.platform.pickImage(source: ImageSource.camera))
    //       as File;
    // } else {
    //   image = (await ImagePicker.platform
    //       .pickImage(source: ImageSource.gallery)) as File;
    // }

    var imageFile = await compressImage(File(pickedFile.path));
    setState(() {
      _imageFile = imageFile;
    });
  }

  //   Future<File> pickImage({@required ImageSource source}) async {
  //      final pickedFile = await _picker.getImage(source: source);
  //   return await compressImage(selectedImage);
  // }

  static Future<File> compressImage(File imageToCompress) async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    int rand = Random().nextInt(10000);

    Im.Image image = Im.decodeImage(imageToCompress.readAsBytesSync());
    Im.copyResize(image, width: 500, height: 500);

    return new File('$path/img_$rand.jpg')
      ..writeAsBytesSync(Im.encodeJpg(image, quality: 85));
  }

  @override
  Widget build(BuildContext context) {
    //var _compressedImage = compressImage(FileImage(_imageFile.path));
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.height * 0.15,
            height: MediaQuery.of(context).size.height * 0.15,
            decoration: BoxDecoration(
              border: Border.all(
                  width: 2, color: Theme.of(context).scaffoldBackgroundColor),
              boxShadow: [
                BoxShadow(
                    spreadRadius: 1.5,
                    blurRadius: 10,
                    color: Color(0xff0f507d).withOpacity(0.1),
                    offset: Offset(0, 10))
              ],
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: _imageFile == null
                    ? AssetImage(
                        'assets/theimages/profile_placeholder.png',
                      )
                    : FileImage(File(_imageFile.path)),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 4,
                  color: Theme.of(context).scaffoldBackgroundColor,
                ),
                color: Color(0xff0f507d),
              ),
              child: GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (builder) => bottomSheet(),
                  );
                },
                child: Icon(
                  Icons.photo_camera,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget bottomSheet() {
    return Container(
      height: 100.0,
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("Choose profile picture"),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                onPressed: () {
                  getImage(ImageSource.camera);
                },
                child: Icon(
                  Icons.camera,
                ),
              ),
              TextButton(
                onPressed: () {
                  getImage(ImageSource.gallery);
                },
                child: Icon(
                  Icons.image,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
