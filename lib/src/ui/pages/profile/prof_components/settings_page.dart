import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/provider/user_provider.dart';
import 'package:login/src/ui/pages/profile/edit_profile_page.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String capitalizeFirstLetter(String s) =>
      (s?.isNotEmpty ?? false) ? '${s[0].toUpperCase()}${s.substring(1)}' : s;
  String capitalize(String string) {
    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey,
          ),
        ),
      ),
      body: Container(
        // padding: EdgeInsets.only(left: 16, top: 25, right: 16),
        child: ListView(
          children: [
            Consumer<UserProvider>(
              builder: (context, userProvider, child) {
                return Container(
                  margin: EdgeInsets.all(0),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: kPrimaryColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        child: CircleAvatar(
                          maxRadius:
                              MediaQuery.of(context).size.aspectRatio * 60,
                          backgroundColor: Colors.white,
                          backgroundImage: userProvider.getUser.profilePic !=
                                  null
                              ? NetworkImage(userProvider.getUser.profilePic)
                              : NetworkImage(
                                  'https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png'),
                        ),
                        decoration: BoxDecoration(
                          color: kPrimaryColor,
                        ),
                      ),
                      SizedBox(
                        height: 1,
                      ),
                      Text(
                        capitalizeFirstLetter(
                                userProvider.getUser.firstName.toString()) +
                            " " +
                            capitalizeFirstLetter(
                                userProvider.getUser.lastName.toString()),
                        style: TextStyle(color: Colors.white),
                      ),
                      Text(
                        'Parent',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w300),
                      ),
                      SizedBox(
                        height: 11,
                      ),
                    ],
                  ),
                );
              },
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              children: [
                Icon(
                  Icons.person,
                  color: kPrimaryColor,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "Account",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),
            SizedBox(
              height: 10,
            ),
            buildAccountOptionRow(
              context,
              "My Account",
            ),
            // buildAccountOptionRow(context, "Change username"),
            // buildAccountOptionRow(context, "Change password"),
            // buildAccountOptionRow(context, "Email"),
            // buildAccountOptionRow(context, "Phone"),
            SizedBox(
              height: 40,
            ),
            Row(
              children: [
                Icon(
                  Icons.volume_up_outlined,
                  color: kPrimaryColor,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "App Settings",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),
            SizedBox(
              height: 10,
            ),
            buildAccountOptionRow(context, "Appearance"),

            buildNotificationOptionRow("Dark Mode", false),

            buildAccountOptionRow(context, "Language"),

            SizedBox(
              height: 50,
            ),
            Row(
              children: [
                Icon(
                  Icons.volume_up_outlined,
                  color: kPrimaryColor,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "Information",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),

            SizedBox(
              height: 10,
            ),
            buildAccountOptionRow(context, "Support"),

            buildAccountOptionRow(context, "FAQs"),

            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  Row buildNotificationOptionRow(String title, bool isActive) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: Colors.grey[600]),
        ),
        Transform.scale(
            scale: 0.7,
            child: CupertinoSwitch(
              value: isActive,
              activeColor: kPrimaryColor,
              onChanged: (bool val) {},
            ))
      ],
    );
  }

  GestureDetector buildAccountOptionRow(BuildContext context, String title) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => EditProfilePage()));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Colors.grey[600],
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }
}
