import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/message.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/provider/user_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class ChatScreen extends StatefulWidget {
  final String receiverStudentFirstName;
  final String receiverStudentLastName;
  final String receiverStudentIdNo;
  final String receiverStudentCurrentAddress;
  final String receiverStudentGender;
  final String receiverStudentProfilePic;
  final String receiverStudentParentId;
  final String receiverStudentId;

  ChatScreen({
    @required this.receiverStudentCurrentAddress,
    @required this.receiverStudentGender,
    @required this.receiverStudentId,
    @required this.receiverStudentIdNo,
    @required this.receiverStudentLastName,
    @required this.receiverStudentParentId,
    @required this.receiverStudentProfilePic,
    @required this.receiverStudentFirstName,
  });

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final textController = TextEditingController();
  AuthMethods _authMethods = AuthMethods();
  ResponseModel _responseModel = ResponseModel();

  String senderId;
  String receiverId;
  String senderRole;
  String receiverRole;
  String messageText;

  @override
  void initState() {
    super.initState();
    senderId = Provider.of<UserProvider>(context, listen: false).getUser.tid;
    senderRole = Provider.of<UserProvider>(context, listen: false).getUser.role;

    receiverId = widget.receiverStudentId;
    receiverRole = "parent";
  }

  Future<ResponseModel> getMessages() async {
    _responseModel =
        await _authMethods.getMessages(stuId: widget.receiverStudentId);
    return _responseModel;
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.receiverStudentFirstName,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Container(
                child: FutureBuilder(
                  future: getMessages(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      final messages = snapshot.data.response.reversed;
                      List<MessageBubble> messageBubbles = [];

                      for (var message in messages) {
                        final messageText = message.message;

                        final messageSenderId = message.senderId;
                        final messageDate = message.messageDate;

                        final messageBubble = MessageBubble(
                          text: messageText,
                          isMe: messageSenderId == senderId,
                          textDate: messageDate,
                        );

                        messageBubbles.add(messageBubble);
                      }

                      print(
                          "${widget.receiverStudentCurrentAddress} ${widget.receiverStudentFirstName} ${widget.receiverStudentParentId}  lasdkjflkdsjfklsfjsdklfjdslkfjdsklfj");
                      return ListView(
                        shrinkWrap: true,
                        reverse: true,
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 20.0),
                        children: messageBubbles,
                      );
                    } else {
                      return Center(child: LoadingPage());
                    }
                  },
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: kPrimaryColor, width: 2.0),
                ),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: textController,
                      onChanged: (value) {
                        messageText = value;
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        hintText: 'Type your message here...',
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  FlatButton(
                    onPressed: () async {
                      Message _message = Message(
                          senderId: senderId,
                          senderRole: senderRole,
                          receiverId: receiverId,
                          receiverRole: receiverRole,
                          message: messageText);
                      _responseModel = await _authMethods.sendMessage(_message);

                      print(
                          "${_message.message} ${_message.senderRole} ${_message.senderId} ${_message.receiverId} ${_message.receiverRole}^^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%");

                      print(
                          "${_responseModel.success} ${_responseModel.msg} ${_responseModel.response} ^^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%");
                      textController.clear();
                      if (_responseModel.success) {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    super.widget));
                      }
                    },
                    child: Text(
                      'Send',
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  MessageBubble({this.text, this.isMe, this.textDate});

  final String text;
  final bool isMe;
  final String textDate;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 6.0, top: 10.0, bottom: 10.0, left: 10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Material(
            borderRadius: isMe
                ? BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    bottomLeft: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0),
                  )
                : BorderRadius.only(
                    bottomLeft: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0),
                    topRight: Radius.circular(15.0),
                  ),
            elevation: 5.0,
            color: isMe ? Color(0xff156885) : Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Text(
                text,
                style: TextStyle(
                  color: isMe ? Colors.white : Colors.black54,
                  fontSize: 15.0,
                ),
              ),
            ),
          ),
          Text(
            "${DateFormat.yMMMMd().format(DateTime.parse(textDate))}"
                .split('T')[0],
            style: TextStyle(
              fontSize: 10.0,
              color: Colors.black54,
            ),
          ),
        ],
      ),
    );
  }
}
