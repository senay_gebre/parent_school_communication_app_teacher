import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final String label;
  final Function onPress;
  const RoundedButton({this.label, this.onPress});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xff156a8a),
          borderRadius: BorderRadius.circular(6),
        ),
        child: TextButton(
          style: TextButton.styleFrom(
            padding: EdgeInsets.all(12),
          ),
          onPressed: onPress,
          child: Text(
            label,
            style: TextStyle(
                fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
