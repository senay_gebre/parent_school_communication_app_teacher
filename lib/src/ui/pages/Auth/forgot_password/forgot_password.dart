import 'package:flutter/material.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/ui/pages/Auth/forgot_password/email_recovery.dart';

class ForgotPasswordPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: kPrimaryColor,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Forgot Password",
              style: TextStyle(
                  color: kPrimaryColor,
                  fontWeight: FontWeight.w500,
                  fontSize: MediaQuery.of(context).size.aspectRatio * 90),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "Select a method to recover your follow-upp account password.",
              style: TextStyle(
                  color: Colors.grey.withOpacity(0.8),
                  fontSize: MediaQuery.of(context).size.aspectRatio * 23),
            ),
            SizedBox(height: 23),
            ForgotPasswordItem(
              icon: Icons.alternate_email,
              title: "Reset password via  email",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EmailRecovery()));
              },
            ),
            SizedBox(height: size.height * 0.03),
            ForgotPasswordItem(
              icon: Icons.message,
              title: "Reset password via text message",
            ),
            SizedBox(height: size.height * 0.03),
            ForgotPasswordItem(
              icon: Icons.security,
              title: "Reset password via security questions.",
            ),
          ],
        ),
      ),
    );
  }
}

class ForgotPasswordItem extends StatelessWidget {
  final String title;
  final Function onTap;
  final IconData icon;

  const ForgotPasswordItem({Key key, this.title, this.onTap, this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
        child: GestureDetector(
          onTap: onTap,
          child: Card(
            shadowColor: kPrimaryColor.withOpacity(0.2),
            color: kPrimaryColor.withOpacity(0.1),
            child: Center(
              child: ListTile(
                leading: Icon(
                  icon,
                  color: kPrimaryColor.withOpacity(0.8),
                  size: 34,
                ),
                title: Text(
                  title,
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.7),
                      fontSize: MediaQuery.of(context).size.aspectRatio * 28),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
