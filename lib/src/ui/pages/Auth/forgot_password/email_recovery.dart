import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/ui/pages/Auth/forgot_password/emailEnterCode.dart';

class EmailRecovery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: kPrimaryColor,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Container(
                //   height: MediaQuery.of(context).size.aspectRatio * 160,
                //   width: MediaQuery.of(context).size.aspectRatio * 160,
                //   child: Padding(
                //     padding: const EdgeInsets.all(20),
                //     child: SvgPicture.asset(
                //       'assets/email.svg',
                //       color: Colors.white,
                //     ),
                //   ),
                //   decoration: BoxDecoration(
                //       borderRadius: BorderRadius.circular(100),
                //       color: kPrimaryColor),
                // ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 0.0, top: 12, right: 40.0, bottom: 12),
                  child: Text(
                    "Email Recovery",
                    style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: size.width * 0.12,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ),
                SizedBox(height: 15.0),

                Text(
                  "Provide your account's email for which you want to recover your password.",
                  style: TextStyle(
                      color: Colors.grey.shade700,
                      fontSize: 15,
                      fontWeight: FontWeight.w300),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: 25.0),

                SizedBox(height: 25.0),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email),
                    filled: true,
                    fillColor: Colors.grey.withOpacity(0.1),
                    hintText: 'Enter your Email',
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.076,
                    color: kPrimaryColor,
                    child: RawMaterialButton(
                      child: Text(
                        "Next",
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                      padding: EdgeInsets.zero,
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EmailEnterCode()));
                      },
                    )),
                SizedBox(
                  height: 50.0,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    "Try another way",
                    style: TextStyle(
                        color: kPrimaryColor.withOpacity(0.8),
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                    textAlign: TextAlign.right,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
