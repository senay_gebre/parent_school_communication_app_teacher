import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login/main.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/model/user_file.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/Auth/auth_components/rounded_button.dart';
import 'package:login/src/ui/pages/Auth/forgot_password/forgot_password.dart';
import 'package:login/src/utils/validation.dart';
// import 'package:login/src/ui/pages/Auth/signup/signup_page.dart';
import 'package:login/src/ui/pages/common/loading_page.dart';
import 'package:login/src/ui/pages/common/toast.dart';

class LoginPage extends StatefulWidget {
  static const String id = "login_page";
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _storage = new FlutterSecureStorage();
  final _formKey = GlobalKey<FormState>();
  AuthMethods _authMethods = AuthMethods();
  CustomValidator _validator = CustomValidator();
  UserFile _user = UserFile();
  ResponseModel _responseModel = ResponseModel();

  bool _passHidden = true;
  bool _isLoginPressed = false;
  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  // @override
  // void dispose() {
  //   FocusManager.instance.primaryFocus.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        decoration: kBackground,
        child: Stack(
          children: [
            Center(
              child: Form(
                key: _formKey,
                child: ListView(
                  shrinkWrap: true,
                  padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                  children: <Widget>[
                    SizedBox(height: size.height * 0.15),
                    TextFormField(
                      decoration:
                          kAuthInputDecoration.copyWith(labelText: 'Username'),
                      validator: _validator.emptyFieldValidator,
                      onChanged: (String value) {
                        _user.username = value;
                      },
                    ),
                    SizedBox(height: 18.0),
                    TextFormField(
                      decoration: kAuthInputDecoration.copyWith(
                        labelText: 'Password',
                        suffixIcon: IconButton(
                          icon: Icon(_passHidden
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            setState(() {
                              _passHidden = !_passHidden;
                            });
                          },
                        ),
                      ),
                      obscureText: _passHidden,
                      validator: _validator.emptyFieldValidator,
                      onChanged: (String value) {
                        _user.password = value;
                      },
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ForgotPasswordPage()));
                      },
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Forgot Password ?",
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: kPrimaryColor),
                        ),
                      ),
                    ),
                    SizedBox(height: 24.0),
                    RoundedButton(
                      label: "Log In",
                      onPress: () {
                        performLogin();
                        FocusManager.instance.primaryFocus.unfocus();
                      },
                    ),
                    SizedBox(
                      height: size.height * 0.26,
                    ),
                  ],
                ),
              ),
            ),
            _isLoginPressed ? LoadingPage() : Container(),
          ],
        ),
      ),
    );
  }

  Future performLogin() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    _formKey.currentState.save();

    setState(() {
      _isLoginPressed = true;
    });

    _responseModel = await _authMethods.signIn(_user);

    if (_responseModel.success == true) {
      setState(() {
        _isLoginPressed = false;
      });

      await _storage.write(
          key: "token", value: _responseModel.response["token"]);
      await _storage.write(
          key: "user", value: jsonEncode(_responseModel.response['user']));

      Navigator.of(context).push(
        MaterialPageRoute(builder: (BuildContext context) => MyApp()),
      );

      fToast.showToast(
        child: CustomToast(
          message: _responseModel.msg,
          icon: Icons.check,
          color: Color(0xff13e8ac),
        ),
        gravity: ToastGravity.BOTTOM,
        toastDuration: Duration(seconds: 4),
      );
    } else {
      setState(() {
        _isLoginPressed = false;
      });

      fToast.showToast(
        child: CustomToast(
          message: _responseModel.msg,
          icon: Icons.error,
          color: Color(0xffF14E4C),
        ),
        gravity: ToastGravity.BOTTOM,
        toastDuration: Duration(seconds: 2),
      );
    }
  }
}
