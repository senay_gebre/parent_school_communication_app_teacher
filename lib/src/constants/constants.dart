import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF035A78);
const kDrawerListTileColor = Color(0xff414141);
const kBackground = BoxDecoration(
  image: DecorationImage(
    image: AssetImage('assets/theimages/background/login_background.png'),
    fit: BoxFit.cover,
  ),
);
final kAuthInputDecoration = InputDecoration(
  fillColor: kPrimaryColor.withOpacity(0.1),
  filled: true,
  contentPadding: const EdgeInsets.fromLTRB(20.0, 17.0, 20.0, 17.0),
  labelText: "",
  labelStyle: TextStyle(fontSize: 14, color: Colors.grey.shade600),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(5),
    borderSide: BorderSide(
      color: Colors.grey.shade600,
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(5),
    borderSide: BorderSide(
      width: 1.5,
      color: kPrimaryColor,
    ),
  ),
  errorBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(5),
    borderSide: BorderSide(
      width: 1.2,
      color: Color(0xffed3d46),
    ),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(5),
    borderSide: BorderSide(
      width: 1.5,
      color: Color(0xffed3d46),
    ),
  ),
);

const List<Map> subjectList = [
  {"subject_name": "English", "subject_icon": "assets/icons/english_icon.png"},
  {"subject_name": "Mathematics", "subject_icon": "assets/icons/math_icon.png"},
  {"subject_name": "Science", "subject_icon": "assets/icons/science_icon.png"},
  {"subject_name": "Physics", "subject_icon": "assets/icons/physics_icon.png"},
  {"subject_name": "Biology", "subject_icon": "assets/icons/biology_icon.png"},
];
