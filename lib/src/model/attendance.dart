class Attendance {
  String date;
  bool present;
  bool permitted;
  bool sLate;
  String stuId;
  String attenId;

  Attendance(
      {this.permitted = false,
      this.present = false,
      this.sLate = false,
      this.stuId});

  Map<String, dynamic> toMap() {
    var convertedMap = <String, dynamic>{};
    if (this.present == true) {
      convertedMap = {"stuid": this.stuId, "present": true};
    } else if (this.permitted == true) {
      convertedMap = {"stuid": this.stuId, "permitted": true};
    } else if (this.sLate == true) {
      convertedMap = {"stuid": this.stuId, "late": true};
    } else {
      convertedMap = {"stuid": this.stuId};
    }

    return convertedMap;
  }

  Attendance.fromMap(Map<String, dynamic> mapData) {
    this.attenId = mapData['attendance']['_id'];
    this.date = mapData['date'].toString().split('T')[0];
    this.permitted = mapData['attendance']['permitted'];
    this.present = mapData['attendance']['present'];
    this.sLate = mapData['attendance']['late'];
    this.stuId = mapData['attendance']['stuid'];
  }
}
