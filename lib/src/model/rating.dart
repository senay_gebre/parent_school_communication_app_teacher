class Rating {
  String stuIdNo;
  String rateDate;
  String subject;
  int comSkill;
  int psSkill;
  int parSkill;
  String msg;

  Rating(
      {this.comSkill,
      this.stuIdNo,
      this.msg,
      this.rateDate,
      this.parSkill,
      this.psSkill,
      this.subject});

  Map<String, dynamic> toMap() {
    return {
      'stuid': this.stuIdNo,
      "status": {
        "date": this.rateDate,
        "subject": this.subject,
        "skills": {
          "comSkill": this.comSkill,
          "probSolving": this.psSkill,
          "participation": this.parSkill
        }
      }
    };
  }
}
