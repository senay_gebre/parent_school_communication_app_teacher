class GradeReport {
  String grid;
  String studId;
  int academicYear;
  String quarter;
  String subject;
  List listOfResult;
  double total;

  GradeReport({
    this.academicYear,
    this.grid,
    this.listOfResult,
    this.studId,
    this.quarter,
    this.subject,
    this.total,
  });

  GradeReport.fromMap(Map<String, dynamic> mapData) {
    this.academicYear = mapData['academicYear'];
    this.grid = mapData['grid'];
    this.quarter = mapData['quarter'];
    this.subject = mapData['subject'];
    this.studId = mapData['_id'];
    this.total = mapData['total'].isNotEmpty
        ? double.parse(mapData['total'][0]['total'].toString())
        : 0.0;
    // this.listOfResult = mapData['results'];
  }
}
