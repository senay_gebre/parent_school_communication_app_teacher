class Event {
  String eventId;
  String eventTitle;
  String startDate;
  String endDate;
  String imageURL;
  String eventDescription;
  String date;
  var comments;

  Event({
    this.comments,
    this.date,
    this.endDate,
    this.eventId,
    this.eventTitle,
    this.eventDescription,
    this.imageURL,
    this.startDate,
  });

  Event.fromMap(Map<String, dynamic> mapData) {
    this.eventId = mapData['_id'];
    this.eventTitle =
        mapData.containsKey("title") ? mapData['title'] : "unamed event";
    this.eventDescription =
        mapData.containsKey("event") ? mapData['event'] : "";
    this.imageURL = mapData['image'];
    this.startDate = mapData['startDate'];
    this.endDate = mapData['endDate'];
    this.date = mapData['date'];
    this.comments = mapData['comments'];
  }
}
