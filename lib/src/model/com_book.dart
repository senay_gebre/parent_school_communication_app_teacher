class ComBook {
  String comid;
  String crid;
  String date;
  List<dynamic> assignmentsList = [];

  ComBook({this.assignmentsList, this.comid, this.crid, this.date});

  ComBook.fromMap(Map<String, dynamic> mapData) {
    this.date = mapData['date'];
    this.crid = mapData['crid'];
    this.comid = mapData['_id'];
  }
}
