class Student {
  var parid,
      studid,
      previousClasses,
      idNo,
      firstName,
      lastName,
      dateOfBirth,
      gender,
      profilePic,
      status,
      currentAddress,
      crid;

  Student({
    this.studid,
    this.parid,
    this.previousClasses,
    this.crid,
    this.idNo,
    this.firstName,
    this.lastName,
    this.profilePic,
    this.status,
    this.dateOfBirth,
    this.gender,
    this.currentAddress,
  });

  Student.fromMap(Map<String, dynamic> mapData) {
    this.studid = mapData['_id'];
    this.idNo = mapData['idNo'];
    this.firstName = mapData['firstName'];
    this.lastName = mapData['lastName'];
    this.dateOfBirth = mapData['dateOfBirth'];
    this.status = mapData['status'];

    this.gender = mapData['gender'];
    this.currentAddress = mapData['currentAddress'];
    this.profilePic = mapData['profilePic'];
    this.crid =
        mapData['crid'] != null ? mapData['crid'] : "class room id is null";
    this.parid = mapData['parid'];
    this.previousClasses = mapData['previousClasses'];
  }

  Map<String, dynamic> toMap() {
    return {
      'idNo': this.idNo,
      'firstName': this.firstName,
      'lastName': this.lastName,
      'dateOfBirth': this.dateOfBirth,
      'gender': this.gender,
      'currentAddress': this.currentAddress,
      'previousClasses': this.previousClasses,
      'crid': this.crid,
      'status': this.status,
      'parid': this.parid,
      '_id': this.studid,
      'profilePic': this.profilePic
    };
  }
}
