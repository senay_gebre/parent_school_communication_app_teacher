import 'package:login/src/model/result.dart';

class Exam {
  String stuid;
  List listOfResult;

  // Exam({this.score, this.stuid, this.title, this.weight});
  Exam({this.listOfResult, this.stuid});

  // void addStuResult({String title, double score, double weight}) {
  //   listOfRestult.add({"title": title, "weight": weight, "score": score});
  // }

  Map<String, dynamic> toMap() {
    return {
      "results": this.listOfResult,
    };
  }
}
