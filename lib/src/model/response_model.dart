// import 'package:login/src/model/user_file.dart';

class ResponseModel {
  var response, statusCode;
  bool success;

  String msg;

  ResponseModel(
      {this.response, this.msg, this.statusCode, this.success = false});
}
