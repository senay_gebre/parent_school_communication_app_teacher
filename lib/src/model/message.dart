class Message {
  String senderId;
  String receiverId;
  String senderRole;
  String receiverRole;
  String message;
  String messageDate;

  Message(
      {this.senderId,
      this.receiverId,
      this.message,
      this.receiverRole,
      this.senderRole,
      this.messageDate});

  //Will be only called when you wish to send an image
  // named constructor
  Message.imageMessage(
      {this.senderId,
      this.receiverId,
      this.message,
      this.receiverRole,
      this.senderRole,
      this.messageDate});

  Map toMap() {
    var map = Map<String, dynamic>();
    map['senderid'] = this.senderId;
    map['receiverid'] = this.receiverId;
    map['senderRole'] = this.senderRole;
    map['receiverRole'] = this.senderRole;
    map['message'] = this.message;
    map['date'] = this.messageDate;

    return map;
  }

  // Map toImageMap() {
  //   var map = Map<String, dynamic>();
  //   map['message'] = this.message;
  //   map['senderId'] = this.senderId;
  //   map['receiverId'] = this.receiverId;

  //   map['photoURL'] = this.photoURL;
  //   return map;
  // }

  // named constructor
  Message.fromMap(Map<String, dynamic> map) {
    this.senderId = map['senderid'];
    this.receiverId = map['receiverid'];
    this.senderRole = map['senderRole'];
    this.senderRole = map['receiverRole'];
    this.message = map['message'];
    this.messageDate = map['date'];
  }
}
