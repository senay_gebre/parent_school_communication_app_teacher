class Result {
  double weight;
  String resultId;
  String title;
  double score;
  bool taken;

  Result(
      {this.score = 0.0,
      this.title,
      this.weight = 0.0,
      this.taken = false,
      this.resultId});

  Map<String, dynamic> toMap() {
    return {
      "title": this.title.toString(),
      "weight": this.weight,
      "score": this.score,
      "taken": this.taken
    };
  }

  Result.fromMap(Map<String, dynamic> mapData) {
    this.score = double.parse(mapData['score'].toString());
    this.taken = mapData['taken'];
    this.title = mapData['title'];
    this.weight = double.parse((mapData['weight'].toString()));
    this.resultId = mapData['_id'];
  }
}
