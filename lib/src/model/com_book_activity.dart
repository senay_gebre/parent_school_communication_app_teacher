class ComBookActivity {
  String dueDate;
  String type;
  String singleActivityId;

  ComBookActivity({this.dueDate, this.singleActivityId, this.type});

  ComBookActivity.fromMap(Map<String, dynamic> mapData) {
    this.dueDate = mapData['dueDate'];
    this.type = mapData['type'];
    this.singleActivityId = mapData['_id'];
  }

  Map<String, dynamic> toMap() {
    return {'dueDate': this.dueDate, 'type': this.type};
  }
}
