class TeacherClass {
  String crid;
  String grade;
  String section;
  String academicYear;
  List<dynamic> subjects;
  bool isHomeRoom = false;

  TeacherClass(
      {this.academicYear,
      this.crid,
      this.grade,
      this.isHomeRoom,
      this.section,
      this.subjects});

  TeacherClass.fromMap(Map<dynamic, dynamic> mapData) {
    this.crid = mapData['_id'];
    this.grade = mapData['grade'].toString();
    this.section = mapData['section'];
    this.academicYear = mapData['academicYear'];
    this.subjects = mapData['subjects'];
    this.isHomeRoom = mapData['isHomeRoom'];
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': this.crid,
      'grade': this.grade,
      'section': this.section,
      'academicYear': this.academicYear,
      'subjects': this.subjects,
      'isHomeRoom': this.isHomeRoom,
    };
  }
}
