class UserFile {
  var username,
      password,
      firstName,
      lastName,
      profilePic,
      phone,
      gender,
      email,
      currentAddress,
      token,
      dateOfBirth,
      role,
      tid;

  UserFile(
      {this.username,
      this.password,
      this.firstName,
      this.lastName,
      this.profilePic,
      this.phone,
      this.gender,
      this.dateOfBirth,
      this.currentAddress,
      this.email,
      this.token,
      this.tid});

  Map<String, dynamic> toMap(UserFile user) {
    var data = Map<String, dynamic>();
    data['username'] = user.username;
    data['password'] = user.password;
    data['firstName'] = user.firstName;
    data['lastName'] = user.lastName;
    data["profilePic"] = user.profilePic;
    data["phone"] = user.phone;
    data["gender"] = user.gender;
    data["currentAddress"] = user.currentAddress;
    data["email"] = user.email;
    data["token"] = user.token;
    data["_id"] = user.tid;
    data["dateOfBirth"] = user.dateOfBirth;
    data["role"] = user.role;

    return data;
  }

  UserFile.fromMap(Map<String, dynamic> mapData) {
    this.username = mapData['username'];
    this.password = mapData['password'];
    this.firstName = mapData['firstName'];
    this.lastName = mapData['lastName'];
    this.profilePic = mapData['profilePic'];
    this.dateOfBirth = mapData['dateOfBirth'];
    this.phone = mapData['phone'];
    this.gender = mapData['gender'];
    this.currentAddress = mapData['currentAddress'];
    this.email = mapData['email'];
    this.token = mapData['token'];
    this.role = mapData['role'];
    this.tid = mapData['_id'];
  }
}
