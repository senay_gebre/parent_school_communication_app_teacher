class ComBookAssignments {
  String subject;
  String comAssiId;
  List<dynamic> activityList = [];
  String comment;
  List approved;

  ComBookAssignments({this.activityList, this.comment, this.subject, this.approved});

  ComBookAssignments.fromMap(Map<String, dynamic> mapData) {
    this.subject = mapData['subject'];
    this.comment = mapData['comment'];
    this.approved = mapData['approvedBy'];
    this.comAssiId = mapData['_id'];
  }

  void setActivity(var listOfActivity) {
    activityList = listOfActivity;
  }
}
