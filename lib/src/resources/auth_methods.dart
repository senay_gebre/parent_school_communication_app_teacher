import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:login/src/model/attendance.dart';
import 'package:login/src/model/com_book.dart';
import 'package:login/src/model/com_book_activity.dart';
import 'package:login/src/model/com_book_assignments.dart';
import 'package:login/src/model/com_book_get_data.dart';
import 'package:login/src/model/event.dart';
import 'package:login/src/model/grade_report.dart';
import 'package:login/src/model/message.dart';
import 'package:login/src/model/result.dart';
import 'package:login/src/model/student.dart';
import 'package:login/src/model/response_model.dart';
import 'package:login/src/model/teacher_class.dart';
import 'package:login/src/model/user_file.dart';

class AuthMethods {
  final String mainURL = "https://examination-backend-pt.herokuapp.com/school";
  ResponseModel _responseModel = ResponseModel();
  final storage = FlutterSecureStorage();
  var _jsonData;
  var _token;

  Future<dynamic> getUserToken() async {
    try {
      _token = await storage.read(key: "token");
      return _token;
    } catch (e) {
      print("Error on getUserToken(): $e");
    }
  }

  Future<ResponseModel> signIn(UserFile user) async {
    Map data = <String, dynamic>{
      'username': user.username,
      'password': user.password
    };

    String body = json.encode(data);

    try {
      var res = await http
          .post(Uri.parse('$mainURL/teacher/login'),
              headers: {"Content-Type": "application/json"}, body: body)
          .timeout(const Duration(seconds: 20));

      print(res.body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        //  print("${res.body} 5555555555666666666666655555566666666");
        _responseModel.response = json.decode(res.body);
        _responseModel.msg = "Successfully signed in!";
        _responseModel.success = true;
      } else if (res.statusCode == 401 || res.statusCode == 403) {
        _responseModel.response = json.decode(res.body);
        _responseModel.msg = "Username or Password is incorrect!";
        _responseModel.success = false;
      } else {
        _responseModel.response = json.decode(res.body);
        _responseModel.msg = "Something is wrong!";
        _responseModel.success = false;
      }
    } on SocketException {
      _responseModel.msg = "No Internet Connection!";
    } on HttpException {
      _responseModel.msg = "Couldn't find the user!";
    } on FormatException catch (error) {
      if (error.toString().contains("username or password incorrect")) {
        _responseModel.msg = "Username or Password is incorrect!";
      } else {
        _responseModel.msg = "Something is wrong!";
      }
    } on TimeoutException {
      _responseModel.msg = "Bad internet connection!";
    } on Error {
      _responseModel.msg = "Something is wrong!";
    }
    return _responseModel;
  }

  Future<UserFile> getCurrentUser() async {
    var _user = await storage.read(key: "user");
    if (_user != null) {
      var _decodedData = await jsonDecode(_user);
      // print(_decodedData);
      return UserFile.fromMap(_decodedData);
    }
    return null;
  }

  Future<List<TeacherClass>> viewMyClass() async {
    List<TeacherClass> listOfClasses;
    var _token = await getUserToken();
    var jsonList;
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/findClasses');
      var res = await http.get(
        url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer $_token",
        },
      );

      print("${res.body} pppppppppppppppppppppppp");
      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        jsonList = _jsonData['classes'];
      } else {
        print(res.statusCode);
        print(res.body);
      }
    } catch (err) {
      print("${err} senay son son son son son son son son son son");
    }
    if (jsonList != null) {
      listOfClasses =
          (jsonList as List).map((i) => new TeacherClass.fromMap(i)).toList();
      // print("${listOfClasses} senay son son son son son son son son son son");

    }

    return listOfClasses;
  }

  Future<ResponseModel> viewClassComBook(ComBookGetData comBookGetData) async {
    Map data = <String, dynamic>{
      'crid': comBookGetData.crid,
    };

    String body = json.encode(data);

    List<ComBookAssignments> listOfClassComBook;

    var _token = await getUserToken();
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/getComBookPerMonth/0');
      var res = await http.post(
        url,
        body: body,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer $_token",
        },
      );

      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        var jsonList = _jsonData['result'];
        // var jsonListAssi = _jsonData['result'][0]['assignments'];
        print("step1");

        if (jsonList.isNotEmpty) {
          print("step2");
          print("sennnnnn");

          _responseModel.response = (jsonList as List).map<ComBook>((i) {
            var singleAssignment;
            var assignments = [];

            var comBook;
            comBook = ComBook.fromMap(i);

            if (i['assignments'].isNotEmpty) {
              for (singleAssignment in i['assignments']) {
                ComBookAssignments comBookAssignment =
                    ComBookAssignments.fromMap(singleAssignment);
                var singleActivity;
                var activities = [];

                if (singleAssignment['activity'].isNotEmpty) {
                  for (singleActivity in singleAssignment['activity']) {
                    activities.add(ComBookActivity.fromMap(singleActivity));
                  }
                }

                comBookAssignment.activityList = activities;

                assignments.add(comBookAssignment);
              }
            }

            comBook.assignmentsList = assignments;

            return comBook;
          }).toList();

          print("${_responseModel.response} ayuresponsemodel.response");

          _responseModel.success = true;
        } else {
          _responseModel.response = [];
          _responseModel.success = true;
          _responseModel.msg = "Communication book is empty";
        }
      } else {
        _responseModel.success = false;
        print(res.statusCode);
        print(res.body);
      }
    } catch (err) {
      print("${err} sennnnnnnnnnnnnnnnnnnnnnnn");
    }

    return _responseModel;
  }

  Future<List<Student>> viewMyStudents(String crid) async {
    Map data = <String, String>{'crid': crid};
    String body = json.encode(data);

    List<Student> listOfStudents;
    var _token = await getUserToken();
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/findStudents');
      var res = await http.post(
        url,
        body: body,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer $_token",
        },
      );

      //print("${res.body} pppppppppppppppppppppppp");
      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
      } else {
        print(res.statusCode);
        print(res.body);
      }
    } catch (err) {
      print("${err} senay son son son son son son son son son son");
    }

    var jsonList = _jsonData['students'];

    listOfStudents =
        (jsonList as List).map((i) => new Student.fromMap(i)).toList();
    // print("${listOfClasses} senay son son son son son son son son son son");

    return listOfStudents;
  }

  Future<ResponseModel> changeProfileData(UserFile user) async {
    var _token = await getUserToken();
    // print(
    //     "${user.gender} senay senay senay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senay");
    Map data = <String, String>{
      'firstName': user.firstName,
      'lastName': user.lastName,
      'username': user.username,
      'gender': user.gender,
      'currentAddress': user.currentAddress,
      'phone': user.phone,
      'email': user.email
    };
    String body = json.encode(data);
    //print(body);
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/parent/changeProf');
      var res = await http.patch(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);
      _responseModel.response = json.decode(res.body);
      //print(_responseModel.response);
      print(
          "${_responseModel.response} senay senay senay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senaysenay senay");

      if (_responseModel.response["message"]
          .toString()
          .contains("change profile successful")) {
        _responseModel.msg = "Account Updated successfully ";
        _responseModel.success = true;
      } else {
        _responseModel.msg = "Something is wrong";
        _responseModel.success = false;
      }
    } catch (errr) {
      _responseModel.msg = "Something is wrong";
      _responseModel.success = false;
    }
    return _responseModel;
  }

  Future<ResponseModel> addComBook(ComBook comBook) async {
    var _token = await getUserToken();

    var singleActivity;

    var listOfMappedActivity = [];
    if (comBook.assignmentsList[0].activityList.isNotEmpty) {
      for (singleActivity in comBook.assignmentsList[0].activityList) {
        listOfMappedActivity.add(singleActivity.toMap());
      }
    }
    Map data = <String, dynamic>{
      'crid': comBook.crid,
      'date': comBook.date,
      'assignments': [
        {
          "subject":
              comBook.assignmentsList[0].subject.toString().toLowerCase(),
          "activity": listOfMappedActivity,
          "comment": comBook.assignmentsList[0].comment
        }
      ]
    };
    String body = json.encode(data);
    print("${body} sheger");
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/createAssignment');
      var res = await http.post(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      print(res.body);
      _responseModel.response = json.decode(res.body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        _responseModel.success = true;
        _responseModel.msg = "Added com book successfully!";
      } else {
        _responseModel.msg = "Something went wrong!";
        _responseModel.success = false;
      }

      // print(_responseModel.response);
    } catch (err) {
      print("Error $err");
      _responseModel.success = false;
      _responseModel.msg = "Something went wrong!";
    }
    return _responseModel;
  }

  Future<ResponseModel> sendMessage(Message message) async {
    Map data = <String, String>{
      'senderid': message.senderId,
      'receiverid': message.receiverId,
      'senderRole': message.senderRole,
      'receiverRole': message.receiverRole,
      'message': message.message
    };
    String body = json.encode(data);
    var _token = await getUserToken();

    print(body);
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/sendMessage');
      var res = await http.post(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        _responseModel.response = json.decode(res.body);
        _responseModel.msg = "message sent";
        _responseModel.success = true;
      } else {
        _responseModel.msg = "message not sent";
        _responseModel.success = false;
      }
    } catch (err) {
      _responseModel.msg = "Something is wrong!";
      _responseModel.success = false;
      print(err);
    }
    return _responseModel;
  }

  Future<ResponseModel> getMessages({@required String stuId}) async {
    Map data = <String, dynamic>{'otherid': stuId};
    String body = json.encode(data);

    var _token = await getUserToken();
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/getMessages');
      var res = await http.post(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        var jsonList;
        if (_jsonData['messages'].isNotEmpty) {
          jsonList = _jsonData['messages'];
          _responseModel.response =
              (jsonList as List).map((i) => new Message.fromMap(i)).toList();
          _responseModel.success = true;
          _responseModel.msg = "message list not empty";
          print(
              "${_responseModel.response} senay son son son son son son son son son son");
        } else if (_jsonData['messages'].isEmpty) {
          _responseModel.response = [];
          _responseModel.success = true;
          _responseModel.msg = "message list is empty";
        } else {
          _responseModel.success = false;
          _responseModel.msg = "something went wrong!";
        }
      } else {
        print(res.statusCode);
        print(res.body);
      }
    } catch (err) {
      print("${err} senay son son son son son son son son son son");
      _responseModel.success = false;
      _responseModel.msg = "something went wrong!";
    }

    return _responseModel;
  }

  Future<ResponseModel> updateRate(var listOfRate) async {
    Map data = <String, dynamic>{"students": listOfRate};
    String body = json.encode(data);
    var _token = await getUserToken();

    print("${body} usabody");
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/rateSkills');
      var res = await http.post(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        _responseModel.response = json.decode(res.body);
        _responseModel.msg = "skill rate updated successfully.";
        _responseModel.success = true;
      } else {
        _responseModel.msg = "Something is wrong!";
        _responseModel.success = false;
      }
    } catch (err) {
      _responseModel.msg = "Something is wrong!";
      _responseModel.success = false;
      print(err);
    }
    return _responseModel;
  }

  Future<ResponseModel> takeAttendance(var attendance, String crid) async {
    Map data = <String, dynamic>{
      "_id": crid,
      "attendance": {"date": "2021-06-22", "attendance": attendance}
    };

    String body = json.encode(data);
    var _token = await getUserToken();

    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/takeAttendance');
      var res = await http.patch(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      print("${res.body} usabody");

      if (res.statusCode == 200 || res.statusCode == 201) {
        _responseModel.msg = json.decode(res.body).toString();
        _responseModel.success = true;
      } else {
        _responseModel.msg = "Something is wrong!";
        _responseModel.success = false;
      }
    } catch (err) {
      _responseModel.msg = "Something is wrong!";
      _responseModel.success = false;
      print(err);
    }

    return _responseModel;
  }

  Future<ResponseModel> editAttendance(var attendance, String crid) async {
    Map data = <String, dynamic>{
      "crid": crid,
      "attendances": {
        "_id": attendance.attenId,
        "date": attendance.date,
        "attendance": [
          {
            "stuid": attendance.stuId,
            "present": attendance.present,
            "permitted": attendance.permitted,
            "late": attendance.sLate
          }
        ]
      },
    };

    String body = json.encode(data);
    var _token = await getUserToken();

    print("${body} atten");
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/editAttendance');
      var res = await http.patch(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      print("${res.body} usabody");

      if (res.statusCode == 200 || res.statusCode == 201) {
        _responseModel.msg = json.decode(res.body).toString();
        _responseModel.success = true;
      } else {
        _responseModel.msg = "Something is wrong!";
        _responseModel.success = false;
      }
    } catch (err) {
      _responseModel.msg = "Something is wrong!";
      _responseModel.success = false;
      print(err);
    }

    return _responseModel;
  }

  Future<ResponseModel> addResult(var listOfExamResults) async {
    Map data = <String, dynamic>{
      "results": listOfExamResults,
    };

    String body = json.encode(data);
    var _token = await getUserToken();

    print("${body} kobobody");
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/addExamResult');
      var res = await http.post(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);
      print("${res.statusCode} niga");

      if (res.statusCode == 200 || res.statusCode == 201) {
        _responseModel.msg = res.body;
        _responseModel.success = true;
      } else {
        _responseModel.msg = "Something is wrong!";
        _responseModel.success = false;
      }
    } catch (err) {
      _responseModel.msg = "Something is wrong!";
      _responseModel.success = false;
      print(err);
    }
    return _responseModel;
  }

  Future<ResponseModel> viewResultPerClassBySub(
      {var selectedClass, String quarter, String subject}) async {
    Map data = <String, dynamic>{
      "crid": selectedClass.crid,
      "academicYear": selectedClass.academicYear,
      "quarter": quarter,
      "subject": subject
    };

    String body = json.encode(data);
    var _token = await getUserToken();
    var jsonList;

    print("${body} kobobody");
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/viewResultPerClassBySub');
      var res = await http.post(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        print("${_jsonData} byres");

        jsonList = _jsonData['gradeReport'];
        if (jsonList.isNotEmpty) {
          print("step2");
          print("sennnnnn");

          _responseModel.response = (jsonList as List).map<GradeReport>((i) {
            var singleResult;
            var listOfResult = [];

            var gradeReport;
            gradeReport = GradeReport.fromMap(i);

            if (i['results'][0].isNotEmpty) {
              print("${i['results'][0]} empty");
              for (singleResult in i['results'][0]) {
                Result result = Result.fromMap(singleResult);

                listOfResult.add(result);
              }
            }

            gradeReport.listOfResult = listOfResult;

            return gradeReport;
          }).toList();
          _responseModel.msg = "notempty";

          _responseModel.success = true;
        } else {
          _responseModel.response = <GradeReport>[];
          _responseModel.msg = "empty";
          _responseModel.success = true;
        }
      } else {
        _responseModel.response = null;
        _responseModel.msg = "Something is wrong!";
        _responseModel.success = false;
      }
    } catch (err) {
      _responseModel.response = null;
      _responseModel.msg = "Something is wrong!";
      _responseModel.success = false;
      print(err);
    }
    return _responseModel;
  }

  Future<List<TeacherClass>> viewHomeRoomClass() async {
    List<TeacherClass> listOfClasses;
    var _token = await getUserToken();
    var jsonList;
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/findHomeRoomClasses');
      var res = await http.get(
        url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer $_token",
        },
      );

      print("${res.body} pppppppppppppppppppppppp");
      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        jsonList = _jsonData['classes'];
      } else {
        print(res.statusCode);
        print(res.body);
      }
    } catch (err) {
      print("${err} senay son son son son son son son son son son");
    }
    if (jsonList != null) {
      listOfClasses =
          (jsonList as List).map((i) => new TeacherClass.fromMap(i)).toList();
      // print("${listOfClasses} senay son son son son son son son son son son");

    }

    return listOfClasses;
  }

  Future<ResponseModel> getStudentAttendance({
    @required String stuId,
    @required String crid,
    @required int month,
    @required int year,
    @required int prevMonth,
  }) async {
    Map data = <String, dynamic>{
      'stuid': stuId,
      'crid': crid,
    };
    String body = json.encode(data);
    print("${prevMonth} prevMonth");

    var _token = await getUserToken();
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/viewAttPerStudentPerMonth/$prevMonth');
      var res = await http.post(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      print("${res.body} akerat");

      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        
        if(_jsonData != null) {
var jsonList;
if (_jsonData['attendance'].isNotEmpty) {
          if (_jsonData['attendance'][0]['result'].isNotEmpty) {
            jsonList = _jsonData['attendance'][0]['result'];
            _responseModel.response =
                (jsonList as List).map((i) => Attendance.fromMap(i)).toList();
            _responseModel.success = true;
            print("${_responseModel.response} liyu");
          } else if (_jsonData['attendance'][0]['result'].isEmpty) {
            _responseModel.response = [];
            _responseModel.success = true;
          }
        } else if (_jsonData['attendance'].isEmpty) {
          _responseModel.response = [];
          _responseModel.success = true;
        }
        }
        
      } else {
        _responseModel.success = false;
        _responseModel.msg = "something went wrong!";
      }
    } catch (err) {
      print(err);
    }

    return _responseModel;
  }

  Future<ResponseModel> updateResult({var listOfEditedResults}) async {
    Map data = <String, dynamic>{
      "exams": listOfEditedResults,
    };

    String body = json.encode(data);
    var _token = await getUserToken();

    print("${body} kobobody");
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/teacher/editExamResult');
      var res = await http.patch(url,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $_token",
          },
          body: body);

      if (res.statusCode == 200 || res.statusCode == 201) {
        _responseModel.msg = res.body;
        _responseModel.success = true;
      } else {
        _responseModel.msg = "Something is wrong!";
        _responseModel.success = false;
      }
    } catch (err) {
      _responseModel.msg = "Something is wrong!";
      _responseModel.success = false;
      print(err);
    }
    return _responseModel;
  }

  Future<ResponseModel> getUpcomingEvents({
    @required int pageNumberUP,
  }) async {
    // print("${pageNumber} lomi");
    var _token = await getUserToken();
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/getUpcomingEvents/$pageNumberUP');
      var res = await http.get(url, headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $_token",
      });

      print("${res.body} lim");

      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        var jsonList;
        if (_jsonData['events'].isNotEmpty) {
          jsonList = _jsonData['events'];
          _responseModel.response =
              (jsonList as List).map((i) => new Event.fromMap(i)).toList();
          _responseModel.success = true;
          _responseModel.msg = "events not empty";
          print(
              "${_responseModel.response} senay son son son son son son son son son son");
        } else if (_jsonData['events'].isEmpty) {
          _responseModel.response = [];
          _responseModel.success = true;
          _responseModel.msg = "events list is empty";
        } else {
          _responseModel.success = false;
          _responseModel.msg = "something went wrong!";
        }
      } else {
        _responseModel.success = false;
        _responseModel.msg = "something went wrong!";
        print(res.statusCode);
        print(res.body);
      }
    } catch (err) {
      print("${err} senay son son son son son son son son son son");
      _responseModel.success = false;
      _responseModel.msg = "something went wrong!";
    }

    return _responseModel;
  }

  Future<ResponseModel> getAllEvents({
    @required int pageNumberAE,
  }) async {
    // print("${pageNumber} lomi");
    var _token = await getUserToken();
    try {
      var url = Uri.parse(
          'https://examination-backend-pt.herokuapp.com/school/getAllEvents/$pageNumberAE');
      var res = await http.get(url, headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $_token",
      });

      print("${res.body} lim");

      if (res.statusCode == 200 || res.statusCode == 201) {
        _jsonData = json.decode(res.body);
        var jsonList;
        if (_jsonData['events'].isNotEmpty) {
          jsonList = _jsonData['events'];
          _responseModel.response =
              (jsonList as List).map((i) => new Event.fromMap(i)).toList();
          _responseModel.success = true;
          _responseModel.msg = "events not empty";
          print(
              "${_responseModel.response} senay son son son son son son son son son son");
        } else if (_jsonData['events'].isEmpty) {
          _responseModel.response = [];
          _responseModel.success = true;
          _responseModel.msg = "events list is empty";
        } else {
          _responseModel.success = false;
          _responseModel.msg = "something went wrong!";
        }
      } else {
        _responseModel.success = false;
        _responseModel.msg = "something went wrong!";
        print(res.statusCode);
        print(res.body);
      }
    } catch (err) {
      print("${err} senay son son son son son son son son son son");
      _responseModel.success = false;
      _responseModel.msg = "something went wrong!";
    }

    return _responseModel;
  }
}
