import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:login/src/constants/constants.dart';
import 'package:login/src/model/teacher_class.dart';
import 'package:login/src/provider/activity_provider.dart';
import 'package:login/src/model/user_file.dart';
import 'package:login/src/provider/attendance_provider.dart';
import 'package:login/src/provider/com_book_provider.dart';
import 'package:login/src/provider/evaluation_provider.dart';
import 'package:login/src/provider/events_provider.dart';
import 'package:login/src/provider/student_provider.dart';
import 'package:login/src/provider/teacher_class_provider.dart';
import 'package:login/src/provider/toggle_provider.dart';
import 'package:login/src/provider/user_provider.dart';
import 'package:login/src/resources/auth_methods.dart';
import 'package:login/src/ui/pages/Auth/login/login_page.dart';
import 'package:login/src/ui/pages/homescreen.dart';
import 'package:login/src/ui/pages/profile/edit_profile_page.dart';
import 'package:login/src/ui/pages/student/com_book/com_book_page.dart';
import 'package:login/src/ui/pages/student/evaluations/grading_page.dart';
import 'package:login/src/ui/pages/student/rating/progress_page.dart';
import 'package:provider/provider.dart';

import 'src/provider/user_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthMethods _authMethods = AuthMethods();
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // ChangeNotifierProvider(create: (_) => ImageUploadProvider()),
        ChangeNotifierProvider(create: (context) => UserProvider()),
        ChangeNotifierProvider<StudentProvider>(
            create: (context) => StudentProvider()),
        // ChangeNotifierProvider<ActivityProvider>(
        //     create: (context) => ActivityProvider()),
        ChangeNotifierProvider<ComBookProvider>(
            create: (context) => ComBookProvider()),
        ChangeNotifierProvider<TeacherClassProvider>(
            create: (context) => TeacherClassProvider()),
        ChangeNotifierProvider<EvaluationProvider>(
            create: (context) => EvaluationProvider()),
        ChangeNotifierProvider<AttendanceProvider>(
            create: (context) => AttendanceProvider()),
        ChangeNotifierProvider<ToggleProvider>(
            create: (context) => ToggleProvider()),
        ChangeNotifierProvider<EventsProvider>(
            create: (context) => EventsProvider()),
      ],
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: kPrimaryColor,
          // fontFamily: 'Nunito',
        ),
        title: "parent_school_communication_app",
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          HomeScreen.id: (context) => HomeScreen(),
          LoginPage.id: (context) => LoginPage(),
          GradingPage.id: (context) => GradingPage(),
          ProgressPage.id: (context) => ProgressPage(),
          ComBookPage.id: (context) => ComBookPage(),
        },
        home: FutureBuilder(
          future: _authMethods.getCurrentUser(),
          builder: (context, AsyncSnapshot<UserFile> snapshot) {
            if (snapshot.hasData) {
              print(
                  "${snapshot.data.firstName}  0000000000000000000000000000000000000000000000");
              return HomeScreen();
            } else {
              return LoginPage();
            }
          },
        ),
      ),
    );
  }
}
